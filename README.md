Fund 3 - Visual Studio Smart Contract Plugin by Rob Greig

Funded project from the Cardano Project Catalyst Experiment

https://cardano.ideascale.com/a/dtd/Visual-Studio-Smart-Contract-Plugin/333827-48088

Use the code written in Visual Basic .net to

- communicate with the blockchain using the BLOCKFROST.IO API

- communicate with the blockchain using the Cardano CLI via Daedalus Mainnet or Daedalus Testnet

Also, 
mint transactions using the CLI
mint NFT transactions using the CLI
send coins and NFT via command line

call lots of functions using the API (no Daedalus node required)

** This is now example working code rather than a Plugin **



## Clone a repository

Use these steps to clone from SourceTree, the client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.


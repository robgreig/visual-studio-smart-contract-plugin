﻿Imports System.IO
Imports System.Net

Public Class frmNFTGallery

    Private myImageList As System.Windows.Forms.ImageList = New ImageList()
    Private myImageListSmall As System.Windows.Forms.ImageList = New ImageList()
    Private myImageListLarge As System.Windows.Forms.ImageList = New ImageList()

    Private Sub frmNFTGallery_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        myImageList.ImageSize = New Size(50, 50)
        myImageListSmall.ImageSize = New Size(32, 32)
        myImageListLarge.ImageSize = New Size(256, 256)
        With lstAssets
            .SmallImageList = myImageListSmall
            .SmallImageList = myImageListLarge
            .View = View.LargeIcon
            .Columns.Clear()
            .Columns.Add("Asset", 350)
            .Columns.Add("Quantity", 80)
            .Columns.Add("Policy ID", 350)
            .Columns.Add("Asset Name", 200)
            .Columns.Add("Fingerprint", 280)
            '.Columns.Add("Qty", 120)
            .Columns.Add("Initial Mint TX Hash", 120)
            .Columns.Add("Onchain - Name", 120)
            .Columns.Add("Onchain - Image", 120)
            .Columns.Add("Metadata - Name", 120)
            .Columns.Add("Metadata - Description", 400)
            .Columns.Add("Metadata - Ticker", 120)
            .Columns.Add("Metadata - URL", 300)
            .Columns.Add("Metadata - Logo", 400)
        End With
        radioButtonLarge.Checked = True
        Timer1.Enabled = True
    End Sub
    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        Call DoRefresh()
    End Sub
    Sub DoRefresh()
        Dim iCount As Integer = 0
        Try
            btnRefresh.Enabled = False
            ToolStripStatusLabel1.Text = Now & " - Loading NFTs from the Cardano Blockchain .."
            Dim iMaxResults As Integer = 99999 'default to bring all pools back
            iMaxResults = NumericUpDownMaxAssets.Value
            Application.DoEvents()

            Dim MyObjectList As New List(Of Object)

            Dim BlockfrostService = New clsBlockfrost(clsBlockfrost.UseMainNet)
            MyObjectList = BlockfrostService.GetAssetsList(iMaxResults)
            lstAssets.Items.Clear()
            Application.DoEvents()

            For Each item In MyObjectList
                Dim sAsset As String = ""
                Dim sQuantity As String = ""
                sAsset = item("asset")
                sQuantity = item("quantity")
                Dim MyAsset As Asset = BlockfrostService.GetSpecificAsset(sAsset)

                If MyAsset.asset > "" Then
                    Dim itmX As New ListViewItem
                    Dim sImageIPFS As String = ""
                    Dim sImageIPFSName As String = ""
                    itmX.Text = sAsset
                    itmX.SubItems.Add(sQuantity)
                    itmX.SubItems.Add(MyAsset.policy_id)
                    itmX.SubItems.Add(BlockfrostService.DecodeHEXString(MyAsset.asset_name))
                    itmX.SubItems.Add(MyAsset.fingerprint)
                    itmX.SubItems(1).Text = MyAsset.quantity

                    itmX.SubItems.Add(MyAsset.initial_mint_tx_hash)
                    If Not IsNothing(MyAsset.onchain_metadata) Then
                        itmX.SubItems.Add(MyAsset.onchain_metadata.name)
                        itmX.SubItems.Add(MyAsset.onchain_metadata.image)
                        sImageIPFS = MyAsset.onchain_metadata.image
                        sImageIPFSName = MyAsset.onchain_metadata.name
                    Else
                        itmX.SubItems.Add("")
                        itmX.SubItems.Add("")
                    End If
                    If Not IsNothing(MyAsset.metadata) Then
                        itmX.SubItems.Add(MyAsset.metadata.name)
                        itmX.SubItems.Add(MyAsset.metadata.description)
                        itmX.SubItems.Add(MyAsset.metadata.ticker)
                        itmX.SubItems.Add(MyAsset.metadata.url)
                        itmX.SubItems.Add(MyAsset.metadata.logo)
                    Else
                        itmX.SubItems.Add("")
                        itmX.SubItems.Add("")
                        itmX.SubItems.Add("")
                        itmX.SubItems.Add("")
                        itmX.SubItems.Add("")
                    End If

                    'only show assets with images
                    If sImageIPFS > "" Then
                        itmX.Text = sImageIPFSName
                        itmX.Tag = sImageIPFSName
                        Dim sURL As String = BlockfrostService.IPFSGetImageURLDev(sImageIPFS)
                        Dim targetURI As New Uri(sURL)
                        Try
                            Dim request = CType(WebRequest.Create(targetURI), HttpWebRequest)
                            Dim img As System.Drawing.Image = Nothing
                            img = System.Drawing.Image.FromStream(request.GetResponse().GetResponseStream)
                            myImageListSmall.Images.Add(img)
                            myImageListLarge.Images.Add(img)
                            itmX.ImageIndex = iCount
                            lstAssets.Items.Add(itmX)
                            iCount += 1
                        Catch ex As Exception
                            'TODO: not all images can be handled by the Image Stream - this needs changing
                            'ToolStripStatusLabel1.Text = Now & " - ERROR  - " & ex.Message
                            'lstAssets.Items.Add(itmX)                            
                        End Try
                    Else
                        ' lstAssets.Items.Add(itmX)
                    End If
                End If
                Application.DoEvents()
            Next
            ToolStripStatusLabel1.Text = Now & " - Loading NFTs from the Cardano Blockchain .. Done"
        Catch ex As Exception
            ToolStripStatusLabel1.Text = Now & " - ERROR  - " & ex.Message
        End Try
        btnRefresh.Enabled = True
    End Sub
    Private Sub radioButtonList_CheckedChanged_1(sender As Object, e As EventArgs) Handles radioButtonList.CheckedChanged
        If radioButtonList.Checked = True Then lstAssets.View = View.Details
    End Sub
    Private Sub radioButtonSmall_CheckedChanged_1(sender As Object, e As EventArgs) Handles radioButtonSmall.CheckedChanged
        If radioButtonSmall.Checked = True Then
            lstAssets.SmallImageList = myImageListSmall
            lstAssets.View = View.SmallIcon
        End If
    End Sub
    Private Sub radioButtonLarge_CheckedChanged_1(sender As Object, e As EventArgs) Handles radioButtonLarge.CheckedChanged
        If radioButtonLarge.Checked = True Then
            lstAssets.LargeImageList = myImageListLarge
            lstAssets.View = View.LargeIcon
        End If
    End Sub
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Timer1.Enabled = False
        Call DoRefresh()
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmCardanoCLI
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblCLI = New System.Windows.Forms.Label()
        Me.txtParameters = New System.Windows.Forms.TextBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.btnExecute = New System.Windows.Forms.Button()
        Me.PanelTop = New System.Windows.Forms.Panel()
        Me.btnRunALL = New System.Windows.Forms.Button()
        Me.cboScripts = New System.Windows.Forms.ComboBox()
        Me.chkLog = New System.Windows.Forms.CheckBox()
        Me.lblLineNo = New System.Windows.Forms.Label()
        Me.lblVarAmountToken = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.lblVarAmountToSendADA = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.lblVarTokenToSendAmount = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.lblVarPolicyHash = New System.Windows.Forms.Label()
        Me.lblVarAmountToSend = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.lblVarLatestSlot = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.lblVarLatestBlock = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.lblVarTxFee = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.lblVarTokenAmountToMint = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.lblVarTokenName = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.lblVarPolicyID = New System.Windows.Forms.Label()
        Me.Label151 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lblVarAmount2 = New System.Windows.Forms.Label()
        Me.lblVarAddressSendTo = New System.Windows.Forms.Label()
        Me.lblVarPaymentAddress = New System.Windows.Forms.Label()
        Me.lblVarAmount = New System.Windows.Forms.Label()
        Me.lblVarTxIx = New System.Windows.Forms.Label()
        Me.lblVarTxHash = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblCLIComments = New System.Windows.Forms.Label()
        Me.txtRedirect = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblTestnetID = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.RadioButtonTESTNET = New System.Windows.Forms.RadioButton()
        Me.RadioButtonMAINNET = New System.Windows.Forms.RadioButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtExecutable = New System.Windows.Forms.TextBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.lblVarPaymentAddressPreviousValue = New System.Windows.Forms.Label()
        Me.lblVarAmountPreviousValue = New System.Windows.Forms.Label()
        Me.lblVarAmountTokenPreviousValue = New System.Windows.Forms.Label()
        Me.PanelNFT = New System.Windows.Forms.Panel()
        Me.lblNFTID = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.lblNFTFolder = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.lblNFTType = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.btnCloseNFT = New System.Windows.Forms.Button()
        Me.lblNFTAttributes = New System.Windows.Forms.Label()
        Me.lblNFTSRC = New System.Windows.Forms.Label()
        Me.lblNFTImage = New System.Windows.Forms.Label()
        Me.lblNFTName = New System.Windows.Forms.Label()
        Me.lblNFTDescription = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.lstFile = New System.Windows.Forms.ListView()
        Me.txtFile = New System.Windows.Forms.TextBox()
        Me.lstTable = New System.Windows.Forms.ListView()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LoadVariablesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RichTextBox2 = New System.Windows.Forms.RichTextBox()
        Me.txtMetaData = New System.Windows.Forms.TextBox()
        Me.StatusStrip1.SuspendLayout()
        Me.PanelTop.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelNFT.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(39, 17)
        Me.ToolStripStatusLabel1.Text = "Ready"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.ToolStripStatusLabel2})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 746)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1303, 22)
        Me.StatusStrip1.TabIndex = 14
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(1249, 17)
        Me.ToolStripStatusLabel2.Spring = True
        Me.ToolStripStatusLabel2.Text = "Ready"
        Me.ToolStripStatusLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCLI
        '
        Me.lblCLI.AutoSize = True
        Me.lblCLI.ForeColor = System.Drawing.Color.White
        Me.lblCLI.Location = New System.Drawing.Point(264, 92)
        Me.lblCLI.Name = "lblCLI"
        Me.lblCLI.Size = New System.Drawing.Size(141, 13)
        Me.lblCLI.TabIndex = 7
        Me.lblCLI.Text = "Searching for Cardano CLI .."
        '
        'txtParameters
        '
        Me.txtParameters.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtParameters.Location = New System.Drawing.Point(151, 119)
        Me.txtParameters.Name = "txtParameters"
        Me.txtParameters.Size = New System.Drawing.Size(864, 20)
        Me.txtParameters.TabIndex = 4
        Me.txtParameters.Text = "--version"
        '
        'btnExecute
        '
        Me.btnExecute.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExecute.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExecute.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.btnExecute.Location = New System.Drawing.Point(1167, 104)
        Me.btnExecute.Name = "btnExecute"
        Me.btnExecute.Size = New System.Drawing.Size(120, 23)
        Me.btnExecute.TabIndex = 1
        Me.btnExecute.Text = "RUN COMMAND"
        Me.btnExecute.UseVisualStyleBackColor = True
        '
        'PanelTop
        '
        Me.PanelTop.BackColor = System.Drawing.Color.FromArgb(CType(CType(63, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(119, Byte), Integer))
        Me.PanelTop.Controls.Add(Me.btnRunALL)
        Me.PanelTop.Controls.Add(Me.cboScripts)
        Me.PanelTop.Controls.Add(Me.chkLog)
        Me.PanelTop.Controls.Add(Me.lblLineNo)
        Me.PanelTop.Controls.Add(Me.lblVarAmountToken)
        Me.PanelTop.Controls.Add(Me.Label23)
        Me.PanelTop.Controls.Add(Me.lblVarAmountToSendADA)
        Me.PanelTop.Controls.Add(Me.Label22)
        Me.PanelTop.Controls.Add(Me.lblVarTokenToSendAmount)
        Me.PanelTop.Controls.Add(Me.Label21)
        Me.PanelTop.Controls.Add(Me.Label13)
        Me.PanelTop.Controls.Add(Me.lblVarPolicyHash)
        Me.PanelTop.Controls.Add(Me.lblVarAmountToSend)
        Me.PanelTop.Controls.Add(Me.Label19)
        Me.PanelTop.Controls.Add(Me.lblVarLatestSlot)
        Me.PanelTop.Controls.Add(Me.Label18)
        Me.PanelTop.Controls.Add(Me.lblVarLatestBlock)
        Me.PanelTop.Controls.Add(Me.Label17)
        Me.PanelTop.Controls.Add(Me.lblVarTxFee)
        Me.PanelTop.Controls.Add(Me.Label16)
        Me.PanelTop.Controls.Add(Me.lblVarTokenAmountToMint)
        Me.PanelTop.Controls.Add(Me.Label14)
        Me.PanelTop.Controls.Add(Me.lblVarTokenName)
        Me.PanelTop.Controls.Add(Me.Label15)
        Me.PanelTop.Controls.Add(Me.lblVarPolicyID)
        Me.PanelTop.Controls.Add(Me.Label151)
        Me.PanelTop.Controls.Add(Me.Label12)
        Me.PanelTop.Controls.Add(Me.lblVarAmount2)
        Me.PanelTop.Controls.Add(Me.lblVarAddressSendTo)
        Me.PanelTop.Controls.Add(Me.lblVarPaymentAddress)
        Me.PanelTop.Controls.Add(Me.lblVarAmount)
        Me.PanelTop.Controls.Add(Me.lblVarTxIx)
        Me.PanelTop.Controls.Add(Me.lblVarTxHash)
        Me.PanelTop.Controls.Add(Me.Label11)
        Me.PanelTop.Controls.Add(Me.Label10)
        Me.PanelTop.Controls.Add(Me.Label9)
        Me.PanelTop.Controls.Add(Me.Label8)
        Me.PanelTop.Controls.Add(Me.Label7)
        Me.PanelTop.Controls.Add(Me.Label6)
        Me.PanelTop.Controls.Add(Me.lblCLIComments)
        Me.PanelTop.Controls.Add(Me.txtRedirect)
        Me.PanelTop.Controls.Add(Me.Label5)
        Me.PanelTop.Controls.Add(Me.Label4)
        Me.PanelTop.Controls.Add(Me.Label3)
        Me.PanelTop.Controls.Add(Me.lblTestnetID)
        Me.PanelTop.Controls.Add(Me.Label2)
        Me.PanelTop.Controls.Add(Me.RadioButtonTESTNET)
        Me.PanelTop.Controls.Add(Me.RadioButtonMAINNET)
        Me.PanelTop.Controls.Add(Me.Label1)
        Me.PanelTop.Controls.Add(Me.txtExecutable)
        Me.PanelTop.Controls.Add(Me.lblCLI)
        Me.PanelTop.Controls.Add(Me.txtParameters)
        Me.PanelTop.Controls.Add(Me.btnExecute)
        Me.PanelTop.Controls.Add(Me.PictureBox2)
        Me.PanelTop.Controls.Add(Me.PictureBox1)
        Me.PanelTop.Controls.Add(Me.lblVarPaymentAddressPreviousValue)
        Me.PanelTop.Controls.Add(Me.lblVarAmountPreviousValue)
        Me.PanelTop.Controls.Add(Me.lblVarAmountTokenPreviousValue)
        Me.PanelTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelTop.Location = New System.Drawing.Point(0, 24)
        Me.PanelTop.Name = "PanelTop"
        Me.PanelTop.Size = New System.Drawing.Size(1303, 155)
        Me.PanelTop.TabIndex = 13
        '
        'btnRunALL
        '
        Me.btnRunALL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRunALL.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRunALL.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnRunALL.Location = New System.Drawing.Point(1167, 131)
        Me.btnRunALL.Name = "btnRunALL"
        Me.btnRunALL.Size = New System.Drawing.Size(120, 23)
        Me.btnRunALL.TabIndex = 69
        Me.btnRunALL.Text = "RUN ALL"
        Me.btnRunALL.UseVisualStyleBackColor = True
        '
        'cboScripts
        '
        Me.cboScripts.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboScripts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboScripts.FormattingEnabled = True
        Me.cboScripts.Location = New System.Drawing.Point(964, 7)
        Me.cboScripts.Name = "cboScripts"
        Me.cboScripts.Size = New System.Drawing.Size(220, 21)
        Me.cboScripts.TabIndex = 68
        '
        'chkLog
        '
        Me.chkLog.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkLog.AutoSize = True
        Me.chkLog.BackColor = System.Drawing.Color.FromArgb(CType(CType(63, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(119, Byte), Integer))
        Me.chkLog.Checked = True
        Me.chkLog.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkLog.ForeColor = System.Drawing.Color.White
        Me.chkLog.Location = New System.Drawing.Point(1016, 131)
        Me.chkLog.Name = "chkLog"
        Me.chkLog.Size = New System.Drawing.Size(83, 17)
        Me.chkLog.TabIndex = 58
        Me.chkLog.Text = "LOG Output"
        Me.chkLog.UseVisualStyleBackColor = False
        '
        'lblLineNo
        '
        Me.lblLineNo.AutoSize = True
        Me.lblLineNo.ForeColor = System.Drawing.Color.White
        Me.lblLineNo.Location = New System.Drawing.Point(9, 122)
        Me.lblLineNo.Name = "lblLineNo"
        Me.lblLineNo.Size = New System.Drawing.Size(13, 13)
        Me.lblLineNo.TabIndex = 60
        Me.lblLineNo.Text = "?"
        '
        'lblVarAmountToken
        '
        Me.lblVarAmountToken.AutoSize = True
        Me.lblVarAmountToken.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblVarAmountToken.Location = New System.Drawing.Point(649, 83)
        Me.lblVarAmountToken.Name = "lblVarAmountToken"
        Me.lblVarAmountToken.Size = New System.Drawing.Size(63, 13)
        Me.lblVarAmountToken.TabIndex = 54
        Me.lblVarAmountToken.Text = "Token QTY"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.ForeColor = System.Drawing.Color.White
        Me.Label23.Location = New System.Drawing.Point(552, 83)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(90, 13)
        Me.Label23.TabIndex = 53
        Me.Label23.Text = "Extra Token QTY"
        '
        'lblVarAmountToSendADA
        '
        Me.lblVarAmountToSendADA.AutoSize = True
        Me.lblVarAmountToSendADA.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblVarAmountToSendADA.Location = New System.Drawing.Point(1207, 88)
        Me.lblVarAmountToSendADA.Name = "lblVarAmountToSendADA"
        Me.lblVarAmountToSendADA.Size = New System.Drawing.Size(54, 13)
        Me.lblVarAmountToSendADA.TabIndex = 52
        Me.lblVarAmountToSendADA.Text = "ADA QTY"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.ForeColor = System.Drawing.Color.White
        Me.Label22.Location = New System.Drawing.Point(1117, 89)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(82, 13)
        Me.Label22.TabIndex = 51
        Me.Label22.Text = "ADA Send QTY"
        '
        'lblVarTokenToSendAmount
        '
        Me.lblVarTokenToSendAmount.AutoSize = True
        Me.lblVarTokenToSendAmount.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblVarTokenToSendAmount.Location = New System.Drawing.Point(866, 84)
        Me.lblVarTokenToSendAmount.Name = "lblVarTokenToSendAmount"
        Me.lblVarTokenToSendAmount.Size = New System.Drawing.Size(85, 13)
        Me.lblVarTokenToSendAmount.TabIndex = 50
        Me.lblVarTokenToSendAmount.Text = "Token Send Qty"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(744, 84)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(118, 13)
        Me.Label21.TabIndex = 49
        Me.Label21.Text = "Qty of Tokens To Send"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(1137, 53)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(63, 13)
        Me.Label13.TabIndex = 48
        Me.Label13.Text = "Policy Hash"
        '
        'lblVarPolicyHash
        '
        Me.lblVarPolicyHash.AutoSize = True
        Me.lblVarPolicyHash.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblVarPolicyHash.Location = New System.Drawing.Point(1207, 52)
        Me.lblVarPolicyHash.Name = "lblVarPolicyHash"
        Me.lblVarPolicyHash.Size = New System.Drawing.Size(63, 13)
        Me.lblVarPolicyHash.TabIndex = 47
        Me.lblVarPolicyHash.Text = "Policy Hash"
        '
        'lblVarAmountToSend
        '
        Me.lblVarAmountToSend.AutoSize = True
        Me.lblVarAmountToSend.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblVarAmountToSend.Location = New System.Drawing.Point(1207, 72)
        Me.lblVarAmountToSend.Name = "lblVarAmountToSend"
        Me.lblVarAmountToSend.Size = New System.Drawing.Size(76, 13)
        Me.lblVarAmountToSend.TabIndex = 46
        Me.lblVarAmountToSend.Text = "Lovelace QTY"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.ForeColor = System.Drawing.Color.White
        Me.Label19.Location = New System.Drawing.Point(1096, 72)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(104, 13)
        Me.Label19.TabIndex = 45
        Me.Label19.Text = "Lovelace Send QTY"
        '
        'lblVarLatestSlot
        '
        Me.lblVarLatestSlot.AutoSize = True
        Me.lblVarLatestSlot.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblVarLatestSlot.Location = New System.Drawing.Point(1007, 52)
        Me.lblVarLatestSlot.Name = "lblVarLatestSlot"
        Me.lblVarLatestSlot.Size = New System.Drawing.Size(57, 13)
        Me.lblVarLatestSlot.TabIndex = 44
        Me.lblVarLatestSlot.Text = "Latest Slot"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.ForeColor = System.Drawing.Color.White
        Me.Label18.Location = New System.Drawing.Point(974, 64)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(25, 13)
        Me.Label18.TabIndex = 43
        Me.Label18.Text = "Slot"
        '
        'lblVarLatestBlock
        '
        Me.lblVarLatestBlock.AutoSize = True
        Me.lblVarLatestBlock.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblVarLatestBlock.Location = New System.Drawing.Point(1006, 35)
        Me.lblVarLatestBlock.Name = "lblVarLatestBlock"
        Me.lblVarLatestBlock.Size = New System.Drawing.Size(66, 13)
        Me.lblVarLatestBlock.TabIndex = 42
        Me.lblVarLatestBlock.Text = "Latest Block"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.ForeColor = System.Drawing.Color.White
        Me.Label17.Location = New System.Drawing.Point(967, 47)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(34, 13)
        Me.Label17.TabIndex = 41
        Me.Label17.Text = "Block"
        '
        'lblVarTxFee
        '
        Me.lblVarTxFee.AutoSize = True
        Me.lblVarTxFee.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblVarTxFee.Location = New System.Drawing.Point(1006, 71)
        Me.lblVarTxFee.Name = "lblVarTxFee"
        Me.lblVarTxFee.Size = New System.Drawing.Size(37, 13)
        Me.lblVarTxFee.TabIndex = 40
        Me.lblVarTxFee.Text = "TxFee"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.ForeColor = System.Drawing.Color.White
        Me.Label16.Location = New System.Drawing.Point(961, 83)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(40, 13)
        Me.Label16.TabIndex = 39
        Me.Label16.Text = "Tx Fee"
        '
        'lblVarTokenAmountToMint
        '
        Me.lblVarTokenAmountToMint.AutoSize = True
        Me.lblVarTokenAmountToMint.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblVarTokenAmountToMint.Location = New System.Drawing.Point(866, 65)
        Me.lblVarTokenAmountToMint.Name = "lblVarTokenAmountToMint"
        Me.lblVarTokenAmountToMint.Size = New System.Drawing.Size(80, 13)
        Me.lblVarTokenAmountToMint.TabIndex = 38
        Me.lblVarTokenAmountToMint.Text = "Token Mint Qty"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.Location = New System.Drawing.Point(747, 65)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(113, 13)
        Me.Label14.TabIndex = 37
        Me.Label14.Text = "Qty of Tokens To Mint"
        '
        'lblVarTokenName
        '
        Me.lblVarTokenName.AutoSize = True
        Me.lblVarTokenName.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblVarTokenName.Location = New System.Drawing.Point(866, 46)
        Me.lblVarTokenName.Name = "lblVarTokenName"
        Me.lblVarTokenName.Size = New System.Drawing.Size(69, 13)
        Me.lblVarTokenName.TabIndex = 36
        Me.lblVarTokenName.Text = "Token Name"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(791, 46)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(69, 13)
        Me.Label15.TabIndex = 35
        Me.Label15.Text = "Token Name"
        '
        'lblVarPolicyID
        '
        Me.lblVarPolicyID.AutoSize = True
        Me.lblVarPolicyID.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblVarPolicyID.Location = New System.Drawing.Point(1207, 35)
        Me.lblVarPolicyID.Name = "lblVarPolicyID"
        Me.lblVarPolicyID.Size = New System.Drawing.Size(49, 13)
        Me.lblVarPolicyID.TabIndex = 34
        Me.lblVarPolicyID.Text = "Policy ID"
        '
        'Label151
        '
        Me.Label151.AutoSize = True
        Me.Label151.ForeColor = System.Drawing.Color.White
        Me.Label151.Location = New System.Drawing.Point(1151, 35)
        Me.Label151.Name = "Label151"
        Me.Label151.Size = New System.Drawing.Size(49, 13)
        Me.Label151.TabIndex = 33
        Me.Label151.Text = "Policy ID"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(588, 65)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(54, 13)
        Me.Label12.TabIndex = 32
        Me.Label12.Text = "ADA QTY"
        '
        'lblVarAmount2
        '
        Me.lblVarAmount2.AutoSize = True
        Me.lblVarAmount2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblVarAmount2.Location = New System.Drawing.Point(650, 65)
        Me.lblVarAmount2.Name = "lblVarAmount2"
        Me.lblVarAmount2.Size = New System.Drawing.Size(54, 13)
        Me.lblVarAmount2.TabIndex = 31
        Me.lblVarAmount2.Text = "ADA QTY"
        '
        'lblVarAddressSendTo
        '
        Me.lblVarAddressSendTo.AutoSize = True
        Me.lblVarAddressSendTo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblVarAddressSendTo.Location = New System.Drawing.Point(650, 27)
        Me.lblVarAddressSendTo.Name = "lblVarAddressSendTo"
        Me.lblVarAddressSendTo.Size = New System.Drawing.Size(89, 13)
        Me.lblVarAddressSendTo.TabIndex = 30
        Me.lblVarAddressSendTo.Text = "Send To Address"
        '
        'lblVarPaymentAddress
        '
        Me.lblVarPaymentAddress.AutoSize = True
        Me.lblVarPaymentAddress.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblVarPaymentAddress.Location = New System.Drawing.Point(650, 8)
        Me.lblVarPaymentAddress.Name = "lblVarPaymentAddress"
        Me.lblVarPaymentAddress.Size = New System.Drawing.Size(89, 13)
        Me.lblVarPaymentAddress.TabIndex = 29
        Me.lblVarPaymentAddress.Text = "Payment Address"
        '
        'lblVarAmount
        '
        Me.lblVarAmount.AutoSize = True
        Me.lblVarAmount.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblVarAmount.Location = New System.Drawing.Point(650, 46)
        Me.lblVarAmount.Name = "lblVarAmount"
        Me.lblVarAmount.Size = New System.Drawing.Size(76, 13)
        Me.lblVarAmount.TabIndex = 28
        Me.lblVarAmount.Text = "Lovelace QTY"
        '
        'lblVarTxIx
        '
        Me.lblVarTxIx.AutoSize = True
        Me.lblVarTxIx.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblVarTxIx.Location = New System.Drawing.Point(569, 103)
        Me.lblVarTxIx.Name = "lblVarTxIx"
        Me.lblVarTxIx.Size = New System.Drawing.Size(27, 13)
        Me.lblVarTxIx.TabIndex = 27
        Me.lblVarTxIx.Text = "TxIx"
        '
        'lblVarTxHash
        '
        Me.lblVarTxHash.AutoSize = True
        Me.lblVarTxHash.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblVarTxHash.Location = New System.Drawing.Point(649, 103)
        Me.lblVarTxHash.Name = "lblVarTxHash"
        Me.lblVarTxHash.Size = New System.Drawing.Size(44, 13)
        Me.lblVarTxHash.TabIndex = 26
        Me.lblVarTxHash.Text = "TxHash"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(553, 27)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(89, 13)
        Me.Label11.TabIndex = 25
        Me.Label11.Text = "Send To Address"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(553, 8)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(89, 13)
        Me.Label10.TabIndex = 24
        Me.Label10.Text = "Payment Address"
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Yellow
        Me.Label9.Location = New System.Drawing.Point(451, 7)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(100, 41)
        Me.Label9.TabIndex = 23
        Me.Label9.Text = "VARIABLES      (F2 to populate)"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(566, 46)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(76, 13)
        Me.Label8.TabIndex = 22
        Me.Label8.Text = "Lovelace QTY"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(535, 103)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(27, 13)
        Me.Label7.TabIndex = 21
        Me.Label7.Text = "TxIx"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(598, 103)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(44, 13)
        Me.Label6.TabIndex = 20
        Me.Label6.Text = "TxHash"
        '
        'lblCLIComments
        '
        Me.lblCLIComments.AutoSize = True
        Me.lblCLIComments.ForeColor = System.Drawing.Color.Yellow
        Me.lblCLIComments.Location = New System.Drawing.Point(32, 140)
        Me.lblCLIComments.Name = "lblCLIComments"
        Me.lblCLIComments.Size = New System.Drawing.Size(69, 13)
        Me.lblCLIComments.TabIndex = 19
        Me.lblCLIComments.Text = "COMMENTS"
        '
        'txtRedirect
        '
        Me.txtRedirect.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtRedirect.Location = New System.Drawing.Point(1017, 107)
        Me.txtRedirect.Name = "txtRedirect"
        Me.txtRedirect.Size = New System.Drawing.Size(144, 20)
        Me.txtRedirect.TabIndex = 18
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(1013, 91)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(101, 13)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "Redirect Output File"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(151, 103)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(73, 13)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "CLI Command"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(264, 58)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(74, 13)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "TESTNET ID:"
        '
        'lblTestnetID
        '
        Me.lblTestnetID.AutoSize = True
        Me.lblTestnetID.ForeColor = System.Drawing.Color.White
        Me.lblTestnetID.Location = New System.Drawing.Point(344, 58)
        Me.lblTestnetID.Name = "lblTestnetID"
        Me.lblTestnetID.Size = New System.Drawing.Size(13, 13)
        Me.lblTestnetID.TabIndex = 14
        Me.lblTestnetID.Text = "?"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(264, 76)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(112, 13)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "CLI SOCKET TO USE"
        '
        'RadioButtonTESTNET
        '
        Me.RadioButtonTESTNET.AutoSize = True
        Me.RadioButtonTESTNET.ForeColor = System.Drawing.Color.Salmon
        Me.RadioButtonTESTNET.Location = New System.Drawing.Point(263, 33)
        Me.RadioButtonTESTNET.Name = "RadioButtonTESTNET"
        Me.RadioButtonTESTNET.Size = New System.Drawing.Size(75, 17)
        Me.RadioButtonTESTNET.TabIndex = 11
        Me.RadioButtonTESTNET.TabStop = True
        Me.RadioButtonTESTNET.Text = "TESTNET"
        Me.RadioButtonTESTNET.UseVisualStyleBackColor = True
        '
        'RadioButtonMAINNET
        '
        Me.RadioButtonMAINNET.AutoSize = True
        Me.RadioButtonMAINNET.ForeColor = System.Drawing.Color.GreenYellow
        Me.RadioButtonMAINNET.Location = New System.Drawing.Point(264, 10)
        Me.RadioButtonMAINNET.Name = "RadioButtonMAINNET"
        Me.RadioButtonMAINNET.Size = New System.Drawing.Size(74, 17)
        Me.RadioButtonMAINNET.TabIndex = 10
        Me.RadioButtonMAINNET.TabStop = True
        Me.RadioButtonMAINNET.Text = "MAINNET"
        Me.RadioButtonMAINNET.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(31, 103)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 13)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "EXEcutable"
        '
        'txtExecutable
        '
        Me.txtExecutable.Location = New System.Drawing.Point(34, 119)
        Me.txtExecutable.Name = "txtExecutable"
        Me.txtExecutable.Size = New System.Drawing.Size(111, 20)
        Me.txtExecutable.TabIndex = 8
        Me.txtExecutable.Text = "cardano-cli.exe"
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.Image = Global.CardanoBlockChainExamples.My.Resources.Resources.cardano_logo_white
        Me.PictureBox2.Location = New System.Drawing.Point(1188, -11)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(127, 70)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox2.TabIndex = 3
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.CardanoBlockChainExamples.My.Resources.Resources.catalyst_logo
        Me.PictureBox1.Location = New System.Drawing.Point(-6, -50)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 179)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'lblVarPaymentAddressPreviousValue
        '
        Me.lblVarPaymentAddressPreviousValue.AutoSize = True
        Me.lblVarPaymentAddressPreviousValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVarPaymentAddressPreviousValue.ForeColor = System.Drawing.Color.Thistle
        Me.lblVarPaymentAddressPreviousValue.Location = New System.Drawing.Point(451, 84)
        Me.lblVarPaymentAddressPreviousValue.Name = "lblVarPaymentAddressPreviousValue"
        Me.lblVarPaymentAddressPreviousValue.Size = New System.Drawing.Size(154, 13)
        Me.lblVarPaymentAddressPreviousValue.TabIndex = 67
        Me.lblVarPaymentAddressPreviousValue.Text = "PaymentAddressPreviousValue"
        Me.lblVarPaymentAddressPreviousValue.Visible = False
        '
        'lblVarAmountPreviousValue
        '
        Me.lblVarAmountPreviousValue.AutoSize = True
        Me.lblVarAmountPreviousValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVarAmountPreviousValue.ForeColor = System.Drawing.Color.Thistle
        Me.lblVarAmountPreviousValue.Location = New System.Drawing.Point(451, 52)
        Me.lblVarAmountPreviousValue.Name = "lblVarAmountPreviousValue"
        Me.lblVarAmountPreviousValue.Size = New System.Drawing.Size(75, 13)
        Me.lblVarAmountPreviousValue.TabIndex = 66
        Me.lblVarAmountPreviousValue.Text = "PreviousValue"
        Me.lblVarAmountPreviousValue.Visible = False
        '
        'lblVarAmountTokenPreviousValue
        '
        Me.lblVarAmountTokenPreviousValue.AutoSize = True
        Me.lblVarAmountTokenPreviousValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVarAmountTokenPreviousValue.ForeColor = System.Drawing.Color.Thistle
        Me.lblVarAmountTokenPreviousValue.Location = New System.Drawing.Point(451, 70)
        Me.lblVarAmountTokenPreviousValue.Name = "lblVarAmountTokenPreviousValue"
        Me.lblVarAmountTokenPreviousValue.Size = New System.Drawing.Size(106, 13)
        Me.lblVarAmountTokenPreviousValue.TabIndex = 65
        Me.lblVarAmountTokenPreviousValue.Text = "TokenPreviousValue"
        Me.lblVarAmountTokenPreviousValue.Visible = False
        '
        'PanelNFT
        '
        Me.PanelNFT.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelNFT.BackColor = System.Drawing.Color.Black
        Me.PanelNFT.Controls.Add(Me.lblNFTID)
        Me.PanelNFT.Controls.Add(Me.Label31)
        Me.PanelNFT.Controls.Add(Me.lblNFTFolder)
        Me.PanelNFT.Controls.Add(Me.Label30)
        Me.PanelNFT.Controls.Add(Me.lblNFTType)
        Me.PanelNFT.Controls.Add(Me.Label29)
        Me.PanelNFT.Controls.Add(Me.btnCloseNFT)
        Me.PanelNFT.Controls.Add(Me.lblNFTAttributes)
        Me.PanelNFT.Controls.Add(Me.lblNFTSRC)
        Me.PanelNFT.Controls.Add(Me.lblNFTImage)
        Me.PanelNFT.Controls.Add(Me.lblNFTName)
        Me.PanelNFT.Controls.Add(Me.lblNFTDescription)
        Me.PanelNFT.Controls.Add(Me.Label20)
        Me.PanelNFT.Controls.Add(Me.Label24)
        Me.PanelNFT.Controls.Add(Me.Label25)
        Me.PanelNFT.Controls.Add(Me.Label26)
        Me.PanelNFT.Controls.Add(Me.Label27)
        Me.PanelNFT.Location = New System.Drawing.Point(-18, 584)
        Me.PanelNFT.Name = "PanelNFT"
        Me.PanelNFT.Size = New System.Drawing.Size(299, 152)
        Me.PanelNFT.TabIndex = 61
        '
        'lblNFTID
        '
        Me.lblNFTID.AutoSize = True
        Me.lblNFTID.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblNFTID.Location = New System.Drawing.Point(66, 138)
        Me.lblNFTID.Name = "lblNFTID"
        Me.lblNFTID.Size = New System.Drawing.Size(42, 13)
        Me.lblNFTID.TabIndex = 70
        Me.lblNFTID.Text = "NFT ID"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.ForeColor = System.Drawing.Color.White
        Me.Label31.Location = New System.Drawing.Point(46, 137)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(18, 13)
        Me.Label31.TabIndex = 69
        Me.Label31.Text = "ID"
        '
        'lblNFTFolder
        '
        Me.lblNFTFolder.AutoSize = True
        Me.lblNFTFolder.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblNFTFolder.Location = New System.Drawing.Point(66, 6)
        Me.lblNFTFolder.Name = "lblNFTFolder"
        Me.lblNFTFolder.Size = New System.Drawing.Size(60, 13)
        Me.lblNFTFolder.TabIndex = 68
        Me.lblNFTFolder.Text = "NFT Folder"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.ForeColor = System.Drawing.Color.White
        Me.Label30.Location = New System.Drawing.Point(28, 5)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(36, 13)
        Me.Label30.TabIndex = 67
        Me.Label30.Text = "Folder"
        '
        'lblNFTType
        '
        Me.lblNFTType.AutoSize = True
        Me.lblNFTType.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblNFTType.Location = New System.Drawing.Point(67, 82)
        Me.lblNFTType.Name = "lblNFTType"
        Me.lblNFTType.Size = New System.Drawing.Size(55, 13)
        Me.lblNFTType.TabIndex = 66
        Me.lblNFTType.Text = "NFT Type"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.ForeColor = System.Drawing.Color.White
        Me.Label29.Location = New System.Drawing.Point(33, 81)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(31, 13)
        Me.Label29.TabIndex = 65
        Me.Label29.Text = "Type"
        '
        'btnCloseNFT
        '
        Me.btnCloseNFT.Location = New System.Drawing.Point(267, 5)
        Me.btnCloseNFT.Name = "btnCloseNFT"
        Me.btnCloseNFT.Size = New System.Drawing.Size(24, 23)
        Me.btnCloseNFT.TabIndex = 64
        Me.btnCloseNFT.Text = "x"
        Me.btnCloseNFT.UseVisualStyleBackColor = True
        '
        'lblNFTAttributes
        '
        Me.lblNFTAttributes.AutoSize = True
        Me.lblNFTAttributes.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblNFTAttributes.Location = New System.Drawing.Point(66, 121)
        Me.lblNFTAttributes.Name = "lblNFTAttributes"
        Me.lblNFTAttributes.Size = New System.Drawing.Size(75, 13)
        Me.lblNFTAttributes.TabIndex = 63
        Me.lblNFTAttributes.Text = "NFT Attributes"
        '
        'lblNFTSRC
        '
        Me.lblNFTSRC.AutoSize = True
        Me.lblNFTSRC.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblNFTSRC.Location = New System.Drawing.Point(67, 103)
        Me.lblNFTSRC.Name = "lblNFTSRC"
        Me.lblNFTSRC.Size = New System.Drawing.Size(53, 13)
        Me.lblNFTSRC.TabIndex = 62
        Me.lblNFTSRC.Text = "NFT SRC"
        '
        'lblNFTImage
        '
        Me.lblNFTImage.AutoSize = True
        Me.lblNFTImage.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblNFTImage.Location = New System.Drawing.Point(67, 43)
        Me.lblNFTImage.Name = "lblNFTImage"
        Me.lblNFTImage.Size = New System.Drawing.Size(60, 13)
        Me.lblNFTImage.TabIndex = 61
        Me.lblNFTImage.Text = "NFT Image"
        '
        'lblNFTName
        '
        Me.lblNFTName.AutoSize = True
        Me.lblNFTName.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblNFTName.Location = New System.Drawing.Point(67, 24)
        Me.lblNFTName.Name = "lblNFTName"
        Me.lblNFTName.Size = New System.Drawing.Size(59, 13)
        Me.lblNFTName.TabIndex = 60
        Me.lblNFTName.Text = "NFT Name"
        '
        'lblNFTDescription
        '
        Me.lblNFTDescription.AutoSize = True
        Me.lblNFTDescription.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblNFTDescription.Location = New System.Drawing.Point(67, 62)
        Me.lblNFTDescription.Name = "lblNFTDescription"
        Me.lblNFTDescription.Size = New System.Drawing.Size(84, 13)
        Me.lblNFTDescription.TabIndex = 59
        Me.lblNFTDescription.Text = "NFT Description"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.ForeColor = System.Drawing.Color.White
        Me.Label20.Location = New System.Drawing.Point(13, 120)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(51, 13)
        Me.Label20.TabIndex = 58
        Me.Label20.Text = "Attributes"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.ForeColor = System.Drawing.Color.White
        Me.Label24.Location = New System.Drawing.Point(35, 102)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(29, 13)
        Me.Label24.TabIndex = 57
        Me.Label24.Text = "SRC"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.ForeColor = System.Drawing.Color.White
        Me.Label25.Location = New System.Drawing.Point(28, 42)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(36, 13)
        Me.Label25.TabIndex = 56
        Me.Label25.Text = "Image"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.ForeColor = System.Drawing.Color.White
        Me.Label26.Location = New System.Drawing.Point(29, 23)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(35, 13)
        Me.Label26.TabIndex = 55
        Me.Label26.Text = "Name"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.ForeColor = System.Drawing.Color.White
        Me.Label27.Location = New System.Drawing.Point(4, 61)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(60, 13)
        Me.Label27.TabIndex = 54
        Me.Label27.Text = "Description"
        '
        'RichTextBox1
        '
        Me.RichTextBox1.BackColor = System.Drawing.Color.Black
        Me.RichTextBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RichTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox1.ForeColor = System.Drawing.Color.White
        Me.RichTextBox1.Location = New System.Drawing.Point(0, 179)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.Size = New System.Drawing.Size(305, 414)
        Me.RichTextBox1.TabIndex = 15
        Me.RichTextBox1.Text = ""
        Me.RichTextBox1.Visible = False
        '
        'lstFile
        '
        Me.lstFile.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lstFile.Dock = System.Windows.Forms.DockStyle.Right
        Me.lstFile.FullRowSelect = True
        Me.lstFile.HideSelection = False
        Me.lstFile.Location = New System.Drawing.Point(305, 179)
        Me.lstFile.MultiSelect = False
        Me.lstFile.Name = "lstFile"
        Me.lstFile.Size = New System.Drawing.Size(998, 567)
        Me.lstFile.TabIndex = 16
        Me.lstFile.UseCompatibleStateImageBehavior = False
        '
        'txtFile
        '
        Me.txtFile.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.txtFile.Location = New System.Drawing.Point(0, 593)
        Me.txtFile.Multiline = True
        Me.txtFile.Name = "txtFile"
        Me.txtFile.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtFile.Size = New System.Drawing.Size(305, 153)
        Me.txtFile.TabIndex = 17
        '
        'lstTable
        '
        Me.lstTable.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lstTable.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lstTable.FullRowSelect = True
        Me.lstTable.HideSelection = False
        Me.lstTable.Location = New System.Drawing.Point(0, 267)
        Me.lstTable.Name = "lstTable"
        Me.lstTable.Size = New System.Drawing.Size(305, 164)
        Me.lstTable.TabIndex = 18
        Me.lstTable.UseCompatibleStateImageBehavior = False
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1303, 24)
        Me.MenuStrip1.TabIndex = 19
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LoadVariablesToolStripMenuItem, Me.ToolStripMenuItem1, Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        '
        'LoadVariablesToolStripMenuItem
        '
        Me.LoadVariablesToolStripMenuItem.Name = "LoadVariablesToolStripMenuItem"
        Me.LoadVariablesToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2
        Me.LoadVariablesToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.LoadVariablesToolStripMenuItem.Text = "&Load Variables"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(165, 6)
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.F4), System.Windows.Forms.Keys)
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.ExitToolStripMenuItem.Text = "E&xit"
        '
        'RichTextBox2
        '
        Me.RichTextBox2.BackColor = System.Drawing.Color.Black
        Me.RichTextBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RichTextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox2.ForeColor = System.Drawing.Color.White
        Me.RichTextBox2.Location = New System.Drawing.Point(0, 179)
        Me.RichTextBox2.Name = "RichTextBox2"
        Me.RichTextBox2.Size = New System.Drawing.Size(305, 88)
        Me.RichTextBox2.TabIndex = 20
        Me.RichTextBox2.Text = ""
        '
        'txtMetaData
        '
        Me.txtMetaData.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtMetaData.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.txtMetaData.Location = New System.Drawing.Point(0, 431)
        Me.txtMetaData.Multiline = True
        Me.txtMetaData.Name = "txtMetaData"
        Me.txtMetaData.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtMetaData.Size = New System.Drawing.Size(305, 162)
        Me.txtMetaData.TabIndex = 21
        '
        'frmCardanoCLI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1303, 768)
        Me.Controls.Add(Me.RichTextBox2)
        Me.Controls.Add(Me.PanelNFT)
        Me.Controls.Add(Me.lstTable)
        Me.Controls.Add(Me.txtMetaData)
        Me.Controls.Add(Me.RichTextBox1)
        Me.Controls.Add(Me.txtFile)
        Me.Controls.Add(Me.lstFile)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.PanelTop)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmCardanoCLI"
        Me.Text = "Cardano CLI viewing - cli-samples.txt"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.PanelTop.ResumeLayout(False)
        Me.PanelTop.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelNFT.ResumeLayout(False)
        Me.PanelNFT.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStripStatusLabel1 As ToolStripStatusLabel
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents lblCLI As Label
    Friend WithEvents txtParameters As TextBox
    Friend WithEvents Timer1 As Timer
    Friend WithEvents btnExecute As Button
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents PanelTop As Panel
    Friend WithEvents RichTextBox1 As RichTextBox
    Friend WithEvents lstFile As ListView
    Friend WithEvents txtExecutable As TextBox
    Friend WithEvents txtFile As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents ToolStripStatusLabel2 As ToolStripStatusLabel
    Friend WithEvents RadioButtonTESTNET As RadioButton
    Friend WithEvents RadioButtonMAINNET As RadioButton
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents lblTestnetID As Label
    Friend WithEvents txtRedirect As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents lblCLIComments As Label
    Friend WithEvents lstTable As ListView
    Friend WithEvents lblVarAmount2 As Label
    Friend WithEvents lblVarAddressSendTo As Label
    Friend WithEvents lblVarPaymentAddress As Label
    Friend WithEvents lblVarAmount As Label
    Friend WithEvents lblVarTxIx As Label
    Friend WithEvents lblVarTxHash As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents lblVarPolicyID As Label
    Friend WithEvents Label151 As Label
    Friend WithEvents lblVarTokenName As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents lblVarTokenAmountToMint As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents FileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents LoadVariablesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RichTextBox2 As RichTextBox
    Friend WithEvents lblVarTxFee As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents lblVarLatestBlock As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents lblVarLatestSlot As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents lblVarAmountToSend As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents lblVarPolicyHash As Label
    Friend WithEvents txtMetaData As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents lblVarTokenToSendAmount As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents lblVarAmountToSendADA As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents lblVarAmountToken As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents lblLineNo As Label
    Friend WithEvents chkLog As CheckBox
    Friend WithEvents PanelNFT As Panel
    Friend WithEvents btnCloseNFT As Button
    Friend WithEvents lblNFTAttributes As Label
    Friend WithEvents lblNFTSRC As Label
    Friend WithEvents lblNFTImage As Label
    Friend WithEvents lblNFTName As Label
    Friend WithEvents lblNFTDescription As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents Label26 As Label
    Friend WithEvents Label27 As Label
    Friend WithEvents lblNFTType As Label
    Friend WithEvents Label29 As Label
    Friend WithEvents lblNFTFolder As Label
    Friend WithEvents Label30 As Label
    Friend WithEvents lblNFTID As Label
    Friend WithEvents Label31 As Label
    Friend WithEvents lblVarPaymentAddressPreviousValue As Label
    Friend WithEvents lblVarAmountPreviousValue As Label
    Friend WithEvents lblVarAmountTokenPreviousValue As Label
    Friend WithEvents cboScripts As ComboBox
    Friend WithEvents btnRunALL As Button
End Class

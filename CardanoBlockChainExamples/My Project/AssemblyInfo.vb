﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Visual Studio Smart Contract Plugin")>
<Assembly: AssemblyDescription("Project Catalyst - Funded in Project 3 - FREE Open Source code to the Cardano and wider community - https://bitbucket.org/robgreig/visual-studio-smart-contract-plugin/src/master/")>
<Assembly: AssemblyCompany("Rob Greig - Cardano Community Member")>
<Assembly: AssemblyProduct("Visual Studio Smart Contract Plugin")>
<Assembly: AssemblyCopyright("This Project is Open Source")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("deccf608-3a49-40cf-931d-9d2cc582cd51")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("2.0.0.1")>
<Assembly: AssemblyFileVersion("2.0.0.1")>

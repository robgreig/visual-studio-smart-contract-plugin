﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNFTGallery
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmNFTGallery))
        Me.PanelTop = New System.Windows.Forms.Panel()
        Me.radioButtonList = New System.Windows.Forms.RadioButton()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.radioButtonSmall = New System.Windows.Forms.RadioButton()
        Me.NumericUpDownMaxAssets = New System.Windows.Forms.NumericUpDown()
        Me.radioButtonLarge = New System.Windows.Forms.RadioButton()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.lstAssets = New System.Windows.Forms.ListView()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.PanelTop.SuspendLayout()
        CType(Me.NumericUpDownMaxAssets, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelTop
        '
        Me.PanelTop.BackColor = System.Drawing.Color.FromArgb(CType(CType(63, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(119, Byte), Integer))
        Me.PanelTop.Controls.Add(Me.radioButtonList)
        Me.PanelTop.Controls.Add(Me.Label3)
        Me.PanelTop.Controls.Add(Me.radioButtonSmall)
        Me.PanelTop.Controls.Add(Me.NumericUpDownMaxAssets)
        Me.PanelTop.Controls.Add(Me.radioButtonLarge)
        Me.PanelTop.Controls.Add(Me.PictureBox2)
        Me.PanelTop.Controls.Add(Me.PictureBox1)
        Me.PanelTop.Controls.Add(Me.btnRefresh)
        Me.PanelTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelTop.Location = New System.Drawing.Point(0, 0)
        Me.PanelTop.Name = "PanelTop"
        Me.PanelTop.Size = New System.Drawing.Size(800, 74)
        Me.PanelTop.TabIndex = 1
        '
        'radioButtonList
        '
        Me.radioButtonList.AutoSize = True
        Me.radioButtonList.ForeColor = System.Drawing.Color.White
        Me.radioButtonList.Location = New System.Drawing.Point(505, 6)
        Me.radioButtonList.Name = "radioButtonList"
        Me.radioButtonList.Size = New System.Drawing.Size(41, 17)
        Me.radioButtonList.TabIndex = 6
        Me.radioButtonList.TabStop = True
        Me.radioButtonList.Text = "List"
        Me.radioButtonList.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(248, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(138, 13)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "Max Assets to be Returned:"
        '
        'radioButtonSmall
        '
        Me.radioButtonSmall.AutoSize = True
        Me.radioButtonSmall.ForeColor = System.Drawing.Color.White
        Me.radioButtonSmall.Location = New System.Drawing.Point(505, 27)
        Me.radioButtonSmall.Name = "radioButtonSmall"
        Me.radioButtonSmall.Size = New System.Drawing.Size(50, 17)
        Me.radioButtonSmall.TabIndex = 5
        Me.radioButtonSmall.TabStop = True
        Me.radioButtonSmall.Text = "Small"
        Me.radioButtonSmall.UseVisualStyleBackColor = True
        '
        'NumericUpDownMaxAssets
        '
        Me.NumericUpDownMaxAssets.Location = New System.Drawing.Point(392, 7)
        Me.NumericUpDownMaxAssets.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.NumericUpDownMaxAssets.Name = "NumericUpDownMaxAssets"
        Me.NumericUpDownMaxAssets.Size = New System.Drawing.Size(49, 20)
        Me.NumericUpDownMaxAssets.TabIndex = 17
        Me.NumericUpDownMaxAssets.Value = New Decimal(New Integer() {30, 0, 0, 0})
        '
        'radioButtonLarge
        '
        Me.radioButtonLarge.AutoSize = True
        Me.radioButtonLarge.ForeColor = System.Drawing.Color.White
        Me.radioButtonLarge.Location = New System.Drawing.Point(505, 45)
        Me.radioButtonLarge.Name = "radioButtonLarge"
        Me.radioButtonLarge.Size = New System.Drawing.Size(52, 17)
        Me.radioButtonLarge.TabIndex = 4
        Me.radioButtonLarge.TabStop = True
        Me.radioButtonLarge.Text = "Large"
        Me.radioButtonLarge.UseVisualStyleBackColor = True
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.Image = Global.CardanoBlockChainExamples.My.Resources.Resources.cardano_logo_white
        Me.PictureBox2.Location = New System.Drawing.Point(685, 1)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(127, 70)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox2.TabIndex = 3
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.CardanoBlockChainExamples.My.Resources.Resources.catalyst_logo
        Me.PictureBox1.Location = New System.Drawing.Point(-6, -50)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 179)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'btnRefresh
        '
        Me.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRefresh.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.btnRefresh.Location = New System.Drawing.Point(251, 42)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(104, 23)
        Me.btnRefresh.TabIndex = 1
        Me.btnRefresh.Text = "Refresh"
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'lstAssets
        '
        Me.lstAssets.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstAssets.FullRowSelect = True
        Me.lstAssets.HideSelection = False
        Me.lstAssets.Location = New System.Drawing.Point(0, 74)
        Me.lstAssets.MultiSelect = False
        Me.lstAssets.Name = "lstAssets"
        Me.lstAssets.Size = New System.Drawing.Size(800, 376)
        Me.lstAssets.TabIndex = 7
        Me.lstAssets.UseCompatibleStateImageBehavior = False
        '
        'Timer1
        '
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 428)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(800, 22)
        Me.StatusStrip1.TabIndex = 8
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(39, 17)
        Me.ToolStripStatusLabel1.Text = "Ready"
        '
        'frmNFTGallery
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.lstAssets)
        Me.Controls.Add(Me.PanelTop)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmNFTGallery"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "NFT Gallery"
        Me.PanelTop.ResumeLayout(False)
        Me.PanelTop.PerformLayout()
        CType(Me.NumericUpDownMaxAssets, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PanelTop As Panel
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents btnRefresh As Button
    Friend WithEvents lstAssets As ListView
    Friend WithEvents radioButtonList As RadioButton
    Friend WithEvents radioButtonSmall As RadioButton
    Friend WithEvents radioButtonLarge As RadioButton
    Friend WithEvents Label3 As Label
    Friend WithEvents NumericUpDownMaxAssets As NumericUpDown
    Friend WithEvents Timer1 As Timer
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As ToolStripStatusLabel
End Class

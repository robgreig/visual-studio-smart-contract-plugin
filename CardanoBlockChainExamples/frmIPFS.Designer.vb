﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmIPFS
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmIPFS))
        Me.PanelTop = New System.Windows.Forms.Panel()
        Me.txtIPFSPath = New System.Windows.Forms.TextBox()
        Me.lblIPFS = New System.Windows.Forms.Label()
        Me.chkPinForever = New System.Windows.Forms.CheckBox()
        Me.PanelIPFS = New System.Windows.Forms.Panel()
        Me.btnValidateIPFS = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnFindFile = New System.Windows.Forms.Button()
        Me.txtFilename = New System.Windows.Forms.TextBox()
        Me.btnUpload = New System.Windows.Forms.Button()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.PanelRightImage = New System.Windows.Forms.Panel()
        Me.PictureBoxAssetIPFS = New System.Windows.Forms.PictureBox()
        Me.lblPolicyIPFS = New System.Windows.Forms.Label()
        Me.PanelTop.SuspendLayout()
        Me.PanelIPFS.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.PanelRightImage.SuspendLayout()
        CType(Me.PictureBoxAssetIPFS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelTop
        '
        Me.PanelTop.BackColor = System.Drawing.Color.FromArgb(CType(CType(63, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(119, Byte), Integer))
        Me.PanelTop.Controls.Add(Me.txtIPFSPath)
        Me.PanelTop.Controls.Add(Me.lblIPFS)
        Me.PanelTop.Controls.Add(Me.chkPinForever)
        Me.PanelTop.Controls.Add(Me.PanelIPFS)
        Me.PanelTop.Controls.Add(Me.Label1)
        Me.PanelTop.Controls.Add(Me.btnFindFile)
        Me.PanelTop.Controls.Add(Me.txtFilename)
        Me.PanelTop.Controls.Add(Me.btnUpload)
        Me.PanelTop.Controls.Add(Me.PictureBox2)
        Me.PanelTop.Controls.Add(Me.PictureBox1)
        Me.PanelTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelTop.Location = New System.Drawing.Point(0, 0)
        Me.PanelTop.Name = "PanelTop"
        Me.PanelTop.Size = New System.Drawing.Size(780, 288)
        Me.PanelTop.TabIndex = 9
        '
        'txtIPFSPath
        '
        Me.txtIPFSPath.Location = New System.Drawing.Point(26, 225)
        Me.txtIPFSPath.Name = "txtIPFSPath"
        Me.txtIPFSPath.Size = New System.Drawing.Size(545, 20)
        Me.txtIPFSPath.TabIndex = 6
        Me.txtIPFSPath.Visible = False
        '
        'lblIPFS
        '
        Me.lblIPFS.AutoSize = True
        Me.lblIPFS.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIPFS.ForeColor = System.Drawing.Color.White
        Me.lblIPFS.Location = New System.Drawing.Point(22, 196)
        Me.lblIPFS.Name = "lblIPFS"
        Me.lblIPFS.Size = New System.Drawing.Size(300, 20)
        Me.lblIPFS.TabIndex = 90
        Me.lblIPFS.Text = "IPFS Link (https://ipfs.blockfrost.dev/ipfs/)"
        '
        'chkPinForever
        '
        Me.chkPinForever.AutoSize = True
        Me.chkPinForever.ForeColor = System.Drawing.Color.White
        Me.chkPinForever.Location = New System.Drawing.Point(456, 155)
        Me.chkPinForever.Name = "chkPinForever"
        Me.chkPinForever.Size = New System.Drawing.Size(83, 17)
        Me.chkPinForever.TabIndex = 89
        Me.chkPinForever.Text = "PIN Forever"
        Me.chkPinForever.UseVisualStyleBackColor = True
        '
        'PanelIPFS
        '
        Me.PanelIPFS.Controls.Add(Me.btnValidateIPFS)
        Me.PanelIPFS.Controls.Add(Me.Label2)
        Me.PanelIPFS.Location = New System.Drawing.Point(12, 251)
        Me.PanelIPFS.Name = "PanelIPFS"
        Me.PanelIPFS.Size = New System.Drawing.Size(738, 21)
        Me.PanelIPFS.TabIndex = 10
        Me.PanelIPFS.Visible = False
        '
        'btnValidateIPFS
        '
        Me.btnValidateIPFS.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnValidateIPFS.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.btnValidateIPFS.Location = New System.Drawing.Point(596, 21)
        Me.btnValidateIPFS.Name = "btnValidateIPFS"
        Me.btnValidateIPFS.Size = New System.Drawing.Size(166, 23)
        Me.btnValidateIPFS.TabIndex = 9
        Me.btnValidateIPFS.Text = "VALIDATE IPFS IMAGE"
        Me.btnValidateIPFS.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(10, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 13)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "IPFS Path"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(19, 156)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(110, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "File to Upload to IPFS"
        '
        'btnFindFile
        '
        Me.btnFindFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFindFile.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.btnFindFile.Location = New System.Drawing.Point(545, 170)
        Me.btnFindFile.Name = "btnFindFile"
        Me.btnFindFile.Size = New System.Drawing.Size(26, 23)
        Me.btnFindFile.TabIndex = 5
        Me.btnFindFile.Text = ".."
        Me.btnFindFile.UseVisualStyleBackColor = True
        '
        'txtFilename
        '
        Me.txtFilename.Location = New System.Drawing.Point(22, 172)
        Me.txtFilename.Name = "txtFilename"
        Me.txtFilename.Size = New System.Drawing.Size(517, 20)
        Me.txtFilename.TabIndex = 4
        '
        'btnUpload
        '
        Me.btnUpload.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUpload.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.btnUpload.Location = New System.Drawing.Point(608, 170)
        Me.btnUpload.Name = "btnUpload"
        Me.btnUpload.Size = New System.Drawing.Size(166, 23)
        Me.btnUpload.TabIndex = 1
        Me.btnUpload.Text = "UPLOAD TO IPFS"
        Me.btnUpload.UseVisualStyleBackColor = True
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.Image = Global.CardanoBlockChainExamples.My.Resources.Resources.cardano_logo_white
        Me.PictureBox2.Location = New System.Drawing.Point(665, 1)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(127, 70)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox2.TabIndex = 3
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.CardanoBlockChainExamples.My.Resources.Resources.catalyst_logo
        Me.PictureBox1.Location = New System.Drawing.Point(-6, -50)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 179)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.ToolStripStatusLabel2})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 288)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(780, 22)
        Me.StatusStrip1.TabIndex = 11
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(39, 17)
        Me.ToolStripStatusLabel1.Text = "Ready"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(0, 17)
        '
        'PanelRightImage
        '
        Me.PanelRightImage.Controls.Add(Me.PictureBoxAssetIPFS)
        Me.PanelRightImage.Controls.Add(Me.lblPolicyIPFS)
        Me.PanelRightImage.Dock = System.Windows.Forms.DockStyle.Right
        Me.PanelRightImage.Location = New System.Drawing.Point(780, 0)
        Me.PanelRightImage.Name = "PanelRightImage"
        Me.PanelRightImage.Size = New System.Drawing.Size(229, 310)
        Me.PanelRightImage.TabIndex = 12
        '
        'PictureBoxAssetIPFS
        '
        Me.PictureBoxAssetIPFS.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.PictureBoxAssetIPFS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxAssetIPFS.Dock = System.Windows.Forms.DockStyle.Top
        Me.PictureBoxAssetIPFS.InitialImage = Global.CardanoBlockChainExamples.My.Resources.Resources.cardano_logo_white
        Me.PictureBoxAssetIPFS.Location = New System.Drawing.Point(0, 0)
        Me.PictureBoxAssetIPFS.Name = "PictureBoxAssetIPFS"
        Me.PictureBoxAssetIPFS.Size = New System.Drawing.Size(229, 254)
        Me.PictureBoxAssetIPFS.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBoxAssetIPFS.TabIndex = 17
        Me.PictureBoxAssetIPFS.TabStop = False
        '
        'lblPolicyIPFS
        '
        Me.lblPolicyIPFS.Location = New System.Drawing.Point(3, 269)
        Me.lblPolicyIPFS.Name = "lblPolicyIPFS"
        Me.lblPolicyIPFS.Size = New System.Drawing.Size(217, 19)
        Me.lblPolicyIPFS.TabIndex = 18
        Me.lblPolicyIPFS.Text = "IPFS Image"
        Me.lblPolicyIPFS.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmIPFS
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1009, 310)
        Me.Controls.Add(Me.PanelTop)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.PanelRightImage)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmIPFS"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "IPFS"
        Me.PanelTop.ResumeLayout(False)
        Me.PanelTop.PerformLayout()
        Me.PanelIPFS.ResumeLayout(False)
        Me.PanelIPFS.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.PanelRightImage.ResumeLayout(False)
        CType(Me.PictureBoxAssetIPFS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanelTop As Panel
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents btnUpload As Button
    Friend WithEvents Timer1 As Timer
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As ToolStripStatusLabel
    Friend WithEvents btnFindFile As Button
    Friend WithEvents txtFilename As TextBox
    Friend WithEvents btnValidateIPFS As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtIPFSPath As TextBox
    Friend WithEvents PanelRightImage As Panel
    Friend WithEvents PictureBoxAssetIPFS As PictureBox
    Friend WithEvents lblPolicyIPFS As Label
    Friend WithEvents PanelIPFS As Panel
    Friend WithEvents chkPinForever As CheckBox
    Friend WithEvents ToolStripStatusLabel2 As ToolStripStatusLabel
    Friend WithEvents lblIPFS As Label
End Class

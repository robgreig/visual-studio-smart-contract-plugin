﻿Public NotInheritable Class frmAbout

    Private Sub AboutBox1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Set the title of the form.
        Dim ApplicationTitle As String
        If My.Application.Info.Title <> "" Then
            ApplicationTitle = My.Application.Info.Title
        Else
            ApplicationTitle = System.IO.Path.GetFileNameWithoutExtension(My.Application.Info.AssemblyName)
        End If
        Me.Text = String.Format("About {0}", ApplicationTitle)
        ' Initialize all of the text displayed on the About Box.
        ' TODO: Customize the application's assembly information in the "Application" pane of the project 
        '    properties dialog (under the "Project" menu).
        Me.LabelProductName.Text = My.Application.Info.ProductName
        Me.LabelVersion.Text = String.Format("Version {0}", My.Application.Info.Version.ToString)
        Me.LabelCopyright.Text = My.Application.Info.Copyright
        Me.LabelCompanyName.Text = My.Application.Info.CompanyName
        Me.TextBoxDescription.Text = My.Application.Info.Description

        Try
            Dim sLink As String = "https://cardano.ideascale.com/a/dtd/Visual-Studio-Smart-Contract-Plugin/333827-48088"
            LinkLabel1.Text = LinkLabel1.Text & " (" & sLink & ")"
            LinkLabel1.Links.Add(26, 35, sLink)
            sLink = "https://www.dragondefender.com"
            LinkLabel2.Text = LinkLabel2.Text & " (" & sLink & ")"
            LinkLabel2.Links.Add(29, 15, sLink)
            sLink = "http://www.cornucopias.io"
            LinkLabel3.Text = LinkLabel3.Text & " (" & sLink & ")"
            LinkLabel3.Links.Add(34, 31, sLink)
        Catch ex As Exception
            Stop
        End Try

    End Sub

    Private Sub OKButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OKButton.Click
        Me.Close()
    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked, LinkLabel2.LinkClicked, LinkLabel3.LinkClicked
        System.Diagnostics.Process.Start(e.Link.LinkData.ToString())
    End Sub

End Class

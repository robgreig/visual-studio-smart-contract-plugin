﻿Imports System.ComponentModel
Imports System.IO
Imports System.Management

'**********************************************************************************************************************************
'** MORE DETAILS ON HOW TO SETUP THE WINDOWS ENVIRONMENT VARIABLE - https://www.youtube.com/watch?v=gfHMpDW6W7w - thanks Boone for creating "Using Daedalus as a Cardano Node for Minting Tokens - CatalystFUND3 Episode1"
'** https://adamaker.space/using-daedalus-as-a-cardano-node/
'**********************************************************************************************************************************
'* DOWNLOAD - DAEDALUS MAINNET AND TESTNET FROM HERE: https://developers.cardano.org/en/testnets/cardano/get-started/wallet/
'* APPLY FOR FREE TEST ADA FOR TESTNET FROM HERE: https://developers.cardano.org/en/testnets/cardano/tools/faucet/
'** THIS PAGE HELPED THE MOST FOR MINTING TOKENS https://ruttkowa.medium.com/the-ultimate-guide-to-minting-nfts-on-the-cardano-blockchain-a8f914d3b2a1
'**********************************************************************************************************************************

Public Class frmCardanoCLI
    Private sDriveC As String = "C:\"       'CHANGE THIS
    Private sDriveD As String = "D:\"
    Private sCLIPath As String = sDriveC & "Program Files\Daedalus Mainnet\"
    Private sCLIPathMainNet As String = sDriveC & "Program Files\Daedalus Mainnet\"
    Private sCLIPathTestNet As String = sDriveC & "Program Files\Daedalus Testnet\"
    Private sCLIExeName As String = "cardano-cli.exe"
    Private sInputFilePath As String = ""
    Private bCanClose As Boolean = False
    Public MyForm As Form = Nothing
    Public sLastRunCommnand As String = ""
    Public bRunning As Boolean = False
    Public bSubmittedCorrectly As Boolean = False
    Public iSyncCount As Integer = 4

    Private Sub btnExecute_Click(sender As Object, e As EventArgs) Handles btnExecute.Click
        Call RunCardanoCLI(txtParameters.Text, txtExecutable.Text, txtRedirect.Text, chkLog.Checked)
    End Sub
    Function FindDaedalus() As Boolean
        Dim bFound As Boolean = False
        Dim searcher As New ManagementObjectSearcher("SELECT * FROM Win32_Process")
        For Each process As ManagementObject In searcher.Get()
            Dim sText As String = process("CommandLine")
            'Console.WriteLine(process("CommandLine"))
            Dim i As Long = InStr(sText, "socket-path")
            If i > 0 Then
                Dim sSocketPath As String = Trim(Mid(sText, i + 12))

                If RadioButtonTESTNET.Checked Then
                    'looking for testnet
                    If InStr(sSocketPath, "testnet") Then
                        'found
                    Else
                        GoTo nextentry
                    End If
                Else
                    'looking for mainnet

                    'ignore testnet
                    If InStr(sSocketPath, "testnet") Then GoTo nextentry
                End If

                i = InStr(sSocketPath, " ")
                If i > 0 Then sSocketPath = Trim(Mid(sSocketPath, 1, i - 1))
                lblCLI.Text = sSocketPath
                bFound = True
                Exit For
            End If
nextentry:
        Next
        Return bFound
    End Function
    Private Function GetTestNetID(sLine As String) As Long
        Dim lRet As Long = 0
        Dim i As Integer = 0
        i = InStr(sLine, "{unNetworkMagic = ")
        If i > 0 Then
            Dim sTemp As String = Mid(sLine, i + 17)
            i = InStr(sTemp, "}")
            If i > 0 Then
                sTemp = Trim(Mid(sTemp, 1, i - 1))
                If IsNumeric(sTemp) Then
                    lblTestnetID.Text = sTemp
                    lRet = sTemp
                    Call CreateFile(sPathEXEPath & "\testnet.id", sTemp, False)
                End If
            End If
        End If
        Return lRet
    End Function
    Private Sub frmCardanoCLI_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            lblVarPaymentAddress.Text = ""
            lblVarAddressSendTo.Text = ""
            lblVarAmount.Text = ""
            lblVarAmountToken.Text = ""
            lblVarAmount2.Text = ""
            lblVarAmountToSend.Text = ""
            lblVarTxHash.Text = ""
            lblVarTxIx.Text = ""
            lblVarPolicyID.Text = ""
            lblVarPolicyHash.Text = ""
            lblVarTokenName.Text = ""
            lblVarTokenAmountToMint.Text = ""
            lblVarTokenToSendAmount.Text = ""
            lblVarTxFee.Text = ""
            lblVarLatestBlock.Text = ""
            lblVarLatestSlot.Text = ""

            If Not Directory.Exists(sCLIPathTestNet) Then
                'on my pc I work from the D drive
                sCLIPath = sDriveD & "Program Files\Daedalus Mainnet\"
                sCLIPathMainNet = sDriveD & "Program Files\Daedalus Mainnet\"
                sCLIPathTestNet = sDriveD & "Program Files\Daedalus Testnet\"
                If Not Directory.Exists(sCLIPathTestNet) Then
                    MsgBox("Cannot find the Testnet files, please update the code", vbCritical)
                End If
            End If

            sInputFilePath = sPath & "cli-samples.txt"
            With lstFile
                .View = View.Details
                .Columns.Clear()
                .Columns.Add("", 30)
                .Columns.Add("EXE", 100)
                .Columns.Add("PARAMETERS", 400)
                .Columns.Add("OUTFILE", 220)
                .Columns.Add("DESCRIPTION", 8000)
                .FullRowSelect = True
            End With
            Call LoadListView(sInputFilePath)

            'create folder for policies
            If (Not System.IO.Directory.Exists(Replace(sPathEXEPath & "\policy", "\\", "\"))) Then
                System.IO.Directory.CreateDirectory(Replace(sPathEXEPath & "\policy", "\\", "\"))
                Call CreateFile(sPathEXEPath & "\policy\token.name", "MYCOIN", False)
                Call CreateFile(sPathEXEPath & "\policy\tokens-to-mint.qty", "64", False)
                Call CreateFile(sPathEXEPath & "\policy\tokens-to-send.qty", "4", False)
                Call CreateFile(sPathEXEPath & "\policy\metadata.json.template", "{" & vbNewLine & "        ""721"": {" & vbNewLine & "            ""$(VAR=PolicyID)"": {" & vbNewLine & "              ""Testtoken1"": {" & vbNewLine & "                ""description"": ""This Is just a testtoken""," & vbNewLine & "                ""name"": ""One Of ten testtokens""," & vbNewLine & "                ""type"":""""," & vbNewLine & "                ""id"": 1," & vbNewLine & "                ""name"": ""This Is my first test token""," & vbNewLine & "                ""image"": """"" & vbNewLine & "              }" & vbNewLine & "            }," & vbNewLine & "            ""$(VAR=PolicyID)"": {" & vbNewLine & "              ""Testtoken2"": {" & vbNewLine & "                ""description"": ""One coin To rule them all""," & vbNewLine & "                ""name"": ""One Of ten Munich Metal Coins""," & vbNewLine & "                ""type"":""""," & vbNewLine & "                ""id"": 1," & vbNewLine & "                ""name"": ""This Is my first test token""," & vbNewLine & "                ""image"": """"" & vbNewLine & "              }" & vbNewLine & "            }" & vbNewLine & "        }" & vbNewLine & "}" & vbNewLine & "", False)
            End If
            If (Not System.IO.Directory.Exists(Replace(sPathEXEPath & "\recipient", "\\", "\"))) Then
                System.IO.Directory.CreateDirectory(Replace(sPathEXEPath & "\recipient", "\\", "\"))
                Call CreateFile(sPathEXEPath & "\recipient\recipientpay.addr", "addr_xxxxxxxxxxxxx", False)
                Call CreateFile(sPathEXEPath & "\recipient\lovelace-to-send.qty", "1000000", False) 'ada
            End If
            If (Not System.IO.Directory.Exists(Replace(sPathEXEPath & "\address", "\\", "\"))) Then
                System.IO.Directory.CreateDirectory(Replace(sPathEXEPath & "\address", "\\", "\"))
                Call CreateFile(sPathEXEPath & "\address\payment.addr", "addr_xxxxxxxxxxxxx", False)
            End If
            If (Not System.IO.Directory.Exists(Replace(sPathEXEPath & "\transaction", "\\", "\"))) Then
                System.IO.Directory.CreateDirectory(Replace(sPathEXEPath & "\transaction", "\\", "\"))
            End If
            If (Not System.IO.File.Exists(sPathEXEPath & "nft.folder")) Then Call CreateFile(sPathEXEPath & "nft.folder", "nft\mynft1", False)
            If (Not System.IO.Directory.Exists(Replace(sPathEXEPath & "\nft", "\\", "\"))) Then
                System.IO.Directory.CreateDirectory(Replace(sPathEXEPath & "\nft", "\\", "\"))
                System.IO.Directory.CreateDirectory(Replace(sPathEXEPath & "\nft\mynft1", "\\", "\"))
                Call CreateFile(sPathEXEPath & "\nft\mynft1\nft.name", "MY NFT NAME", False)
                Call CreateFile(sPathEXEPath & "\nft\mynft1\nft.image", "ipfs://", False)
                Call CreateFile(sPathEXEPath & "\nft\mynft1\nft.description", "MY NFT DESCRIPTION", False)
                Call CreateFile(sPathEXEPath & "\nft\mynft1\nft.type", "image/png", False)
                Call CreateFile(sPathEXEPath & "\nft\mynft1\nft.src", "ipfs://", False)
                Call CreateFile(sPathEXEPath & "\nft\mynft1\nft.attributes", "['ATT1','ATT2']", False)
                Call CreateFile(sPathEXEPath & "\nft\mynft1\nft.id", "1", False)
            End If

            RadioButtonMAINNET.Checked = frmCardanoMain.RadioButtonMAINNET.Checked
            If FindDaedalus() Then
                'check if the CLI is installed
                If File.Exists(sCLIPath & sCLIExeName) = False Then
                    MsgBox("Error - Cardano CLI file is missing - " & vbNewLine & vbNewLine & sCLIPath & sCLIExeName, vbCritical)
                    Me.Close()
                End If
            Else
                'MsgBox("Error - Daedalus Mainnet is not running - please install or switch to Testnet and sync with the Blockchain before running the Cardano CLI" & vbNewLine & vbNewLine & "See top of this file for Install instructions", vbCritical)
            End If
            frmCardanoMain.Visible = False

            Call LoadScripts()
            Call SaveToLogs("START", False)

            RadioButtonTESTNET.Checked = frmCardanoMain.RadioButtonTESTNET.Checked
        Catch ex As Exception
            MsgBox(ex.Message, vbCritical)
        End Try
    End Sub
    Public Sub LoadVariables()
        Try
            bSubmittedCorrectly = False

            For Each itmX As ListViewItem In lstFile.Items
                If itmX.SubItems.Count > 2 Then
                    If InStr(itmX.SubItems(3).Text, "VAR") Then
                        itmX.Selected = True
                        RunCommandFromListviewItem(itmX)
                    ElseIf InStr(itmX.SubItems(3).Text, "TABLE-REFRESH") Then
                        itmX.Selected = True
                        RunCommandFromListviewItem(itmX)
                    ElseIf InStr(itmX.SubItems(3).Text, "REFRESH") Then
                        itmX.Selected = True
                        RunCommandFromListviewItem(itmX)
                    ElseIf InStr(itmX.SubItems(3).Text, "METADATA") Then
                        itmX.Selected = True
                        RunCommandFromListviewItem(itmX)
                    End If
                End If
            Next
            bSubmittedCorrectly = False
        Catch ex As Exception
        End Try
    End Sub
    Public Sub LoadListView(sFilePath As String)
        Try
            Dim iCount As Integer = 1
            If File.Exists(sFilePath) Then
                Dim inputstream As New IO.StreamReader(sFilePath)
                Dim newstr(2) As String
                Dim value As String = ""

                'remove any previous lines
                lstFile.Items.Clear()

                'Read while there is mor characters to read
                Do While inputstream.Peek <> -1
                    Dim itmX As New ListViewItem

                    'Split each line containing Account|Password into the array
                    newstr = inputstream.ReadLine().Split("~")
                    'Assigm the values to the variables
                    value = iCount

                    'If iCount = 31 Then Stop

                    If newstr.Length > 0 Then itmX.SubItems.Add(Trim(newstr(0))) Else itmX.SubItems.Add("")
                    If newstr.Length > 1 Then itmX.SubItems.Add(Trim(newstr(1))) Else itmX.SubItems.Add("")
                    If newstr.Length > 2 Then itmX.SubItems.Add(Trim(newstr(2))) Else itmX.SubItems.Add("")
                    If newstr.Length > 3 Then itmX.SubItems.Add(Trim(newstr(3))) Else itmX.SubItems.Add("")

                    If newstr.Length > 0 Then
                        If InStr(newstr(0), "READ") Or InStr(newstr(0), "CREATE") Then
                            itmX.BackColor = Color.Aquamarine
                        ElseIf InStr(newstr(0), "#") Then
                            itmX.BackColor = Color.LightGray
                        Else
                            itmX.BackColor = Color.GreenYellow
                        End If
                    End If
                    If newstr.Length > 1 Then
                        If InStr(newstr(1), "--testnet-magic") Then
                            itmX.BackColor = Color.LightSalmon
                        End If
                    End If

                    itmX.Text = value
                    If itmX.SubItems(1).Text > "" Then
                        iCount = iCount + 1
                        lstFile.Items.Add(itmX)
                    End If
                Loop
                inputstream.Close()
            Else
                'MsgBox(sFilePath & " - not found ")
                DoLog(2, "ERROR LoadListView - not found " & sFilePath)
            End If

        Catch ex As Exception
            DoLog(2, "ERROR - " & ex.Message)
        End Try
    End Sub
    Sub DoLog(iPanel As Integer, sMessage As String)
        Try
            If iPanel = 1 Then
                ToolStripStatusLabel1.Text = sMessage
            Else
                ToolStripStatusLabel2.Text = Mid(sMessage, 1, 300)
            End If
            Application.DoEvents()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub lstFile_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstFile.Click
        Try
            For Each itmX As ListViewItem In lstFile.Items
                itmX.ForeColor = Color.Black
            Next

            If lstFile.SelectedItems.Count > 0 Then
                RunCommandFromListviewItem(lstFile.SelectedItems(0))
                lstFile.SelectedItems(0).ForeColor = Color.Red
                lstFile.SelectedItems.Clear()
            End If
        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try
    End Sub
    Private Sub RunCommandFromListviewItem(itmX As ListViewItem)
        Try
            With itmX
                txtExecutable.Text = ""
                txtParameters.Text = ""
                txtRedirect.Text = ""
                lblLineNo.Text = "?"
                lblCLIComments.Text = ""
                If Not IsNothing(itmX.Text) Then txtExecutable.Text = Trim(itmX.SubItems(1).Text)
                If itmX.SubItems.Count > 1 Then txtParameters.Text = Trim(itmX.SubItems(2).Text)
                If itmX.SubItems.Count > 2 Then txtRedirect.Text = Trim(itmX.SubItems(3).Text)
                If itmX.SubItems.Count > 3 Then lblCLIComments.Text = Trim(itmX.SubItems(4).Text)
                lblLineNo.Text = Trim(itmX.Text)
                Application.DoEvents()
            End With
        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try
        If txtExecutable.Text > "" Then Call btnExecute.PerformClick()
    End Sub
    Private Sub PictureBox2_Click(sender As Object, e As EventArgs) Handles PictureBox2.Click
        'a quick way to reload the CLI input file while developing
        Call LoadListView(sInputFilePath)
    End Sub
    Private Function ReadFile(sFilename As String, sParameters As String, sRedirectOutputFile As String, Optional iTry As Integer = 1) As String
        Dim sRet As String = ""
        Try
            txtFile.Text = ""
            sFilename = Replace(sFilename & sParameters, "/", "\")
            sRedirectOutputFile = Replace(sRedirectOutputFile, "/", "\")
            sRet = Replace(System.IO.File.ReadAllText(sFilename), vbLf, vbNewLine)
            If InStr(sRedirectOutputFile, "VAR") Then Call SetVariable(Mid(sRedirectOutputFile.ToUpper, 5), sRet)
        Catch e As Exception
            ' Let the user know what went wrong.
            Console.WriteLine("The file could not be read:")
            Console.WriteLine(e.Message)
            txtFile.Text = Now & " ERROR READING FILE - " & e.Message
            If iTry = 1 Then
                Dim sRet2 As String = ""
                sRet2 = GetAppPathActual()
                sRet2 = ReadFile(GetAppPathActual(), sParameters, sRedirectOutputFile, iTry + 1)
            End If
        End Try
        Return sRet
    End Function
    Private Function CreateFile(sFilename As String, sContents As String, bAppendToFileInsteadOfOverwriting As Boolean) As String
        Dim bRet As Boolean = False
        sFilename = Replace(sFilename, "/", "\")

        Dim sFile() As String = Split(sFilename, ",")
        If sFile.Length > 1 Then If sFile(1).ToUpper = "APPEND" Then bAppendToFileInsteadOfOverwriting = True
        If sFile.Length > 1 Then If sFile(1).ToUpper = "DELETE" Then Return True
        Try
            Dim objWriter As New System.IO.StreamWriter(sFile(0), bAppendToFileInsteadOfOverwriting)
            If bAppendToFileInsteadOfOverwriting Then
                objWriter.Write(sContents & vbCrLf)
            Else
                objWriter.Write(Replace(sContents, vbLf, ""))
            End If
            objWriter.Close()
        Catch ex As Exception
            MsgBox(ex.Message, vbCritical)
        End Try
        Return System.IO.File.Exists(sFile(0))
    End Function
    Private Function DeleteFile(sFilename As String) As String
        Dim bRet As Boolean = False

        sFilename = Replace(sFilename, "/", "\")
        Dim sFile() As String = Split(sFilename, ",")
        Try
            If System.IO.File.Exists(sFile(0)) Then System.IO.File.Delete(sFile(0))
        Catch ex As Exception
            MsgBox(ex.Message, vbCritical)
        End Try
        Return System.IO.File.Exists(sFile(0))
    End Function
    Private Sub ToolStripStatusLabel1_Click(sender As Object, e As EventArgs) Handles ToolStripStatusLabel1.Click
        Clipboard.SetText(ToolStripStatusLabel1.Text)
    End Sub
    Private Sub frmCardanoCLI_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        frmCardanoMain.Show()
    End Sub
    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButtonMAINNET.CheckedChanged, RadioButtonTESTNET.CheckedChanged, RadioButtonMAINNET.Click, RadioButtonTESTNET.Click
        If RadioButtonTESTNET.Checked Then
            sCLIPath = sCLIPathTestNet
            If FindDaedalus() = False Then MsgBox("ERROR - TESTNET Not Running", vbCritical)
        Else
            sCLIPath = sCLIPathMainNet
            If FindDaedalus() = False Then MsgBox("ERROR - MAINNET Not Running", vbCritical)
        End If
    End Sub
    Function CheckForVariables(sText As String, sEXE As String) As String
        Dim sRet As String = sText
        Dim i As Integer = InStr(sText, "$(VAR=")
        If i > 0 Then
            Dim sVar() As String = Split(sText, "$(")
            For Each MyVar In sVar
                Dim iVar As Integer = InStr(MyVar, "=")
                Dim jVar As Integer = InStr(MyVar, ")")
                If iVar > 0 And jVar > 0 Then
                    Dim sTempVar As String = Mid(MyVar, iVar + 1, jVar - iVar - 1)
                    If sEXE.ToUpper = "CALC" Then
                        'work out any calculations
                        If InStr(sTempVar, "+") Or InStr(sTempVar, "*") Or InStr(sTempVar, "-") Then
                            Dim sPlus As String = GetVariable(sTempVar)
                        End If
                        Dim sNewVar As String = GetVariable(sTempVar)
                        If sText > "" Then sRet = Replace(sText, "$(VAR=" & sTempVar & ")", sNewVar)
                        sText = sRet
                    Else
                        Dim sNewVar As String = GetVariable(sTempVar)
                        If sText > "" Then sRet = Replace(sText, "$(VAR=" & sTempVar & ")", sNewVar)
                        sText = sRet
                    End If
                End If
            Next
        End If
        Return sRet
    End Function
    Private Sub lstTable_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstTable.SelectedIndexChanged
        Try
            With lstTable
                If .SelectedItems.Count > 0 Then
                    If lstTable.Columns.Count > 0 Then
                        'check the table contains transaction values
                        If lstTable.Columns(0).Text = "TxHash" Then
                            lblVarTxIx.Text = .SelectedItems(0).SubItems(1).Text
                            lblVarAmount.Text = .SelectedItems(0).SubItems(2).Text
                            lblVarAmount2.Text = CLng(.SelectedItems(0).SubItems(2).Text) / 1000000
                            lblVarTxHash.Text = .SelectedItems(0).Text

                            Dim sRedirectOutputFile As String = txtRedirect.Text
                            Dim bRefreshTable As Boolean = False
                            If InStr(sRedirectOutputFile, "TABLE-REFRESH") > 0 Then bRefreshTable = True

                            Try
                                'see if we already have any tokens on the address
                                lblVarAmountToken.Text = "0"
                                lblVarAmountToken.Text = .SelectedItems(0).SubItems(5).Text

                                If bRefreshTable Then
                                    If lblVarAmountPreviousValue.Text = "0" Then lblVarAmountPreviousValue.Text = lblVarAmount.Text
                                    If lblVarAmountTokenPreviousValue.Text = "0" Then lblVarAmountTokenPreviousValue.Text = lblVarAmountToken.Text
                                End If
                            Catch ex As Exception

                            End Try

                        End If
                    End If
                End If
            End With
        Catch ex As Exception
            DoLog(1, ex.Message)
        End Try
    End Sub
    Private Sub SetVariable(sVariable As String, sContents As String)
        If sVariable = "PAYMENTADDRESS" Then
            lblVarPaymentAddress.Text = Replace(Replace(sContents, vbLf, ""), vbCr, "")
        ElseIf sVariable = "PAYMENTADDRESSTO" Then
            lblVarAddressSendTo.Text = Replace(Replace(sContents, vbLf, ""), vbCr, "")
        ElseIf sVariable = "PREVIOUSTOKENBALANCE" Then
            lblVarAmountTokenPreviousValue.Text = Replace(Replace(sContents, vbLf, ""), vbCr, "")
        ElseIf sVariable = "PREVIOUSLOVELACEBALANCE" Then
            lblVarAmountPreviousValue.Text = Replace(Replace(sContents, vbLf, ""), vbCr, "")
        ElseIf sVariable = "POLICYID" Then
            lblVarPolicyID.Text = Replace(Replace(sContents, vbLf, ""), vbCr, "")
        ElseIf sVariable = "POLICYHASH" Then
            lblVarPolicyHash.Text = Replace(Replace(sContents, vbLf, ""), vbCr, "")
            'Dim sTemp As String = Replace(sContents, vbLf, "")
            'Dim i As Integer = InStr(sTemp, " ")
            'If i > 0 Then If IsNumeric(Trim(Mid(sTemp, 1, i - 1))) Then lblVarPolicyHash.Text = Trim(Mid(sTemp, 1, i - 1)) Else lblVarPolicyHash.Text = ""
        ElseIf sVariable = "TOKENNAME" Then
            lblVarTokenName.Text = Replace(Replace(sContents, vbLf, ""), vbCr, "")
        ElseIf sVariable = "TOKENAMOUNTTOMINT" Then
            Dim sTemp As String = Replace(Replace(sContents, vbLf, ""), vbCr, "")
            If IsNumeric(sTemp) Then lblVarTokenAmountToMint.Text = sTemp Else lblVarTokenAmountToMint.Text = "0"
        ElseIf sVariable = "TOKENAMOUNTTOSEND" Then
            Dim sTemp As String = Replace(Replace(sContents, vbLf, ""), vbCr, "")
            If IsNumeric(sTemp) Then lblVarTokenToSendAmount.Text = sTemp Else lblVarTokenToSendAmount.Text = ""
        ElseIf sVariable = "TXFEE" Then
            Dim sTemp As String = Replace(Replace(sContents, vbLf, ""), vbCr, "")
            Dim i As Integer = InStr(sTemp, " ")
            If i > 0 Then If IsNumeric(Trim(Mid(sTemp, 1, i - 1))) Then lblVarTxFee.Text = Trim(Mid(sTemp, 1, i - 1)) Else lblVarTxFee.Text = ""
        ElseIf sVariable = "TESTNETID" Then
            Dim sTemp As String = Replace(Replace(sContents, vbLf, ""), vbCr, "")
            If IsNumeric(sTemp) Then lblTestnetID.Text = sTemp Else lblTestnetID.Text = "?"
        ElseIf sVariable = "BEFORESLOT" Then
            Dim sTemp As String = Replace(Replace(sContents, vbLf, ""), vbCr, "")
            If IsNumeric(sTemp) Then lblVarLatestBlock.Text = sTemp Else lblVarLatestBlock.Text = ""
        ElseIf sVariable = "LATESTSLOT" Then
            Dim sTemp As String = GetValue(sContents, "SLOT")
            If IsNumeric(sTemp) Then lblVarLatestSlot.Text = sTemp Else lblVarLatestSlot.Text = ""
        ElseIf sVariable = "LATESTBLOCK" Then
            Dim sTemp As String = GetValue(sContents, "BLOCK")
            If IsNumeric(sTemp) Then lblVarLatestBlock.Text = sTemp Else lblVarLatestBlock.Text = ""
        ElseIf sVariable = "LOVELACETOSEND" Then
            Dim sTemp As String = Replace(Replace(sContents, vbLf, ""), vbCr, "")
            If IsNumeric(sTemp) Then lblVarAmountToSend.Text = sTemp Else lblVarAmountToSend.Text = ""
        ElseIf sVariable = "NFTFOLDER" Then
            lblNFTFolder.Text = Replace(Replace(sContents, vbLf, ""), vbCr, "")
        ElseIf sVariable = "NFTNAME" Then
            lblNFTName.Text = Replace(Replace(sContents, vbLf, ""), vbCr, "")
        ElseIf sVariable = "NFTIMAGE" Then
            lblNFTImage.Text = Replace(Replace(sContents, vbLf, ""), vbCr, "")
        ElseIf sVariable = "NFTDESCRIPTION" Then
            lblNFTDescription.Text = Replace(Replace(sContents, vbLf, ""), vbCr, "")
        ElseIf sVariable = "NFTTYPE" Then
            lblNFTType.Text = Replace(Replace(sContents, vbLf, ""), vbCr, "")
        ElseIf sVariable = "NFTSRC" Then
            lblNFTSRC.Text = Replace(Replace(sContents, vbLf, ""), vbCr, "")
        ElseIf sVariable = "NFTATTRIBUTES" Then
            lblNFTAttributes.Text = Replace(Replace(sContents, vbLf, ""), vbCr, "")
        ElseIf sVariable = "NFTID" Then
            lblNFTID.Text = Replace(Replace(sContents, vbLf, ""), vbCr, "")
        Else
            Stop
            DoLog(1, "Variable Not Found:" & sVariable)
        End If
    End Sub
    Function GetValue(sContent As String, sSearch As String) As String
        Dim sRet As String = ""
        Try
            Dim sFields() As String = Split(sContent, ",")
            For Each col In sFields
                If InStr(col.ToUpper, sSearch.ToUpper) > 0 Then
                    Dim sFields2() As String = Split(col, ":")
                    If sFields2.Length > 0 Then sRet = Trim(sFields2(1))
                End If
            Next
        Catch ex As Exception
            Stop
        End Try
        Return sRet
    End Function
    Function GetVariable(sVar As String) As String
        Dim sRet As String = ""
        Try
            If sVar > "" Then
                If sVar.ToUpper = "TXHASH" Then
                    sRet = lblVarTxHash.Text
                ElseIf sVar.ToUpper = "TXIX" Then
                    sRet = lblVarTxIx.Text
                ElseIf sVar.ToUpper = "PAYMENTADDRESS" Then
                    sRet = lblVarPaymentAddress.Text
                ElseIf sVar.ToUpper = "PAYMENTADDRESSTO" Then
                    sRet = lblVarAddressSendTo.Text
                ElseIf sVar.ToUpper = "LOVELACETOSEND" Then
                    sRet = lblVarAmountToSend.Text
                ElseIf sVar.ToUpper = "LOVELACE" Then
                    If IsNumeric(lblVarAmount.Text) Then
                        sRet = lblVarAmount.Text
                    Else
                        'MsgBox("Missing Lovelace Variable", vbCritical)
                    End If
                ElseIf sVar.ToUpper = "ADDRESSTOKENAMOUNT" Then
                    If IsNumeric(lblVarAmountToken.Text) Then
                        sRet = lblVarAmountToken.Text
                    Else
                        sRet = "0"
                    End If
                ElseIf sVar.ToUpper = "PREVIOUSTOKENBALANCE" Then
                    If IsNumeric(lblVarAmountTokenPreviousValue.Text) Then
                        sRet = lblVarAmountTokenPreviousValue.Text
                    Else
                        sRet = "0"
                    End If
                ElseIf sVar.ToUpper = "PREVIOUSLOVELACEBALANCE" Then
                    If IsNumeric(lblVarAmountPreviousValue.Text) Then
                        sRet = lblVarAmountPreviousValue.Text
                    Else
                        sRet = "0"
                    End If
                ElseIf sVar.ToUpper = "LOVELACESEND" Then
                    If IsNumeric(lblVarAmountToSend.Text) Then sRet = lblVarAmountToSend.Text
                ElseIf sVar.ToUpper = "LOVELACE-TXFEE" Then
                    'If IsNumeric(lblVarAmount.Text) And IsNumeric(lblVarTxFee.Text) Then
                    '    sRet = CLng(lblVarAmount.Text) - CLng(lblVarTxFee.Text)
                    'Else
                    '    'MsgBox("Missing Lovelace Variable", vbCritical)
                    'End If
                ElseIf sVar.ToUpper = "POLICYID" Then
                    sRet = lblVarPolicyID.Text
                ElseIf sVar.ToUpper = "POLICYHASH" Then
                    sRet = lblVarPolicyHash.Text
                ElseIf sVar.ToUpper = "TOKENNAME" Then
                    sRet = lblVarTokenName.Text
                ElseIf sVar.ToUpper = "BEFORESLOT" Then
                    sRet = lblVarLatestBlock.Text
                ElseIf sVar.ToUpper = "TOKENAMOUNTTOMINT" Then
                    If IsNumeric(lblVarTokenAmountToMint.Text) Then
                        sRet = lblVarTokenAmountToMint.Text
                    Else
                        'MsgBox("Missing Token Amount Variable", vbCritical)
                    End If
                ElseIf sVar.ToUpper = "TOKENAMOUNTTOSEND" Then
                    If IsNumeric(lblVarTokenToSendAmount.Text) Then
                        sRet = lblVarTokenToSendAmount.Text
                    Else
                        'MsgBox("Missing Token Amount Variable", vbCritical)
                    End If
                ElseIf sVar.ToUpper = "TXFEE" Then
                    If IsNumeric(lblVarTxFee.Text) Then
                        sRet = lblVarTxFee.Text
                    Else
                        'MsgBox("Missing Fee Variable", vbCritical)
                    End If
                ElseIf InStr(sVar.ToUpper, "LATESTSLOT") Then
                    If IsNumeric(lblVarLatestSlot.Text) Then sRet = lblVarLatestSlot.Text
                ElseIf sVar.ToUpper = "LATESTBLOCK" Then
                    If IsNumeric(lblVarLatestBlock.Text) Then sRet = lblVarLatestBlock.Text
                ElseIf sVar.ToUpper = "NFTNAME" Then
                    sRet = lblNFTName.Text
                ElseIf sVar.ToUpper = "NFTIMAGE" Then
                    sRet = lblNFTImage.Text
                ElseIf sVar.ToUpper = "NFTDESCRIPTION" Then
                    sRet = lblNFTDescription.Text
                ElseIf sVar.ToUpper = "NFTTYPE" Then
                    sRet = lblNFTType.Text
                ElseIf sVar.ToUpper = "NFTSRC" Then
                    sRet = lblNFTSRC.Text
                ElseIf sVar.ToUpper = "NFTATTRIBUTES" Then
                    sRet = lblNFTAttributes.Text
                ElseIf sVar.ToUpper = "NFTFOLDER" Then
                    sRet = lblNFTFolder.Text
                ElseIf sVar.ToUpper = "NFTID" Then
                    sRet = lblNFTID.Text
                ElseIf IsNumeric(sVar) Then
                    'ignore
                    sRet = sVar
                Else
                    Stop
                    DoLog(1, "Variable Not Found - " & sVar)
                End If
            End If
        Catch ex As Exception
            DoLog(1, ex.Message)
        End Try
        If sRet = "" Then MsgBox("Missing Variable Value for " & sVar, vbCritical)
Exit_function:
        Return Trim(sRet)
    End Function
    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub
    Private Sub LoadVariablesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LoadVariablesToolStripMenuItem.Click
        Call LoadVariables()
    End Sub
    Private Sub lblVarAmountToSend_TextChanged(sender As Object, e As EventArgs) Handles lblVarAmountToSend.TextChanged
        lblVarAmountToSendADA.Text = ""
        Try
            If IsNumeric(lblVarAmountToSend.Text) Then
                lblVarAmountToSendADA.Text = CLng(lblVarAmountToSend.Text) / 1000000
            End If
        Catch ex As Exception

        End Try
    End Sub
    Function RunCardanoCLI(sParameters As String, sEXE As String, sRedirectOutputFile As String, bLog As Boolean) As Boolean
        Dim proc As New Process
        Dim bRet As Boolean = False
        Dim sParameters2 As String = sParameters
        Dim sParametersOLD As String = sParameters
        Dim sRedirectOutputFileOld = sRedirectOutputFile
        Dim i As Integer = 0
        Try
thebeginning:
            sParameters = Replace(sParameters, "--testnet-magic=174540", "--testnet-magic=$TESTNET_ID")

            If RadioButtonMAINNET.Checked = True And InStr(sParameters.ToUpper, "--testnet".ToUpper) > 0 Then
                sParameters = Replace(sParameters, "--testnet-magic=$TESTNET_ID", "--mainnet")
                sParameters = Replace(sParameters, "--testnet-magic $TESTNET_ID", "--mainnet")
                DoLog(3, "Changing TestNet script to MainNet")
            End If

            If InStr(sEXE, "#") > 0 Then GoTo end_sub 'ignore comments

            If InStr(sRedirectOutputFile, "-SUBMIT_TRUE") > 0 Then
                If bSubmittedCorrectly = False Then
                    GoTo end_sub 'ignore if not submiited 
                Else
                    sRedirectOutputFile = Replace(sRedirectOutputFile, "-SUBMIT_TRUE", "")
                End If
            End If

            If InStr(sRedirectOutputFile, "TABLE-REFRESH") > 0 Then sRedirectOutputFile = Replace(sRedirectOutputFile, "TABLE-REFRESH", "TABLE")

            If sLastRunCommnand = sParametersOLD And InStr(sRedirectOutputFile, "TABLE-SYNC") = 0 And InStr(sRedirectOutputFile, "VAR") = 0 Then GoTo end_sub
            sLastRunCommnand = sParametersOLD

            If InStr(sParameters, "transaction build-raw") Then bSubmittedCorrectly = False

            txtFile.Text = ""
            RichTextBox1.Clear() 'used to store the output
            RichTextBox2.Clear() 'used to show the input and output
            PictureBox2.BackColor = Color.Red
            DoLog(2, Now & " - Running command line ..")
            With proc.StartInfo
                .UseShellExecute = False  ' Required for redirection
                If sEXE = sCLIExeName Or sEXE & ".exe" = sCLIExeName Then
                    .FileName = sCLIPath & sEXE
                Else
                    .FileName = sEXE
                End If

                'check if any of our special input parameters are being passed
                i = InStr(sParameters, "$(VAR=")
                If sEXE.ToUpper = "READ_FILE" And i > 0 Then
                    Dim sTempVar As String = Trim(Mid(sParameters, i + 6))
                    Dim j As Integer = InStr(sTempVar, ")")
                    If j > 0 Then
                        sTempVar = Trim(Mid(sTempVar, 1, j - 1))
                        Dim sNewVar As String = GetVariable(sTempVar)
                        If sNewVar > "" Then sTempVar = sNewVar
                    End If
                    Dim sNewParameters As String = Mid(sParameters, 1, i - 1) & sTempVar & Mid(sParameters, i + j + 6)
                    sParameters = sNewParameters
                End If

                sParameters = CheckForVariables(sParameters, sEXE)
                sRedirectOutputFile = CheckForVariables(sRedirectOutputFile, sEXE)

                'check if any of our special input parameters are being passed
                i = InStr(sParameters, "$(READ_FILE")
                If i > 0 Then
                    'extract the file
                    Dim sNewFile As String = Trim(Mid(sParameters, i + 12))
                    Dim j As Integer = InStr(sNewFile, ")")
                    If j > 0 Then sNewFile = Trim(Mid(sNewFile, 1, j - 1))
                    Dim sReply As String = ReadFile(sPathEXEPath, sNewFile, sRedirectOutputFile)
                    Dim sNewParameters As String = Mid(sParameters, 1, i - 1) & sReply & Mid(sParameters, i + j + 12)
                    sParameters = sNewParameters
                End If

                If sEXE.ToUpper = "READ_FILE" Then
                    Dim sContents As String = ReadFile(sPathEXEPath, sParameters, sRedirectOutputFile)
                    If sRedirectOutputFile.ToUpper = "METADATA" Then
                        i = InStr(sContents, "$(VAR=")
                        If i > 0 Then
                            Dim sVar() As String = Split(sContents, "$(")
                            For Each MyVar In sVar
                                Dim iVar As Integer = InStr(MyVar, "=")
                                Dim jVar As Integer = InStr(MyVar, ")")
                                If iVar > 0 And jVar > 0 Then
                                    Dim sTempVar As String = Mid(MyVar, iVar + 1, jVar - iVar - 1)
                                    Dim sNewVar As String = GetVariable(sTempVar)
                                    If sContents > "" Then sContents = Replace(Replace(sContents, "$(VAR=" & sTempVar & ")", sNewVar), vbCr, vbNewLine)
                                End If
                            Next
                        End If
                        txtMetaData.Text = sContents
                    Else
                        txtFile.Text = sContents
                    End If
                    DoLog(2, Now & " - Finished.")
                    GoTo end_sub
                End If

                sParameters = CheckForVariables(sParameters, sEXE)
                sRedirectOutputFile = CheckForVariables(sRedirectOutputFile, sEXE)

                'check if any of our special input parameters are being passed
                i = InStr(sParameters, "$(READ_FILE")
                If i > 0 Then
                    'extract the file
                    Dim sNewFile As String = Trim(Mid(sParameters, i + 12))
                    Dim j As Integer = InStr(sNewFile, ")")
                    If j > 0 Then sNewFile = Trim(Mid(sNewFile, 1, j - 1))
                    Dim sReply As String = ReadFile(sPathEXEPath, sNewFile, sRedirectOutputFile)
                    Dim sNewParameters As String = Mid(sParameters, 1, i - 1) & sReply & Mid(sParameters, i + j + 12)
                    sParameters = sNewParameters
                End If

                i = InStr(sRedirectOutputFile, ",DELETE")
                If i > 0 Then
                    Call DeleteFile(sPathEXEPath & sRedirectOutputFile)
                End If

                Dim iTry As Integer = 0
docalc:
                iTry = iTry + 1
                i = InStr(sParameters, "$CALC")
                If i > 0 Then
                    Try
                        Dim sTempVar = Mid(sParameters, i)
                        i = InStr(sTempVar, " ")
                        If i > 0 Then sTempVar = Mid(sTempVar, 1, i - 2)
                        i = InStr(sTempVar, ",")
                        If i > 0 Then sTempVar = Mid(sTempVar, 1, i - 1)
                        i = InStr(sTempVar, Chr(34))
                        If i > 0 Then sTempVar = Mid(sTempVar, 1, i - 1)
                        i = InStr(sTempVar, ")")
                        If i > 0 Then sTempVar = Mid(sTempVar, 1, i)

                        Dim sCalc As String = Replace(sTempVar & ")", "))", ")")
                        sTempVar = Replace(Replace(Replace(sTempVar, "$CALC", ""), "(", ""), ")", "")

                        Dim sVars(50) As String
                        Dim iCount As Integer = 0
                        sVars(0) = ""
                        For Each c In sTempVar
                            If (c = "+" Or c = "-" Or c = "*" Or c = "/" Or c = "(" Or c = ")") Then
                                iCount = iCount + 1
                                sVars(iCount) = sVars(iCount) + c
                            Else
                                sVars(iCount) = sVars(iCount) + c
                            End If
                        Next

                        iCount = 0
                        Dim lFinal As Long = 0
                        For Each d In sVars
                            If sVars(iCount + 1) > "" Then
                                If InStr(sVars(iCount + 1), "-") Then
                                    lFinal = Int(sVars(iCount)) - Replace(sVars(iCount + 1), "-", "")
                                ElseIf InStr(sVars(iCount + 1), "+") Then
                                    lFinal = Int(sVars(iCount)) + Replace(sVars(iCount + 1), "+", "")
                                End If
                            Else
                                Exit For
                            End If
                            sVars(iCount + 1) = lFinal
                            iCount = iCount + 1
                        Next

                        sParameters2 = sParameters2 & vbNewLine & vbNewLine & sCalc
                        sParameters = Replace(sParameters, sCalc, lFinal)
                    Catch ex As Exception
                        'Stop
                        Debug.Print(ex.Message)
                    End Try
                End If
                i = InStr(sParameters, "$CALC")
                If i > 0 And iTry < 5 Then GoTo docalc
                sParameters = Replace(Replace(Replace(sParameters, "$CALC", ""), "(", ""), ")", "")

                If sEXE.ToUpper = "CREATE_FILE" Then
                    Dim sContents As String = sParameters
                    i = InStr(sContents, "$(VAR=")
                    If i > 0 Then
                        Dim sVar() As String = Split(sContents, "$(")
                        For Each MyVar In sVar
                            Dim iVar As Integer = InStr(MyVar, "=")
                            Dim jVar As Integer = InStr(MyVar, ")")
                            If iVar > 0 And jVar > 0 Then
                                Dim sTempVar As String = Mid(MyVar, iVar + 1, jVar - iVar - 1)
                                Dim sNewVar As String = GetVariable(sTempVar)
                                If sContents > "" Then sContents = Replace(Replace(sContents, "$(VAR=" & sTempVar & ")", sNewVar), vbCr, vbNewLine)
                            End If
                        Next
                    End If
                    sParameters = sContents
                End If

                sParameters = Replace(Replace(Replace(sParameters, "$CALC", ""), "(", ""), ")", "")

                If sEXE.ToUpper = "CREATE_FILE" Then
                    Dim sContents As String = sParameters
                    Call CreateFile(sPathEXEPath & sRedirectOutputFile, sContents, False)
                    DoLog(2, Now & " - Finished.")
                    GoTo end_sub
                ElseIf sEXE.ToUpper = "CALC" Then

                    With RichTextBox2
                        'change the text color
                        .SelectionColor = Color.Orange
                        .AppendText(sParameters2 & vbNewLine & vbNewLine & sParameters & vbNewLine & vbNewLine)
                        .SelectionColor = Color.White
                    End With

                    DoLog(2, Now & " - Finished.")
                    GoTo end_sub
                End If

                If InStr(sEXE, "EXPORT_TABLE") > 0 Then
                    'update latest transaction
                    Dim myTX As ListViewItem = FindUTXOInTable(lblVarTxHash.Text)

                    If Not IsNothing(myTX) Then
                        If myTX.SubItems(2).Text <> lblVarAmountPreviousValue.Text Then myTX.SubItems(2).Text = lblVarAmountPreviousValue.Text
                        If myTX.SubItems(5).Text <> lblVarAmountTokenPreviousValue.Text Then myTX.SubItems(5).Text = lblVarAmountTokenPreviousValue.Text
                    End If

                    'If lstTable.Items.Count > 0 Then Call frmCardanoMain.DoExport(lstTable, False, True, sRedirectOutputFile)
                    DoLog(2, Now & " - Finished.")
                    GoTo end_sub
                End If

                .RedirectStandardError = True
                .RedirectStandardOutput = True
                .RedirectStandardInput = True
                .CreateNoWindow = True
                'sParameters = sParameters & " || out.txt"

                If InStr(sParameters, "$TESTNET_ID") Then
                    If IsNumeric(lblTestnetID.Text) Then
tryagain2:
                        sParameters = Replace(sParameters, "$TESTNET_ID", lblTestnetID.Text)
                        If RadioButtonTESTNET.Checked = False Then
                            'MsgBox("Switching to TESTNET", vbInformation)
                            RadioButtonTESTNET.Checked = True
                        End If
                    Else
                        'run a query to  get the testnet id
                        'MsgBox("Getting TESTNET ID", vbInformation)
                        Call RunCardanoCLI("query tip --cardano-mode --testnet-magic=0", "cardano-cli", "", chkLog.Checked)
                        If IsNumeric(lblTestnetID.Text) Then
                            RichTextBox1.Clear()
                            PictureBox2.BackColor = Color.Red
                            DoLog(2, Now & " - Running command line ..")
                            GoTo tryagain2
                        End If
                    End If
                ElseIf InStr(sParameters, "--mainnet") Then
                    If RadioButtonTESTNET.Checked = True Then
                        'MsgBox("Switching to MAINNET", vbInformation)
                        RadioButtonMAINNET.Checked = True
                    End If
                End If

                DoLog(1, sEXE & " " & sParameters)
                If sParameters > "" Then .Arguments = sParameters
                'at last run the command!
                .UseShellExecute = False

                With RichTextBox2
                    'change the text color
                    .SelectionColor = Color.Orange
                    .AppendText(sEXE & " " & sParameters & vbNewLine & vbNewLine)
                    .SelectionColor = Color.White
                End With

                If sEXE.ToLower = "cardano-cli" Then
                    Try
                        Environment.SetEnvironmentVariable("CARDANO_NODE_SOCKET_PATH", lblCLI.Text)
                        'Debug.Print(Environment.GetEnvironmentVariable("CARDANO_NODE_SOCKET_PATH"))
                        .EnvironmentVariables.Remove("CARDANO_NODE_SOCKET_PATH")
                        .EnvironmentVariables.Add("CARDANO_NODE_SOCKET_PATH", lblCLI.Text)
                        'Debug.Print(.EnvironmentVariables("CARDANO_NODE_SOCKET_PATH"))
                    Catch ex As Exception
                        Debug.Print(ex.Message)
                    End Try
                End If
                '.WindowStyle = ProcessWindowStyle.Normal
            End With

            If bLog Then Call SaveToLogs(sEXE.ToLower & " " & sParameters, True)
            proc.Start()

            Dim iTimeoutMax As Integer = 2
            Dim iTimeoutCount As Integer = 0
tryagain:
            i = 0
            Do While Not proc.HasExited
                i = i + 1
                Application.DoEvents()
                System.Threading.Thread.Sleep(5000)
                'timeout
                If i > 2 Then Exit Do
            Loop
            'proc.WaitForExit()

            If proc.HasExited Then
            Else
                iTimeoutCount = iTimeoutCount + 1
                If iTimeoutCount < iTimeoutMax Then
                    RichTextBox1.AppendText("Still waiting for the command to finish.. " & vbNewLine)
                    GoTo tryagain
                Else
                    If MsgBox("The Query has timed out - Do you want to keep waiting for it to finish?", vbYesNo + vbQuestion) = vbYes Then
                        iTimeoutCount = 0
                        GoTo tryagain
                    End If
                    RichTextBox1.AppendText("** TIMEOUT **" & vbNewLine)
                End If
            End If
            'to show its complete
            'RichTextBox1.AppendText(">")
            Dim output() As String = proc.StandardOutput.ReadToEnd.Split(CChar(vbLf))
            For Each line As String In output
                If line > "" Then
                    line = Replace(line, vbCr, vbNewLine)
                    If RichTextBox1.Text > "" Then RichTextBox1.AppendText(vbCrLf)
                    RichTextBox1.AppendText(line)

                    'POST PROCESSING CAN GO HERE
                    If InStr(Replace(line, vbCrLf, ""), "Transaction successfully submitted") Then
                        bSubmittedCorrectly = True

                        lblVarAmountPreviousValue.Text = lblVarAmount.Text
                        iSyncCount = 0

                        If IsNumeric(lblVarAmountToken.Text) Then
                            lblVarAmountTokenPreviousValue.Text = CLng(lblVarAmountToken.Text - lblVarTokenToSendAmount.Text)
                        Else
                            lblVarAmountTokenPreviousValue.Text = lblVarAmountToken.Text
                        End If

                        'TODO MINT OR SEND NFT
                        Dim sType As String = "MINT"
                        bCanClose = True
                    End If
                ElseIf InStr(sParameters, "transaction submit") Then
                Else
                    'ignore
                End If
            Next

            If sRedirectOutputFile = "TABLE" Or sRedirectOutputFile = "TABLE-SYNC" Then
                'we are exepecting a table

                lblVarAmount.Text = "0"
                lblVarAmountToken.Text = "0"
                lblVarAmount2.Text = "0"
                lblVarTxHash.Text = "0"
                lblVarTxIx.Text = ""

                'NOTE: utxo out put here is probably a fixed length output so quite easy to parse however I am just doing it the old fasioned way to be safe and keep this generic
                'WARNING: This assumes there are never any spaces in all the columns except the last one

                Dim iRow As Integer = 0
                For Each line As String In output
                    If line > "" Then
                        iRow = iRow + 1
                        If iRow = 1 Then
                            'this is the column headings, anything that is not a space is a heading
                            Dim sFields() As String = Split(Replace(Replace(line, " ", ","), vbCr, ""), ",")
                            lstTable.Items.Clear()
                            lstTable.Columns.Clear()
                            lstTable.View = View.Details
                            Dim iColCount As Integer = 0

                            If lstTable.Columns.Count = 0 Then
                                For Each col In sFields
                                    If col > "" Then
                                        iColCount = iColCount + 1
                                        Dim iColWidth As Integer = 100
                                        If iColCount = 1 Then
                                            iColWidth = 400
                                        ElseIf iColCount = 2 Then
                                            iColWidth = 50
                                        End If
                                        lstTable.Columns.Add(col, iColWidth)
                                    End If
                                Next
                            End If
                        ElseIf iRow = 2 Then
                            'ignore this line as it just contains dashes
                        Else
                            'this is the column data
                            Dim sFields() As String = Split(Replace(Replace(line, " ", ","), vbCr, ""), ",")
                            Dim itmX As New ListViewItem
                            Dim iColCount As Integer = 0
                            For Each col In sFields
                                If col > "" Then
                                    iColCount = iColCount + 1
                                    If iColCount = 1 Then
                                        itmX.Text = col
                                    Else
                                        If iColCount > lstTable.Columns.Count Then
                                            'add an extra column
                                            If col <> "TxOutDatumHashNone" Then
                                                lstTable.Columns.Add("Extra", 100)
                                                itmX.SubItems.Add(col)
                                            Else
                                                iColCount = iColCount - 1
                                            End If
                                        Else
                                            itmX.SubItems.Add(col)
                                        End If
                                    End If
                                End If
                            Next
                            itmX.SubItems.Add("")
                            itmX.SubItems.Add("")
                            itmX.SubItems.Add("")
                            itmX.SubItems.Add("")
                            itmX.SubItems.Add("")
                            itmX.SubItems.Add("")
                            lstTable.Items.Add(itmX)
                        End If
                    End If
                Next
                If lstTable.Items.Count > 0 Then
                    'find an entry with more than 2 ADA so we can send transactions
                    Dim bFoundTable As Boolean = False
                    Dim bFoundNFT As Boolean = False
                    For Each itmX As ListViewItem In lstTable.Items

                        If itmX.SubItems(6).Text > "" Then
                            If InStr(itmX.SubItems(6).Text, lblVarTokenName.Text.ToUpper) Then
                                bFoundNFT = True
                                bFoundTable = True
                                itmX.Selected = True
                                Exit For
                            End If
                        End If

                        If LovelaceToADA(itmX.SubItems(2).Text) > 2 And bFoundNFT = False Then
                            bFoundTable = True
                            itmX.Selected = True
                            Exit For
                        End If
                    Next
                    If bFoundTable = False Then lstTable.Items(0).Selected = True
                End If
            End If

            Dim output2() As String = proc.StandardError.ReadToEnd.Split(CChar(vbLf))
            For Each line As String In output2
                If line > "" Then
                    line = Replace(line, vbCr, vbNewLine)
                    If InStr(line, "{unNetworkMagic = ") And RadioButtonTESTNET.Checked = False Then
                        'MsgBox("Switching to TESTNET", vbInformation)
                        RadioButtonTESTNET.Checked = True
                        GoTo thebeginning
                    ElseIf InStr(line, "{unNetworkMagic = ") Then
                        'get testnet id if there is a failure or a change whilst running
                        If GetTestNetID(line) > 0 Then
                            'this looks complicated but this is needed in case we had to get the latest magic id so we don't loop forever
                            sParameters = Replace(sParameters, "--testnet-magic=0", "--testnet-magic=$TESTNET_ID")
                            GoTo thebeginning
                        End If
                    End If
                    If RichTextBox1.Text > "" Then RichTextBox1.AppendText(vbCrLf)
                    RichTextBox1.AppendText(line)
                End If
            Next
            bRet = True

            If InStr(sRedirectOutputFile, ".") > 0 Then Call CreateFile(sPathEXEPath & sRedirectOutputFile, RichTextBox1.Text, False)

            If sRedirectOutputFile = "TABLE-SYNC" Then
                Application.DoEvents()

                If lblVarAmountTokenPreviousValue.Text = "" Then lblVarAmountTokenPreviousValue.Text = "0"

                If lblVarAmount.Text <> lblVarAmountPreviousValue.Text Then
                    'blockchain in sync - happy days
                    DoLog(3, "Blockchain in sync.")
                Else
                    sRedirectOutputFile = sRedirectOutputFileOld
                    DoLog(3, "Waiting for Blockchain to be in sync, sleeping for 5 seconds ..")
                    Application.DoEvents()
                    Threading.Thread.Sleep(5000)
                    DoLog(3, "Waiting for Blockchain to be in sync .. " & iSyncCount)
                    Application.DoEvents()

                    iSyncCount = iSyncCount + 1
                    If iSyncCount < 5 Then
                        GoTo thebeginning
                    Else
                        DoLog(2, Now & " - SYNC TIMED OUT.")
                    End If
                End If
            End If
            DoLog(2, Now & " - Finished.")
        Catch ex As Exception
            DoLog(2, Now & " - ERROR (1) - " & ex.Message)
            RichTextBox1.AppendText(Now & " APPLICATION ERROR: " & ex.Message)
        Finally
            Try
                proc.Close()
                If InStr(sEXE, "#") = 0 Then RichTextBox2.AppendText(RichTextBox1.Text & vbNewLine & vbNewLine)
            Catch ex As Exception
                DoLog(2, Now & " - ERROR (2) - " & ex.Message)
                RichTextBox1.AppendText(Now & " APPLICATION CLOSE PROCESS ERROR: " & ex.Message)
            Finally
                Call CheckErrors()
            End Try
        End Try
end_sub:
        PictureBox2.BackColor = Color.FromArgb(255, 63, 78, 119)
        Return bRet
    End Function
    Sub SaveToLogs(sMessage As String, bAppend As Boolean)
        Dim sFilenameAll As String = "cli-all.log"
        Dim sFilenameDaily As String = "cli-local.log"
        If bAppend Then
            SaveToLog(sFilenameAll, sMessage, bAppend)
            SaveToLog(sFilenameDaily, sMessage, bAppend)
        Else
            sMessage = "STARTING"
            SaveToLog(sFilenameDaily, sMessage, False)
        End If
    End Sub
    Sub SaveToLog(sFilename As String, sMessage As String, bAppend As Boolean)
        Dim MyFile As System.IO.StreamWriter = Nothing
        Dim bOpen As Boolean = False
        Try
            Dim sFilePath As String = sPath & "\" & sFilename
            sFilePath = Replace(sFilePath, "\\", "\")
            Application.DoEvents()
            MyFile = My.Computer.FileSystem.OpenTextFileWriter(sFilePath, bAppend)
            bOpen = True
            'Dim sLine As String = Now & " - " & sMessage
            Dim sLine As String = sMessage
            MyFile.WriteLine(sLine)
        Catch ex As Exception
            DoLog(1, "ERROR - SAVING TO LOG - " & ex.Message)
        Finally
            If bOpen Then MyFile.Close()
        End Try
        Debug.Print(sMessage)
        Application.DoEvents()
    End Sub
    Private Function CheckErrors() As Boolean
        Dim bError As Boolean = False
        Dim bLog As Boolean = chkLog.Checked
        Try
            Dim iCount As Integer = 0
            Dim sMessage As String = "POSSIBLE ISSUES DETECTED" & vbNewLine & vbNewLine
            If InStr(RichTextBox1.Text, "invalid address") Then
                iCount = iCount + 1
                sMessage = sMessage & "Possible Issue: " & iCount & " - INVALID ADDRESS - Check the Addresses used exist" & vbNewLine & vbNewLine
                bError = True
            End If
            If InStr(RichTextBox1.Text, "invalidBefore") Then
                If InStr(RichTextBox1.Text, "invalidBefore = SNothing") = 0 Then
                    iCount = iCount + 1
                    sMessage = sMessage & "Possible Issue: " & iCount & " - INVALID SLOT IN THE FUTURE - The slot for the command to run has not yet passed" & vbNewLine & vbNewLine
                    bError = True
                End If
            End If
            If InStr(RichTextBox1.Text, "invalidHereafter") Then
                If InStr(RichTextBox1.Text, "invalidHereafter = SNothing") = 0 Then
                    iCount = iCount + 1
                    sMessage = sMessage & "Possible Issue: " & iCount & " - INVALID SLOT IN THE PAST - The slot for the command to run has passed" & vbNewLine & vbNewLine
                    bError = True
                End If
            End If
            If InStr(RichTextBox1.Text, "ValueNotConservedUTxO") Then
                iCount = iCount + 1
                sMessage = sMessage & "Possible Issue: " & iCount & " - INVALID CHANGE - The remaining address balance is incorect" & vbNewLine & vbNewLine
                iCount = iCount + 1
                If InStr(RichTextBox1.Text, "BadInputsUTxO") Then
                    sMessage = sMessage & "Possible Issue: " & iCount & " - INVALID TxHash - Are you trying to send a Minted coin an address Hash that has DOES NOT have a Minted coin or has a different named COIN than the one you are trying to send? use 'query utxo' commend to check the TxHash of the sending address" & vbNewLine & vbNewLine
                Else
                    sMessage = sMessage & "Possible Issue: " & iCount & " - INVALID TxHash - Are you trying to send ADA using an address Hash that has a Minted coin attached? use 'query utxo' commend to check the TxHash of the sending address" & vbNewLine & vbNewLine
                End If
                bError = True
            End If
            If InStr(RichTextBox1.Text, "Negative quantity") Then
                iCount = iCount + 1
                sMessage = sMessage & "Possible Issue: " & iCount & " - Does the sending address have enough Lovelace/ADA to send?" & vbNewLine & vbNewLine
                bError = True
            End If
            If InStr(RichTextBox1.Text, "OutputTooSmallUTxO") Then
                iCount = iCount + 1
                sMessage = sMessage & "Possible Issue: " & iCount & " - There is a minimum of 1 ADA you have to send (100000 Lovelace)" & vbNewLine & vbNewLine
                bError = True
            End If
            If InStr(RichTextBox1.Text, "MissingVKey") Then
                iCount = iCount + 1
                sMessage = sMessage & "Possible Issue: " & iCount & " - KEY MISMATCH - Are you signing the transaction with the correct vkey for the payment address?" & vbNewLine & vbNewLine
                bError = True
            End If
            If InStr(RichTextBox1.Text, "ScriptWitnessNotValidatingUTXOW") Then
                iCount = iCount + 1
                sMessage = sMessage & "Possible Issue: " & iCount & " - ERROR Submitting Transaction - in the build transaction do you have the invalid-hereafter parameter and if so does it match the same value entered in the policy script" & vbNewLine & vbNewLine
            End If
            If InStr(RichTextBox1.Text, "Command failed") Then
                iCount = iCount + 1
                If InStr(RichTextBox1.Text, "Error while submitting tx") Then
                    sMessage = sMessage & "Possible Issue: " & iCount & " - ERROR Submitting Transaction - look at the error message for more information" & vbNewLine & vbNewLine
                Else
                    sMessage = sMessage & "Possible Issue: " & iCount & " - ERROR Command Failed - look at the error message for more informationKEY MISMATCH - Are you signing the transaction with the correct vkey for the payment address?" & vbNewLine & vbNewLine
                End If
                bError = True
            End If

            If bError Then
                txtFile.BackColor = Color.Red
                txtFile.ForeColor = Color.Yellow
                txtFile.Text = sMessage
                If bLog Then Call SaveToLogs(sMessage, True)
            Else
                txtFile.BackColor = Color.White
                txtFile.ForeColor = Color.Black
            End If
        Catch ex As Exception
        End Try
        Return bError
    End Function
    Public Function LovelaceToADA(sAmount As String) As String
        Dim dReturn As String = ""
        Try
            Dim iAmount As Long = 0
            If IsNumeric(sAmount) And sAmount > "" Then iAmount = (CLng(sAmount))
            If iAmount > 100000 Then dReturn = iAmount / 1000000
        Catch ex As Exception
            Call DoLog(2, "ERROR Converting Lovelace to ADA - " & ex.Message)
        End Try
        Return dReturn
    End Function

    Private Sub btnCloseNFT_Click(sender As Object, e As EventArgs) Handles btnCloseNFT.Click
        PanelNFT.Visible = False
    End Sub

    Function FindUTXOInTable(sTXID As String) As ListViewItem
        Dim MyRetRow As ListViewItem = Nothing
        Try
            If sTXID > "" Then
                For Each MyRow As ListViewItem In lstTable.Items
                    If MyRow.Text = sTXID Then
                        MyRetRow = MyRow
                        Exit For
                    End If
                Next
            End If
        Catch ex As Exception
            DoLog(1, "ERROR - DOES ROW EXIST - " & ex.Message)
        End Try
        Return MyRetRow
    End Function

    Private Sub cboScripts_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboScripts.SelectedIndexChanged
        If cboScripts.SelectedIndex >= 0 Then
            sInputFilePath = sPath & cboScripts.Text
            Call LoadListView(sInputFilePath)
        End If
    End Sub
    Private Sub LoadScripts()
        Try
            ' Make a reference to a directory.
            Dim MyFolder As String = sPath
            Dim di As New DirectoryInfo(MyFolder)
            ' Get a reference to each file in that directory.
            Dim fiArr As FileInfo() = di.GetFiles()
            ' Display the names of the files.
            Dim fri As FileInfo
            For Each fri In fiArr
                If InStr(fri.Name.ToUpper, ".TXT") Then cboScripts.Items.Add(fri.Name)
            Next fri
            If cboScripts.Items.Count > 0 Then cboScripts.SelectedIndex = 0
        Catch ex As Exception
            DoLog(2, ex.Message)
        End Try
    End Sub

    Private Sub btnRunALL_Click(sender As Object, e As EventArgs) Handles btnRunALL.Click
        Call RunALL()
    End Sub
    Sub RunALL()
        Try
            RichTextBox1.Text = ""
            txtFile.BackColor = Color.White
            txtFile.ForeColor = Color.Black
            For Each itmX As ListViewItem In lstFile.Items
                Dim sLine As String = itmX.SubItems(1).Text
                itmX.Selected = True
                Application.DoEvents()
                If CheckErrors() = True Then
                    DoLog(3, "ERROR FOUND - QUITING")
                    bCanClose = False
                    Exit For
                End If
            Next

            If bCanClose Then
                Me.Close()
            End If
        Catch ex As Exception
            DoLog(3, ex.Message)
        End Try
    End Sub

    Private Sub lstFile_SelectedIndexChanged_1(sender As Object, e As EventArgs) Handles lstFile.SelectedIndexChanged
        Try
            Debug.Print(".")
            If lstFile.SelectedItems.Count > 0 Then
                bRunning = False
                'Debug.Print("HERE 1 " & Now & " " & lstFile.SelectedItems(0).Text)
                For Each itmX As ListViewItem In lstFile.Items
                    itmX.ForeColor = Color.Black
                Next

                RunCommandFromListviewItem(lstFile.SelectedItems(0))
                lstFile.SelectedItems(0).ForeColor = Color.Red

                'stop the double pressing of the button
                'Debug.Print("HERE 2 " & Now & " " & lstFile.SelectedItems(0).Text)
                bRunning = True
                'lstFile.SelectedItems.Clear()
                'Debug.Print("HERE 3 " & Now)
            End If
        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try
    End Sub
End Class
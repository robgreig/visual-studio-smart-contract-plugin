﻿Imports System.IO

Module basSettings
    Public MySettingsFile As String = "C:\Users\Public\main_settings.txt" 'EDIT THIS FILE WITH YOUR SETTINGS

    'API DOCUMENTATION https://docs.blockfrost.io/#section/Authentication
    Public BLOCKFROST_API_KEY_MAINNET As String = GetSettings("BLOCKFROST_API_KEY_MAINNET")   ' CREATE YOUR API KEY HERE  - https://blockfrost.io/
    Public BLOCKFROST_API_KEY_TESTNET As String = GetSettings("BLOCKFROST_API_KEY_TESTNET")   ' CREATE YOUR API KEY HERE  - https://blockfrost.io/
    Public BLOCKFROST_API_KEY_IPFS As String = GetSettings("BLOCKFROST_API_KEY_IPFS")   ' CREATE YOUR API KEY HERE  - https://blockfrost.io/
    Public Function SetupInitialSettings() As String
        Dim sRet As String = ""
        MySettingsFile = Replace(MySettingsFile, "/", "\")
        If Not File.Exists(MySettingsFile) Then
            'create default file
            Call CreateDefaultSettingsFile()
            End
        Else
            If BLOCKFROST_API_KEY_MAINNET = "" Then
                MsgBox("You need to create a Blockfrost account at " & vbNewLine & vbNewLine & "https://blockfrost.io" & vbNewLine & vbNewLine & "before you can query the blockchain then edit this file with your API keys - " & MySettingsFile, vbInformation)
                End
            End If
        End If
        Return sRet
    End Function
    Public Function GetSettings(sName As String)
        Dim sRet As String = ""
        Try
            'I am sure there is a more efficient way to do this
            If File.Exists(MySettingsFile) Then
                sRet = Replace(System.IO.File.ReadAllText(MySettingsFile), vbLf, vbNewLine)
                Dim sFile() As String = Split(sRet, ",")
                For i = 0 To sFile.Length - 1
                    If InStr(sFile(i), sName) Then
                        Dim sContents() As String = Split(sFile(i), "=")
                        If sContents.Length > 1 Then
                            sRet = Trim(sContents(1))
                            Exit For
                        End If
                    End If
                Next
            End If
        Catch e As Exception
            ' Let the user know what went wrong.
            MsgBox("The settings file could not be read:" & MySettingsFile, vbCritical)
            Console.WriteLine(e.Message)
        End Try
        Return sRet
    End Function
    Public Function CreateDefaultSettingsFile() As Boolean
        Dim sFile() As String = Split(MySettingsFile, ",")
        Dim bAppendToFileInsteadOfOverwriting As Boolean = False
        Try
            MsgBox("You need to create a Blockfrost account at " & vbNewLine & vbNewLine & "https://blockfrost.io" & vbNewLine & vbNewLine & "before you can query the blockchain then edit this file with your API keys - " & MySettingsFile, vbInformation)
            Dim objWriter As New System.IO.StreamWriter(sFile(0), bAppendToFileInsteadOfOverwriting)
            Dim sContents As String = "BLOCKFROST_API_KEY_MAINNET=,"
            objWriter.Write(sContents & vbNewLine)
            sContents = "BLOCKFROST_API_KEY_TESTNET=,"
            objWriter.Write(sContents & vbNewLine)
            sContents = "BLOCKFROST_API_KEY_IPFS=,"
            objWriter.Write(sContents & vbNewLine)
            sContents = "USEMAINNET=FALSE,"
            objWriter.Write(sContents & vbNewLine)
            objWriter.Close()
        Catch ex As Exception
            MsgBox(ex.Message, vbCritical)
        End Try
        Return System.IO.File.Exists(sFile(0))
    End Function
    Public Function GetAppPath() As String
        Dim P As String = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)
        P = Replace(P, "file:\", "")
        sPathEXEPath = P & "\"
        P = Replace(P, "bin\Debug", "") 'resolve exe folder while debugging
        P = Replace(P, "bin\Release", "")
        sPath = Replace(P & "\", "\\", "\")   'add and extra slash at the end as some operating systems misses this off
        sPathEXEPath = sPath
        Try
            If (Not System.IO.Directory.Exists(sPathEXEPath)) Then System.IO.Directory.CreateDirectory(sPathEXEPath)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return sPath
    End Function
    Public Function GetAppPathActual() As String
        Dim P As String = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)
        Dim sPath2 As String = ""
        P = Replace(P, "file:\", "")
        sPath2 = Replace(P & "\", "\\", "\")   'add and extra slash at the end as some operating systems misses this off
        Return sPath2
    End Function
    Public Function UnixToTime(ByVal strUnixTime As String) As Date
        UnixToTime = DateAdd(DateInterval.Second, Val(strUnixTime), #1/1/1970#)
        If UnixToTime.IsDaylightSavingTime = True Then
            UnixToTime = DateAdd(DateInterval.Hour, 1, UnixToTime)
        End If
    End Function
    Public Function UnixToTimeUTC(ByVal strUnixTime As String, ByVal offset As Integer) As Date
        'uses an offset from GMT to work out timezone
        UnixToTimeUTC = DateAdd(DateInterval.Second, Val(strUnixTime - offset), #1/1/1970#)
        UnixToTimeUTC = DateAdd(DateInterval.Hour, 1, UnixToTimeUTC)
    End Function
    Public Function TimeToUnix(ByVal dteDate As Date) As String
        If dteDate.IsDaylightSavingTime = True Then
            dteDate = DateAdd(DateInterval.Hour, -1, dteDate)
        End If
        TimeToUnix = DateDiff(DateInterval.Second, #1/1/1970#, dteDate)
    End Function
End Module

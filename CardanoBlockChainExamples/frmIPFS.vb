﻿Imports System.IO
Public Class frmIPFS
    Private Sub frmIPFS_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'put your own random file here
        'txtFilename.Text = Replace(sPathEXEPath & "\policy\token.png", "\\", "\") 'testing
        lblIPFS.Text = ""
    End Sub
    Private Sub btnFindFile_Click(sender As Object, e As EventArgs) Handles btnFindFile.Click
        Using dialog As New OpenFileDialog
            If dialog.ShowDialog() <> DialogResult.OK Then Return
            txtFilename.Text = dialog.FileName
        End Using
    End Sub
    Private Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        If txtFilename.Text > "" Then Call UploadToIPFS(txtFilename.Text, chkPinForever.Checked)
    End Sub
    Function UploadToIPFS(sFilename As String, bPinForever As Boolean) As String
        Dim sRet As String = ""
        txtIPFSPath.Text = ""
        PanelIPFS.Visible = False
        PanelRightImage.Visible = False
        ToolStripStatusLabel1.Text = Now & " Uploading file to IPFS"
        ToolStripStatusLabel2.Text = ""

        Try
            If sFilename = "" Then
                DoLog(2, Now & " Blank file was passed to upload to IPFS")
                MsgBox("Blank file was passed to upload to IPFS")
            ElseIf File.Exists(sFilename) = False Then
                DoLog(2, Now & " Missing file was passed to upload to IPFS - " & sFilename)
                MsgBox("Missing file was passed to upload to IPFS - " & sFilename)
            Else
                Dim BlockfrostService = New clsBlockfrost(clsBlockfrost.UseMainNet, True)
                Dim IPFSSavedURL As String = BlockfrostService.IPFSUploadFile(sFilename)
                If IPFSSavedURL > "" Then
                    txtIPFSPath.Text = IPFSSavedURL
                    If txtIPFSPath.Text > "" Then
                        'PanelIPFS.Visible = True
                        PanelRightImage.Visible = True
                        Call ValidateIPFS()
                        sRet = txtIPFSPath.Text
                    End If

                    lblIPFS.Text = txtIPFSPath.Text
                    DoLog(1, Now & "The file has been uploaded to IPFS")

                    'if you dont pin the file it will get garbage collected so always pin for something you want to be around forever
                    If bPinForever Then
                        DoLog(1, "Pinning IPFS File .. ")
                        Dim IPFSPinnedLink As IPFS_Pin = BlockfrostService.IPFSPinFile(sRet)

                        If IsNothing(IPFSPinnedLink) Then
                            DoLog(1, Now & " ERROR Pinning the IPFS File")
                        Else
                            DoLog(1, Now & "The file has been uploaded to IPFS and Pinned - current status - " & IPFSPinnedLink.state)
                        End If
                    End If
                Else
                    DoLog(2, Now & " ERROR Saving IPFS File")
                End If
            End If
        Catch ex As Exception
            ToolStripStatusLabel1.Text = Now & " ERROR - " & ex.Message
        End Try

        txtIPFSPath.Visible = lblIPFS.Text > ""

        Return sRet
    End Function
    Function LoadImagePreviewFromIPFS(sURL As String, MyPictureBox As PictureBox) As Boolean
        Dim bRet As Boolean = False
        Try
            If sURL > "" Then
                Try
                    MyPictureBox.Visible = False
                    MyPictureBox.Image = Nothing
                    Application.DoEvents()
                    MyPictureBox.LoadAsync(sURL)
                    MyPictureBox.Visible = True
                    bRet = True
                Catch ex As Exception
                    Debug.Print(ex.Message)
                End Try
            End If
        Catch ex As Exception
            ToolStripStatusLabel1.Text = Now & " ERROR - " & ex.Message
        End Try
        Return bRet
    End Function
    Private Sub btnCopy_Click(sender As Object, e As EventArgs) Handles btnValidateIPFS.Click
        Call ValidateIPFS()
    End Sub
    Private Function ValidateIPFS() As Boolean
        Dim bRet As Boolean = False
        Try
            Dim BlockfrostService = New clsBlockfrost(clsBlockfrost.UseMainNet, True)
            Dim sURL As String = BlockfrostService.IPFSGetImageURLDev(txtIPFSPath.Text)
            bRet = LoadImagePreviewFromIPFS(sURL, PictureBoxAssetIPFS)
        Catch ex As Exception
            ToolStripStatusLabel1.Text = Now & " ERROR - " & ex.Message
        End Try
        Return bRet
    End Function
    Sub DoLog(iPanel As Integer, sMessage As String)
        If iPanel = 1 Then
            ToolStripStatusLabel1.Text = Mid(sMessage, 1, 200)
        ElseIf iPanel = 2 Then
            ToolStripStatusLabel2.Text = Mid(sMessage, 1, 200)
        End If
    End Sub
End Class
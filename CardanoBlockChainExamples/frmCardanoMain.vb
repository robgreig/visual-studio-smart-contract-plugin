﻿Imports System.Net.Http
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Public Class frmCardanoMain
    Public bPageLoaded As Boolean = False
    Private Sub frmCardanoMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim sMainnet As String = GetSettings("USEMAINNET")
        Dim bMainnet As Boolean = False
        If sMainnet.ToUpper = "TRUE" Then bMainnet = True
        RadioButtonMAINNET.Checked = bMainnet

        Call InitPage()

        'get default settings
        Call SetupInitialSettings()

        'this stops functions being called on initialisation of form fields
        bPageLoaded = True

        'some interesting wallets to query
        txtCardanoWalletAddress.Text = "addr1qyr6925ldalmzwtmf7mta3gk5vpysm0jssa5h8lawc7r8hq0lmu4r9643qyglrppuvnk777m82t4dtjysau8tptf2paqs8wg4y"
        txtAssetID.Text = "d5e6bf0500378d4f0da4e8dde6becec7621cd8cbf5cbb9b87013d4cc537061636542756434313938"

        txtCardanoWalletAddress.Text = "addr1qyymvhgw4gfplatesew30y32ztt674zhxv7x7vfwa9dyvu584wjveyhlc0r303zty73eeaswpjguvryrsrkj9lzxepssl9dxxk"
        txtAssetID.Text = "0fe7b9c1abbf139414d8e138721a85dd8d6e24ee7dc0d895587b4f576a633030303133363239"

        'txtCardanoWalletAddress.Text = "addr_test1qrdp5svq2j6c6qzaue2um34mw8ptraglt2m0hnh9c20zlzdfp6eeylru4a9m500hjtf9d7qyglvl4ldh52yvdc33c4tsfeaxsy" 'ant
        'txtCardanoWalletAddress.Text = "addr_test1qrg0ct6s4lmta2lxpvx8a20vnx4zjj7ywkk5rt99jtqwrq3uv6dtfdmvncyds8z34d2763mnj76e4rmj3kwqcpn4ekcsyqp5m2" 'test 2 shared
        'RadioButtonTESTNET.Checked = True

        txtCardanoWalletAddress.Tag = "addr_test1vqe70kxnlpwxdrut0a64p99kn2xjdua4wjkfufz40deeracupf9wr"
        txtCardanoWalletAddress.Text = "addr_test1vzcpnmhrkmts99m4jla64tp9xkmmm5320zmcq9hffq4ccsg4fh5ly"
        txtCardanoWalletAddress.Text = "addr_test1qp3dcyj33jxe3ktn60rgzk4f75sv6xzyt4ha2dw3me2ppp50yj3d4yns84ly7xsgdr8j3f3kyds0563j7zsyku3l0wjsa6tnt4"
        txtCardanoWalletAddress.Text = "addr_test1qpw3kaxkvr0s3ad9lq3hqst5fgpz4jkkl84fqhvh7avmz3tttqv2huthua7jenl574prgxx3r6y3rwrkvmeclkmsk45qljuzjf"
        txtCardanoWalletAddress.Tag = "addr_test1qz9ag665l4tkmuj49z6qrug50w70kvhzkp9tza0xgnf62mj0f79ez6rnk5edahnvrwwxmql4dclv4u9qkz6efcrx3lesslwnl5"
        txtCardanoWalletAddress.Text = "addr_test1qrs32ushwtc9ejel7ft24tn6t2h8lqdymyw5m9pajrszht9ntx2resjx3mhvd98469ahd9ggv0x4aqsk5gq9eqahdu5qx0t03d"
        txtCardanoWalletAddress.Text = "addr_test1qz9ag665l4tkmuj49z6qrug50w70kvhzkp9tza0xgnf62mj0f79ez6rnk5edahnvrwwxmql4dclv4u9qkz6efcrx3lesslwnl5"

        btnTrackRewards.PerformClick()

        RadioButtonTESTNET.Checked = Not bMainnet

        'run test
        'Timer1.Enabled = True
    End Sub
    Sub InitPage()
        Dim P As String = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)
        P = Replace(P, "file:\", "")
        sPathEXEPath = P & "\"
        P = Replace(P, "bin\Debug", "") 'resolve exe folder while debugging
        P = Replace(P, "bin\Release", "")
        sPath = Replace(P & "\", "\\", "\")    'add and extra slash at the end as some operating systems misses this off

        Call InitPanels()
        Call DoLog(1, "")
        Call DoLog(2, "Loading..")
        Call DoLog(3, "")
        With lstAssets
            .View = View.Details
            .Columns.Clear()
            .Columns.Add("Asset", 350)
            .Columns.Add("Quantity", 80)
            .Columns.Add("Policy ID", 350)
            .Columns.Add("Asset Name", 200)
            .Columns.Add("Fingerprint", 280)
            '.Columns.Add("Qty", 120)
            .Columns.Add("Initial Mint TX Hash", 120)
            .Columns.Add("Onchain Name", 180)
            .Columns.Add("Onchain Image", 170)
            .Columns.Add("Metadata - Name", 120)
            .Columns.Add("Metadata - Description", 400)
            .Columns.Add("Metadata - Ticker", 120)
            .Columns.Add("Metadata - URL", 300)
            .Columns.Add("Metadata - Logo", 400)
        End With
        With lstAssetsLookup
            .View = View.Details
            .Columns.Clear()
            .Columns.Add("Asset Lookup History", 350)
            .Columns.Add("Quantity", 80)
            .Columns.Add("Policy ID", 350)
            .Columns.Add("Asset Name", 200)
            .Columns.Add("Fingerprint", 280)
            .Columns.Add("Initial Mint TX Hash", 120)
            .Columns.Add("Onchain Name", 180)
            .Columns.Add("Onchain Image", 170)
            .Columns.Add("Metadata - Name", 120)
            .Columns.Add("Metadata - Description", 400)
            .Columns.Add("Metadata - Ticker", 120)
            .Columns.Add("Metadata - URL", 300)
            .Columns.Add("Metadata - Logo", 400)
        End With
        With lstAssetAddresses
            .View = View.Details
            .Columns.Clear()
            .Columns.Add("Address", 650)
            .Columns.Add("Quantity", 80)
        End With
        With lstAssetTransactions
            .View = View.Details
            .Columns.Clear()
            .Columns.Add("TX_Hash", 450)
            .Columns.Add("TX_Index", 60)
            .Columns.Add("block_height", 120)
            .Columns.Add("block_time", 120)
        End With
        With lstAssetHistory
            .View = View.Details
            .Columns.Clear()
            .Columns.Add("TX_Hash", 450)
            .Columns.Add("Amount", 80)
            .Columns.Add("Action", 120)
        End With
        With lstAssetsOfPolicy
            .View = View.Details
            .Columns.Clear()
            .Columns.Add("Asset", 350)
            .Columns.Add("Quantity", 80)
            .Columns.Add("Policy ID", 350)
            .Columns.Add("Asset Name", 200)
            .Columns.Add("Fingerprint", 280)
            '.Columns.Add("Qty", 120)
            .Columns.Add("Initial Mint TX Hash", 120)
            .Columns.Add("Onchain Name", 180)
            .Columns.Add("Onchain Image", 170)
            .Columns.Add("Metadata - Name", 120)
            .Columns.Add("Metadata - Description", 400)
            .Columns.Add("Metadata - Ticker", 120)
            .Columns.Add("Metadata - URL", 300)
            .Columns.Add("Metadata - Logo", 400)
        End With
        With lstRewardsTransactions
            .View = View.Details
            .Columns.Clear()
            .Columns.Add("TX", 60)
            .Columns.Add("Block", 60) '1
            .Columns.Add("Fees", 60) '2
            .Columns.Add("DATE", 120) '3
            .Columns.Add("METADATA", 50) '4
            .Columns.Add("", 10) '5
            .Columns.Add("TYPE", 120) '6 SENT / RECEIVED
            .Columns.Add("TO", 120) '7
            .Columns.Add("FROM", 120) '8
            .Columns.Add("AMOUNT", 120) '9
            .Columns.Add("TOTALVALUE", 120) '10
            .Columns.Add("Policy ID", 350) '11
            .Columns.Add("Asset Name", 200) '12
            .Columns.Add("Fingerprint", 280) '13
            .Columns.Add("Initial Mint TX Hash", 120) '14
            .Columns.Add("Onchain Name", 180) '15
            .Columns.Add("Onchain Image", 170) '16
            .Columns.Add("Metadata - Name", 120) '17
            .Columns.Add("Metadata - Description", 400) '18
            .Columns.Add("Metadata - Ticker", 120) '19
            .Columns.Add("Metadata - URL", 300) '20
            .Columns.Add("Metadata - Logo", 400) '21
            .Columns.Add("Transaction", 300) '22
        End With
        With lstWalletTransactions
            .View = View.Details
            .Columns.Clear()
            .Columns.Add("Asset", 300)
            .Columns.Add("Balance", 200)
            .Columns.Add("Policy ID", 350)
            .Columns.Add("Asset Name", 200)
            .Columns.Add("Fingerprint", 280)
            '.Columns.Add("Qty", 120)
            .Columns.Add("Initial Mint TX Hash", 120)
            .Columns.Add("Onchain Name", 180)
            .Columns.Add("Onchain Image", 170)
            .Columns.Add("Metadata - Name", 120)
            .Columns.Add("Metadata - Description", 400)
            .Columns.Add("Metadata - Ticker", 120)
            .Columns.Add("Metadata - URL", 300)
            .Columns.Add("Metadata - Logo", 400)
            .Columns.Add("Transaction", 300)
        End With
        With lstCardanoAddress
            .View = View.Details
            .Columns.Clear()
            .Columns.Add("Type", 50)
            .Columns.Add("Unit", 60)
            .Columns.Add("Amount", 60)
            .Columns.Add("Amount ADA", 90)
            .Columns.Add("Policy ID", 90)
            .Columns.Add("Asset Name", 180)
            .Columns.Add("Fingerprint", 90)
            .Columns.Add("Initial Mint TX", 90)
            .Columns.Add("Onchain Name", 180)
            .Columns.Add("Onchain Image", 170)
            .Columns.Add("Metadata - Name", 120)
            .Columns.Add("Metadata - Description", 400)
            .Columns.Add("Metadata - Ticker", 120)
            .Columns.Add("Metadata - URL", 300)
            .Columns.Add("Metadata - Logo", 400)
        End With
        With lstWalletTransactionsUTXOs
            .View = View.Details
            .Columns.Clear()
            .Columns.Add("Type", 50)
            .Columns.Add("Unit", 60) '1
            .Columns.Add("Amount", 80) '2
            .Columns.Add("Amount ADA", 90) '3
            .Columns.Add("Address", 300) '4
            .Columns.Add("Policy ID", 350) '5
            .Columns.Add("Asset Name", 200) '6
            .Columns.Add("Fingerprint", 280) '7
            .Columns.Add("Initial Mint TX Hash", 120) '8
            .Columns.Add("Onchain - Name", 120) '9
            .Columns.Add("Onchain - Image", 120) '10
            .Columns.Add("Metadata - Name", 120) '11
            .Columns.Add("Metadata - Description", 200) '12
            .Columns.Add("Metadata - Ticker", 120) '13
            .Columns.Add("Metadata - URL", 200) '14
            .Columns.Add("Metadata - Logo", 200) '15
            .Columns.Add("TXID", 450) '16
            .Columns.Add("Comments", 250) '17
        End With
        With lstWalletTransactionsUTXOsNFTS
            .View = View.Details
            .Columns.Clear()
            .Columns.Add("Type", 50)
            .Columns.Add("Unit", 60) '1
            .Columns.Add("Amount", 80) '2
            .Columns.Add("Amount ADA", 90) '3
            .Columns.Add("Address", 300) '4
            .Columns.Add("Policy ID", 350) '5
            .Columns.Add("Asset Name", 200) '6
            .Columns.Add("Fingerprint", 280) '7
            .Columns.Add("Initial Mint TX Hash", 120) '8
            .Columns.Add("Onchain - Name", 120) '9
            .Columns.Add("Onchain - Image", 120) '10
            .Columns.Add("Metadata - Name", 120) '11
            .Columns.Add("Metadata - Description", 200) '12
            .Columns.Add("Metadata - Ticker", 120) '13
            .Columns.Add("Metadata - URL", 200) '14
            .Columns.Add("Metadata - Logo", 200) '15
            .Columns.Add("TXID", 450) '16
            .Columns.Add("Comments", 250) '17
        End With
        With lstStakePoolsActive
            .View = View.Details
            .Columns.Clear()
            .Columns.Add("Pool ID", 350)
            .Columns.Add("URL", 300)
            .Columns.Add("Hash", 400)
            .Columns.Add("Ticker", 80)
            .Columns.Add("Name", 100)
            .Columns.Add("Description", 400)
            .Columns.Add("Homepage", 200)
            .Columns.Add("", 200)
        End With

        With lstStakePoolsRetiring
            .View = View.Details
            .Columns.Clear()
            .Columns.Add("Pool ID", 350)
            .Columns.Add("URL", 300)
            .Columns.Add("Hash", 400)
            .Columns.Add("Ticker", 80)
            .Columns.Add("Name", 100)
            .Columns.Add("Description", 400)
            .Columns.Add("Homepage", 200)
            .Columns.Add("ePoch", 200)
        End With
        With lstStakePoolsRetired
            .View = View.Details
            .Columns.Clear()
            .Columns.Add("Pool ID", 350)
            .Columns.Add("URL", 300)
            .Columns.Add("Hash", 400)
            .Columns.Add("Ticker", 80)
            .Columns.Add("Name", 100)
            .Columns.Add("Description", 400)
            .Columns.Add("Homepage", 200)
            .Columns.Add("ePoch", 200)
        End With
        With lstMetadata
            .View = View.Details
            .Columns.Clear()
            .Columns.Add("Label", 50)
            .Columns.Add("Count", 50)
            .Columns.Add("cip10", 120)
        End With
        With lstMetadataLabelData
            .View = View.Details
            .Columns.Clear()
            .Columns.Add("TX_Hash", 400)
            .Columns.Add("JSON_Metadata", 1200)
        End With
        PanelMetadata.Visible = False
        PanelStakePools.Visible = False
        PanelTrackRewards.Visible = False
        Call DoLog(2, "Ready")
        Call DoLog(3, "")
    End Sub
    Function DoesAssetExistInAssetLookupList(sAssetID As String) As Boolean
        Dim MyRetRow As ListViewItem = Nothing
        Dim bRet As Boolean = False
        Try
            If sAssetID > "" Then
                For Each MyRow As ListViewItem In lstAssetsLookup.Items
                    If MyRow.Text.ToUpper = sAssetID.ToUpper Then
                        bRet = True
                        Exit For
                    End If
                Next
            End If
        Catch ex As Exception
            DoLog(1, "ERROR - DOES ROW EXIST - " & ex.Message)
        End Try
        Return bRet
    End Function
    Public Function FindTXInBlockchainQueue(lstRewards As ListView, sTXID As String) As ListViewItem
        Dim MyRetRow As ListViewItem = Nothing
        Try
            If sTXID > "" Then
                For Each MyRow As ListViewItem In lstRewards.Items
                    If (MyRow.Text = sTXID) Then
                        MyRetRow = MyRow
                        Exit For
                    End If
                Next
            End If
        Catch ex As Exception
            DoLog(1, "ERROR - DOES ROW EXIST - " & ex.Message)
        End Try
        Return MyRetRow
    End Function
    Function FindAssetInAssetLookupList(sAssetID As String) As Asset
        Dim MyRetRow As ListViewItem = Nothing
        Dim retAsset As New Asset
        Try
            If sAssetID > "" Then
                For Each MyRow As ListViewItem In lstAssetsLookup.Items
                    If MyRow.Text.ToUpper = sAssetID.ToUpper Then
                        MyRetRow = MyRow
                        With retAsset
                            .asset = MyRow.Text 'asset
                            .quantity = MyRow.SubItems(1).Text 'qty
                            .policy_id = MyRow.SubItems(2).Text 'policyid
                            .asset_name = MyRow.SubItems(3).Text 'asset name
                            .fingerprint = MyRow.SubItems(4).Text 'Fingerprint
                            .initial_mint_tx_hash = MyRow.SubItems(5).Text 'Initial Mint TX Hash
                            .onchain_metadata.name = MyRow.SubItems(6).Text 'Onchain - Name
                            .onchain_metadata.image = MyRow.SubItems(7).Text 'Onchain - Image
                            .metadata.name = MyRow.SubItems(8).Text 'Metadata - name
                            .metadata.description = MyRow.SubItems(9).Text 'Metadata - desctiption
                            .metadata.ticker = MyRow.SubItems(10).Text 'Metadata - ticker
                            .metadata.url = MyRow.SubItems(11).Text 'Metadata - url
                            .metadata.logo = MyRow.SubItems(12).Text 'Metadata - logo                    
                        End With
                        Exit For
                    End If
                Next
            End If
        Catch ex As Exception
            DoLog(1, "ERROR - DOES ROW EXIST - " & ex.Message)
        End Try
        Return retAsset
    End Function
    Public Sub DoLog(iPanel As Integer, sMessage As String)
        If iPanel = 1 Then
            ToolStripStatusLabel1.Text = Mid(sMessage, 1, 100)
        ElseIf iPanel = 2 Then
            ToolStripStatusLabel2.Text = Mid(sMessage, 1, 100)
        Else
            Console.WriteLine(sMessage)
            ToolStripStatusLabel3.Text = Mid(sMessage, 1, 100)
        End If
        Application.DoEvents()
    End Sub
    Function GetTransactionInformation() As Integer
        Dim iRet As Integer
        Try
            Call DoLog(2, "Getting Transaction Information ..")
            Call DoLog(3, "")

            txtMetaDataMain.Text = ""
            lstWalletTransactions.Items.Clear()
            lstRewardsTransactions.Items.Clear()
            lstCardanoAddress.Items.Clear()

            Dim BlockfrostService = New clsBlockfrost(RadioButtonMAINNET.Checked)

            '------------------------------------------
            Dim MyAddressInformation As Cardano_Address_Information = BlockfrostService.GetAddressInformation(txtCardanoWalletAddress.Text)

            If Not IsNothing(MyAddressInformation) Then
                lblCardanoAddressAddress.Text = MyAddressInformation.address
                lblCardanoAddressStakeAddress.Text = MyAddressInformation.stake_address
                lblCardanoAddressType.Text = MyAddressInformation.type
                lblCardanoAddressScript.Text = MyAddressInformation.script
                For Each UTXO In MyAddressInformation.amount
                    Dim itmX As New ListViewItem
                    Dim sAmount As String = ""
                    itmX.SubItems.Add(UTXO.unit)
                    If UTXO.unit = "lovelace" Then
                        'ada payment
                        itmX.Text = "COIN"
                        itmX.SubItems.Add(UTXO.quantity)
                        itmX.SubItems.Add(LovelaceToADA(UTXO.quantity))
                    Else
                        'if not lovelace then get the transaction information
                        'other type - Concatenation of the policy_id and hex-encoded asset_name
                        itmX.Text = "NFT"
                        itmX.SubItems.Add(UTXO.quantity)
                        itmX.SubItems.Add("")

                        Dim sAsset As String = UTXO.unit
                        Dim MyAsset As New Asset

                        MyAsset = FindAssetInAssetLookupList(sAsset)
                        If MyAsset.asset = "" Then
                            MyAsset = BlockfrostService.GetSpecificAsset(sAsset)
                            If Not IsNothing(MyAsset) Then Call AddAssetToHistory(MyAsset)
                        End If

                        If MyAsset.asset > "" Then
                            Dim sImage1 As String = ""
                            Dim sImage2 As String = ""
                            itmX.SubItems.Add(MyAsset.policy_id)
                            itmX.SubItems.Add(MyAsset.asset_name)
                            itmX.SubItems.Add(MyAsset.fingerprint)

                            itmX.SubItems.Add(MyAsset.initial_mint_tx_hash)
                            If Not IsNothing(MyAsset.onchain_metadata) Then
                                itmX.SubItems.Add(MyAsset.onchain_metadata.name)
                                itmX.SubItems.Add(MyAsset.onchain_metadata.image)
                                sImage1 = MyAsset.onchain_metadata.image
                            Else
                                itmX.SubItems.Add("")
                                itmX.SubItems.Add("")
                            End If
                            If Not IsNothing(MyAsset.metadata) Then
                                itmX.SubItems.Add(MyAsset.metadata.name)
                                itmX.SubItems.Add(MyAsset.metadata.description)
                                itmX.SubItems.Add(MyAsset.metadata.ticker)
                                itmX.SubItems.Add(MyAsset.metadata.url)
                                sImage2 = MyAsset.metadata.logo
                                itmX.SubItems.Add(sImage2)
                            Else
                                itmX.SubItems.Add("")
                                itmX.SubItems.Add("")
                                itmX.SubItems.Add("")
                                itmX.SubItems.Add("")
                                itmX.SubItems.Add("")
                            End If
                        End If
                        itmX.SubItems.Add("")
                        itmX.SubItems.Add("")
                    End If
                    lstCardanoAddress.Items.Add(itmX)
                Next
            End If
            '------------------------------------------

            'TRUE uses Mainnet FALSE uses Testnet
            Dim MyTransactionList As List(Of String) = BlockfrostService.GetAddressTransactionsList(txtCardanoWalletAddress.Text)
            For Each item In MyTransactionList
                Dim itmX As New ListViewItem(item)
                itmX.Text = item

                'fix as Blockfrost format changed
                Dim MyAddressTransactions As New TX_Address_Transactions
                MyAddressTransactions = JsonHelper.ToClass(Of TX_Address_Transactions)(item)

                Dim MyTransaction As TX = BlockfrostService.GetTransactionInformation(MyAddressTransactions.tx_hash) 'Get transaction details
                If Not IsNothing(MyTransaction) Then
                    itmX.Text = MyAddressTransactions.tx_hash
                    itmX.SubItems.Add(MyTransaction.block)
                    itmX.SubItems.Add(MyTransaction.fees)

                    Dim sTime As String = UnixToTime(MyTransaction.block_time)
                    Dim UTCOffset As Integer = -18000   '-18000 represents -5 hrs from GMT which is Eastern Standard Time
                    'If IsNumeric(frmNFT.txtUTCOffset.Text) Then UTCOffset = Int(frmNFT.txtUTCOffset.Text)
                    'sTime = UnixToTimeUTC(MyTransaction.block_time, UTCOffset)
                    itmX.SubItems.Add(sTime)
                Else
                    itmX.SubItems.Add("")
                    itmX.SubItems.Add("")
                    itmX.SubItems.Add("")
                End If

                'optional - get transaction metadata
                itmX.SubItems.Add(GetTransactionMetaData(MyAddressTransactions.tx_hash))

                itmX.SubItems.Add("")
                itmX.SubItems.Add("")
                itmX.SubItems.Add("")
                itmX.SubItems.Add("")
                itmX.SubItems.Add("")
                itmX.SubItems.Add("")
                itmX.SubItems.Add("")
                itmX.SubItems.Add("")
                itmX.SubItems.Add("")
                itmX.SubItems.Add("")
                itmX.SubItems.Add("")
                itmX.SubItems.Add("")
                itmX.SubItems.Add("")
                itmX.SubItems.Add("")
                itmX.SubItems.Add("")
                itmX.SubItems.Add("")
                itmX.SubItems.Add("")
                itmX.SubItems.Add("")
                lstRewardsTransactions.Items.Add(itmX)
            Next

            'select the first one so the transactions are retrieved
            If lstRewardsTransactions.Items.Count > 0 Then lstRewardsTransactions.Items(0).Selected = True
            Call DoColourTX()
            Call GetBalance()

            'Dim MyList3 As List(Of String) = BlockfrostService.GetAddressTransactionsList(txtCardanoWalletAddress.Text)
            'For Each item In MyList3
            '    'fix as Blockfrost format changed
            '    Dim MyAddressTransactions As New TX_Address_Transactions
            '    MyAddressTransactions = JsonHelper.ToClass(Of TX_Address_Transactions)(item)
            '    Dim sTransactionHash As String = MyAddressTransactions.tx_hash
            '    Dim MyList3a As TX = BlockfrostService.GetTransactionMetaDataList(sTransactionHash)
            'Next
            Call DoLog(2, "Ready")
            Call DoLog(2, "Get Transaction Information - Complete")
        Catch ex As Exception
            Call DoLog(2, "ERROR Get Transaction Information - " & ex.Message)
        End Try
        Return iRet
    End Function
    Function AddAssetToHistory(MyAsset As Asset) As Boolean
        Dim bRet As Boolean = False
        Try
            If DoesAssetExistInAssetLookupList(MyAsset.asset) = False Then
                Dim itmX As New ListViewItem
                Dim BlockfrostService = New clsBlockfrost(RadioButtonMAINNET.Checked)
                With itmX
                    .Text = MyAsset.asset  'asset
                    .SubItems.Add(MyAsset.quantity) 'qty
                    .SubItems.Add(MyAsset.policy_id) 'policyid
                    .SubItems.Add(BlockfrostService.DecodeHEXString(MyAsset.asset_name)) 'asset name
                    .SubItems.Add(MyAsset.fingerprint) 'Fingerprint
                    .SubItems.Add(MyAsset.initial_mint_tx_hash) 'Initial Mint TX Hash
                    If Not IsNothing(MyAsset.onchain_metadata) Then
                        .SubItems.Add(MyAsset.onchain_metadata.name) 'Onchain - Name
                        .SubItems.Add(MyAsset.onchain_metadata.image) 'Onchain - Image
                    Else
                        .SubItems.Add("")
                        .SubItems.Add("")
                    End If
                    If Not IsNothing(MyAsset.metadata) Then
                        .SubItems.Add(MyAsset.metadata.name) 'Metadata - name
                        .SubItems.Add(MyAsset.metadata.description) 'Metadata - desctiption
                        .SubItems.Add(MyAsset.metadata.ticker) 'Metadata - ticker
                        .SubItems.Add(MyAsset.metadata.url) 'Metadata - url
                        .SubItems.Add(MyAsset.metadata.logo) 'Metadata - logo                    
                    Else
                        .SubItems.Add("")
                        .SubItems.Add("")
                        .SubItems.Add("")
                        .SubItems.Add("")
                        .SubItems.Add("")
                    End If
                    lstAssetsLookup.Items.Add(itmX)
                End With
                bRet = True
            End If
        Catch ex As Exception
            Call DoLog(2, "ERROR Add Asset To History - " & ex.Message)
        End Try
        Return bRet
    End Function
    Function GetBalance() As Integer
        Dim iRet As Integer
        Try
            Call DoLog(2, "Getting balance..")
            Call DoLog(3, "")

            'TRUE uses Mainnet FALSE uses Testnet
            Dim BlockfrostService = New clsBlockfrost(RadioButtonMAINNET.Checked)
            lblAPIBalance.Text = BlockfrostService.GetAPIBalance

            Call DoLog(2, "Ready")
            Call DoLog(2, "Get Balance - Complete")
        Catch ex As Exception
            Call DoLog(2, "ERROR Get Balance - " & ex.Message)
        End Try
        Return iRet
    End Function
    Sub InitPanels()
        Call HidePanels()
        PanelStakePools.Dock = DockStyle.Fill
        PanelAssets.Dock = DockStyle.Fill
        PanelTrackRewards.Dock = DockStyle.Fill
        PanelMetadata.Dock = DockStyle.Fill
        lstStakePoolsActive.Dock = DockStyle.Fill
        lstStakePoolsRetired.Dock = DockStyle.Fill
        lstStakePoolsRetiring.Dock = DockStyle.Fill
        lstAssets.Dock = DockStyle.Fill
        'lstWalletTransactions.Dock = DockStyle.Left
        'lstRewardsTransactions.Dock = DockStyle.Right
    End Sub
    Private Sub btnStakePools_Click(sender As Object, e As EventArgs) Handles btnStakePools.Click
        Call HidePanels()
        PanelStakePools.Visible = True
        lstStakePoolsActive.Visible = True
        If lstStakePoolsActive.Items.Count = 0 Then Call RetreiveStakePools()
        Call GetBalance()
    End Sub
    Sub HidePanels()
        PanelAssets.Visible = False
        PanelStakePools.Visible = False
        PanelTrackRewards.Visible = False
        PanelMetadata.Visible = False
        PanelRightImage.Visible = True
        'PanelAssetsPolicy.Visible = False
    End Sub
    Private Sub GetStakePools(iType As Integer)
        'https://adapools.org/pool/40d04c1367c25e6da2521f906f71bbb07858dc1d2209025364369173
        Dim MyListView As New ListView
        Try
            'TRUE uses Mainnet FALSE uses Testnet
            Dim BlockfrostService = New clsBlockfrost(RadioButtonMAINNET.Checked)
            Dim iMaxResults As Integer = 99999 'default to bring all pools back
            iMaxResults = NumericUpDownMaxPools.Value
            lblTotalStakePools.Text = "?"
            Application.DoEvents()
            btnStakePoolsRetreive.Enabled = False

            Dim MyObjectList As List(Of Object) = BlockfrostService.GetStakePoolList(iMaxResults, iType)
            If iType = 2 Then
                MyListView = lstStakePoolsRetiring
            ElseIf iType = 3 Then
                MyListView = lstStakePoolsRetired
            Else
                MyListView = lstStakePoolsActive
            End If
            MyListView.Items.Clear()

            For Each item In MyObjectList
                Dim sPoolID As String = ""
                Dim sEpoch As String = ""

                If iType = 2 Or iType = 3 Then
                    'there is a different format for retired and retiing - they have epoch and pool_id fields
                    sPoolID = item("pool_id")
                    sEpoch = item("epoch")
                Else
                    sPoolID = item
                End If

                Dim MyStakePoolMetaData As StakePoolMetaData = BlockfrostService.GetStakePoolMetaData(sPoolID)

                If MyStakePoolMetaData.poolid > "" Then
                    Dim itmX As New ListViewItem
                    itmX.Text = sPoolID
                    itmX.SubItems.Add(MyStakePoolMetaData.url)
                    itmX.SubItems.Add(MyStakePoolMetaData.hash)
                    itmX.SubItems.Add(MyStakePoolMetaData.ticker)
                    itmX.SubItems.Add(MyStakePoolMetaData.name)
                    itmX.SubItems.Add(MyStakePoolMetaData.description)
                    itmX.SubItems.Add(MyStakePoolMetaData.homepage)
                    itmX.SubItems.Add(sEpoch)
                    MyListView.Items.Add(itmX)
                End If
                lblTotalStakePools.Text = MyListView.Items.Count
                If MyListView.Items.Count Mod 100 = 0 Then Application.DoEvents()
            Next
        Catch ex As Exception
            Call DoLog(2, ex.Message)
        End Try

        lblTotalStakePools.Text = MyListView.Items.Count
        btnStakePoolsRetreive.Enabled = True
    End Sub
    Private Sub RadioStakePoolActive_CheckedChanged(sender As Object, e As EventArgs) Handles RadioStakePoolActive.CheckedChanged, RadioStakePoolRetiring.CheckedChanged, RadioStakePoolRetired.CheckedChanged
        If bPageLoaded Then
            lstStakePoolsActive.Visible = RadioStakePoolActive.Checked
            lstStakePoolsRetired.Visible = RadioStakePoolRetired.Checked
            lstStakePoolsRetiring.Visible = RadioStakePoolRetiring.Checked
            If lstStakePoolsActive.Visible Then
                lblTotalStakePools.Text = lstStakePoolsActive.Items.Count
                If lstStakePoolsActive.Items.Count = 0 Then Call RetreiveStakePools()
            ElseIf lstStakePoolsRetired.Visible Then
                lblTotalStakePools.Text = lstStakePoolsRetired.Items.Count
                If lstStakePoolsRetired.Items.Count = 0 Then Call RetreiveStakePools()
            Else
                lblTotalStakePools.Text = lstStakePoolsRetiring.Items.Count
                If lstStakePoolsRetiring.Items.Count = 0 Then Call RetreiveStakePools()
            End If
        End If
    End Sub
    Sub RetreiveStakePools()
        If RadioStakePoolRetiring.Checked Then
            Call GetStakePools(2)
        ElseIf RadioStakePoolRetired.Checked Then
            Call GetStakePools(3)
        Else
            Call GetStakePools(1)
        End If
    End Sub
    Private Sub btnStakePoolsRetreive_Click(sender As Object, e As EventArgs) Handles btnStakePoolsRetreive.Click
        Call RetreiveStakePools()
    End Sub
    Private Sub btnAssets_Click(sender As Object, e As EventArgs) Handles btnAssets.Click
        Call HidePanels()
        PanelAssets.Visible = True
        lstAssets.Visible = True
        If lstAssets.Items.Count = 0 Then RetreiveAssets(3, lstAssets)
        Call GetBalance()
    End Sub
    Sub RetreiveAssets(iType As Integer, MyListView As ListView)
        'https://docs.blockfrost.io/#tag/Cardano-Assets/paths/~1assets/get
        Try
            'TRUE uses Mainnet FALSE uses Testnet
            Dim BlockfrostService = New clsBlockfrost(RadioButtonMAINNET.Checked)
            Dim iMaxResults As Integer = 99999 'default to bring all pools back
            iMaxResults = NumericUpDownMaxAssets.Value
            Application.DoEvents()
            btnAssets.Enabled = False

            Call DoLog(2, "")
            Call DoLog(3, "")

            Dim MyObjectList As New List(Of Object)

            If iType = 2 Then
                'all assets of a policy
                lblTotalAssetsOfPolicy.Text = "?"
                'MyObjectList = BlockfrostService.GetPolicyAssetsList(lblPolicyID.Text, iMaxResults)
                MyObjectList = BlockfrostService.GetPolicyAssetsList(txtPolicyID.Text, iMaxResults)
            ElseIf iType = 3 Then
                'individual asset
                lblTotalAssets.Text = "?"
                'PanelAssetsPolicy.Visible = False

                Dim sJSONResponse As String = "{'asset':'" & txtAssetID.Text & "','quantity':'1'}"
                Dim array As JObject = JObject.Parse(sJSONResponse)
                MyObjectList.Add(array)
            Else
                'all assets
                lblTotalAssets.Text = "?"
                'PanelAssetsPolicy.Visible = False
                MyObjectList = BlockfrostService.GetAssetsList(iMaxResults)
                PanelAssetsPolicy.Visible = True
            End If

            MyListView.Items.Clear()
            Application.DoEvents()
            Dim iCount As Integer = 0

            For Each item In MyObjectList
                Dim sAsset As String = ""
                Dim sQuantity As String = ""
                iCount = iCount + 1
                Try
                    'there is a different format for retired and retiing - they have epoch and pool_id fields
                    sAsset = item("asset")
                    sQuantity = item("quantity")
                    'If sQuantity <> "1" Then Stop
                Catch ex As Exception
                    Debug.Print(ex.Message)
                End Try

                Dim MyAsset As New Asset
                MyAsset = FindAssetInAssetLookupList(sAsset)
                If MyAsset.asset = "" Then
                    MyAsset = BlockfrostService.GetSpecificAsset(sAsset)
                    If Not IsNothing(MyAsset) Then
                        Call AddAssetToHistory(MyAsset)
                    End If
                End If

                If MyAsset.asset > "" Then
                    Dim itmX As New ListViewItem
                    Dim sImage1 As String = ""
                    Dim sImage2 As String = ""
                    itmX.Text = sAsset
                    itmX.SubItems.Add(sQuantity)
                    itmX.SubItems.Add(MyAsset.policy_id)
                    itmX.SubItems.Add(MyAsset.asset_name)
                    itmX.SubItems.Add(MyAsset.fingerprint)
                    sQuantity = MyAsset.quantity
                    itmX.SubItems(1).Text = sQuantity

                    itmX.SubItems.Add(MyAsset.initial_mint_tx_hash)
                    If Not IsNothing(MyAsset.onchain_metadata) Then
                        itmX.SubItems.Add(MyAsset.onchain_metadata.name)
                        itmX.SubItems.Add(MyAsset.onchain_metadata.image)
                        sImage1 = MyAsset.onchain_metadata.image
                    Else
                        itmX.SubItems.Add("")
                        itmX.SubItems.Add("")
                    End If
                    If Not IsNothing(MyAsset.metadata) Then
                        itmX.SubItems.Add(MyAsset.metadata.name)
                        itmX.SubItems.Add(MyAsset.metadata.description)
                        itmX.SubItems.Add(MyAsset.metadata.ticker)
                        itmX.SubItems.Add(MyAsset.metadata.url)
                        sImage2 = MyAsset.metadata.logo
                        itmX.SubItems.Add(sImage2)
                    Else
                        itmX.SubItems.Add("")
                        itmX.SubItems.Add("")
                        itmX.SubItems.Add("")
                        itmX.SubItems.Add("")
                        itmX.SubItems.Add("")
                    End If
                    If chkAssetImage.Checked = True And iType = 1 Then
                        If sImage1 > "" Or sImage2 > "" Then MyListView.Items.Add(itmX)
                    Else
                        MyListView.Items.Add(itmX)
                    End If
                    If MyListView.Items.Count >= NumericUpDownMaxAssets.Value Then Exit For
                End If
                If iType = 2 Then
                    lblTotalAssetsOfPolicy.Text = MyListView.Items.Count
                    lstAssetsOfPolicy.Visible = True
                Else
                    lblTotalAssets.Text = MyListView.Items.Count
                End If
                If iCount Mod 20 = 0 Then
                    Call DoLog(2, Now & " Searching Asset " & iCount & " of " & MyObjectList.Count & "..")
                    Application.DoEvents()
                End If
            Next
        Catch ex As Exception
            Call DoLog(2, ex.Message)
        End Try

        If iType = 2 Then
            lblTotalAssetsOfPolicy.Text = MyListView.Items.Count
            lstAssetsOfPolicy.Visible = True
        Else
            lblTotalAssets.Text = MyListView.Items.Count
        End If
        btnAssets.Enabled = True
    End Sub
    Private Sub btnAssetsRetreive_Click(sender As Object, e As EventArgs) Handles btnAssetsLatestRetreive.Click
        btnAssetsLatestRetreive.Enabled = False
        Call RetreiveAssets(1, lstAssets)
        btnAssetsLatestRetreive.Enabled = True
    End Sub
    Sub LoadImagePreviewBase64(sString As String, MyPictureBox As PictureBox)
        MyPictureBox.Visible = False
        If sString > "" Then
            Try
                Dim converter As ImageConverter = New ImageConverter()
                Dim vOut() As Byte = System.Convert.FromBase64String(sString)
                Dim image = CType(converter.ConvertFrom(vOut), Image)
                MyPictureBox.Image = image
                MyPictureBox.Visible = True
            Catch ex As Exception
                'Debug.Print(ex.Message)
                DoLog(3, ex.Message)
            End Try
        End If
    End Sub
    Sub LoadImagePreviewFromIPFS(sURL As String, MyPictureBox As PictureBox)
        If sURL > "" Then
            Try
                Call DoLog(2, "Loading IPFS Image ..")
                Call DoLog(3, "")

                MyPictureBox.Visible = False
                MyPictureBox.Image = Nothing
                Application.DoEvents()

                MyPictureBox.LoadAsync(sURL)
                MyPictureBox.Visible = True

                Call DoLog(2, "Loaded IPFS Image.")
            Catch ex As Exception
                'Debug.Print(ex.Message)
                Call DoLog(3, ex.Message)
            End Try
        End If
    End Sub
    Private Sub lstAssets_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstAssets.SelectedIndexChanged, lstAssets.Click
        With lstAssets
            If .SelectedItems.Count > 0 Then Call DrawAssetDetails(.SelectedItems(0), PictureBoxAssetIPFS, PictureBoxAssetIPFS)
        End With
    End Sub
    Private Sub btnGetOtherPolicyAssets_Click(sender As Object, e As EventArgs) Handles btnGetOtherPolicyAssets.Click
        Call RetreiveAssets(2, lstAssetsOfPolicy)
    End Sub
    Private Sub lstAssetsOfPolicy_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstAssetsOfPolicy.SelectedIndexChanged
        With lstAssetsOfPolicy
            If .SelectedItems.Count > 0 Then
                Call DrawAssetDetails(.SelectedItems(0), PictureBoxAssetIPFS, PictureBoxAssetLogo)
                Dim sAssetID As String = .SelectedItems(0).Text
                Dim BlockfrostService = New clsBlockfrost(RadioButtonMAINNET.Checked)

                Dim MyObjectList As List(Of Object) = BlockfrostService.GetAssetAddressList(sAssetID, 100, 1, "asc")
                lstAssetAddresses.Items.Clear()
                For Each item In MyObjectList
                    Dim itmX As New ListViewItem
                    itmX.Text = item("address")
                    itmX.SubItems.Add(item("quantity"))
                    lstAssetAddresses.Items.Add(itmX)
                Next
                lstAssetAddresses.Visible = True
                lblAssetLocation.Text = "Asset Locations: " & MyObjectList.Count

                MyObjectList = BlockfrostService.GetAssetHistoryList(sAssetID, 100, 1, "asc")
                lstAssetHistory.Items.Clear()
                For Each item In MyObjectList
                    Dim itmX As New ListViewItem
                    itmX.Text = item("tx_hash")
                    itmX.SubItems.Add(item("amount"))
                    itmX.SubItems.Add(item("action"))
                    lstAssetHistory.Items.Add(itmX)
                Next
                lstAssetHistory.Visible = True

                MyObjectList = BlockfrostService.GetAssetTransactionList(sAssetID, 100, 1, "asc")
                lstAssetTransactions.Items.Clear()
                For Each item In MyObjectList
                    Dim itmX As New ListViewItem
                    itmX.Text = item("tx_hash")
                    itmX.SubItems.Add(item("tx_index"))
                    itmX.SubItems.Add(item("block_height"))
                    itmX.SubItems.Add(UnixToTime(item("block_time")))
                    lstAssetTransactions.Items.Add(itmX)
                Next
                lstAssetTransactions.Visible = True
            End If
        End With
    End Sub
    Sub DrawAssetDetailsIPFS(sImageIPFS As String, MyPictureBoxAssetIPFS As PictureBox)
        MyPictureBoxAssetIPFS.Visible = False
        Application.DoEvents()
        Try
            Call DoLog(2, "Getting Image from IPFS.. ")
            Timer2.Enabled = True
            If sImageIPFS > "" Then
                If sImageIPFS > "" Then
                    lblPolicyIPFS.Tag = 0
                    lblPolicyIPFS.Text = "Getting Image from IPFS.. "
                    'TRUE uses Mainnet FALSE uses Testnet
                    Dim BlockfrostService = New clsBlockfrost(RadioButtonMAINNET.Checked)
                    Dim sURL2 As String = BlockfrostService.IPFSGetImageURLDev(sImageIPFS)
                    Call LoadImagePreviewFromIPFS(sURL2, MyPictureBoxAssetIPFS)
                    Application.DoEvents()
                End If
            Else
                lblPolicyIPFS.Text = "There is no IPFS for this Asset"
            End If
            lblPolicyIPFS.Visible = True
            Call GetBalance()
        Catch ex As Exception
            DoLog(3, ex.Message)
        End Try
        Timer2.Enabled = False
    End Sub
    Sub DrawAssetDetails(itmX As ListViewItem, MyPictureBoxAssetIPFS As PictureBox, MyPictureBoxAssetLogo As PictureBox)
        MyPictureBoxAssetIPFS.Visible = False
        Application.DoEvents()

        lblPolicyIPFS.Text = ""
        lblPolicyLogo.Visible = False
        MyPictureBoxAssetLogo.Visible = False
        MyPictureBoxAssetIPFS.Visible = False
        Application.DoEvents()
        Try

            Dim sImageIPFS As String = ""
            Dim sImageLogo As String = ""

            txtPolicyID.Text = ""
            txtAssetID.Tag = ""

            'With lstAssetsOfPolicy
            With itmX
                Dim sAsset As String = itmX.Text
                txtAssetID.Text = sAsset
                'this is allowed to fail
                Try
                    txtPolicyID.Text = itmX.SubItems(2).Text
                    LinkLabel1.Text = "View on Pool.pm"
                    txtAssetID.Tag = itmX.SubItems(2).Text & "." & itmX.SubItems(3).Text
                    sImageIPFS = itmX.SubItems(7).Text
                    sImageLogo = itmX.SubItems(12).Text
                Catch ex As Exception
                End Try

                lblPolicyIPFS.Tag = 0
                lblPolicyIPFS.Text = "Getting Image from IPFS.. "
                Timer2.Enabled = True
            End With

            Call DoLog(2, "Getting Image from IPFS.. ")
            Timer2.Enabled = True

            If sImageLogo > "" Then
                Call LoadImagePreviewBase64(sImageLogo, MyPictureBoxAssetLogo)
                lblPolicyLogo.Visible = True
            End If

            If sImageIPFS > "" Then
                If sImageIPFS > "" Then
                    lblPolicyIPFS.Tag = 0
                    lblPolicyIPFS.Text = "Getting Image from IPFS.. "
                    'TRUE uses Mainnet FALSE uses Testnet
                    Dim BlockfrostService = New clsBlockfrost(RadioButtonMAINNET.Checked)
                    Dim sURL2 As String = BlockfrostService.IPFSGetImageURLDev(sImageIPFS)
                    Call LoadImagePreviewFromIPFS(sURL2, MyPictureBoxAssetIPFS)
                    Application.DoEvents()
                End If
            Else
                lblPolicyIPFS.Text = "There is no IPFS for this Asset"
                Call DoLog(2, lblPolicyIPFS.Text)
            End If
            lblPolicyIPFS.Visible = True
        Catch ex As Exception
            DoLog(3, ex.Message)
        End Try
        Timer2.Enabled = False
    End Sub

    'Sub DrawAssetDetailsOLD(itmX As ListViewItem, MyPictureBoxAssetIPFS As PictureBox, MyPictureBoxAssetLogo As PictureBox)
    '    lblPolicyIPFS.Text = ""
    '    lblPolicyLogo.Visible = False
    '    MyPictureBoxAssetLogo.Visible = False
    '    MyPictureBoxAssetIPFS.Visible = False
    '    Application.DoEvents()
    '    Try
    '        'With lstAssetsOfPolicy
    '        With itmX
    '            Dim sAsset As String = itmX.Text
    '            txtAssetID.Text = sAsset
    '            'this is allowed to fail
    '            Dim sImageIPFS As String = itmX.SubItems(7).Text
    '            Dim sImageLogo As String = itmX.SubItems(12).Text

    '            LinkLabel1.Text = "View on Pool.pm"
    '            'txtAssetID.Tag = .SelectedItems(0).SubItems(2).Text & "." & .SelectedItems(0).SubItems(3).Text
    '            txtAssetID.Tag = itmX.SubItems(2).Text & "." & itmX.SubItems(3).Text

    '            txtPolicyID.Text = itmX.SubItems(2).Text
    '            lblPolicyIPFS.Tag = 0
    '            lblPolicyIPFS.Text = "Getting Image from IPFS.. "
    '            Timer2.Enabled = True
    '            If sImageLogo > "" Then
    '                Call LoadImagePreviewBase64(sImageLogo, MyPictureBoxAssetLogo)
    '                lblPolicyLogo.Visible = True
    '            End If
    '            If sImageIPFS > "" Then
    '                If sImageIPFS > "" Then
    '                    lblPolicyIPFS.Tag = 0
    '                    lblPolicyIPFS.Text = "Getting Image from IPFS.. "
    '                    'TRUE uses Mainnet FALSE uses Testnet
    '                    Dim BlockfrostService = New clsBlockfrost(RadioButtonMAINNET.Checked)
    '                    Dim sURL2 As String = BlockfrostService.IPFSGetImageURLDev(sImageIPFS)
    '                    Call LoadImagePreviewFromIPFS(sURL2, MyPictureBoxAssetIPFS)
    '                    Application.DoEvents()
    '                    lblPolicyIPFS.Text = itmX.SubItems(6).Text
    '                End If
    '            Else
    '                lblPolicyIPFS.Text = "There is no IPFS for this Asset"
    '            End If
    '            lblPolicyIPFS.Visible = True
    '        End With
    '    Catch ex As Exception
    '        DoLog(3, ex.Message)
    '    End Try
    '    Timer2.Enabled = False
    'End Sub
    Private Sub btnAssetsIndividualRetreive_Click(sender As Object, e As EventArgs) Handles btnAssetsIndividualRetreive.Click
        RetreiveAssets(3, lstAssets)
        Call GetBalance()
    End Sub
    Private Sub cmdTrackRewardsGO_Click(sender As Object, e As EventArgs) Handles cmdTrackRewardsGO.Click
        lblTotalTransactions.Text = "?"
        lstRewardsTransactions.Items.Clear()
        lstWalletTransactions.Items.Clear()
        lstWalletTransactionsUTXOs.Items.Clear()
        If Len(txtCardanoWalletAddress.Text) < 50 Then
            MsgBox("You must enter a correct Address", vbCritical)
            Exit Sub
        ElseIf InStr(txtCardanoWalletAddress.Text, "addr_test") And RadioButtonMAINNET.Checked Then
            MsgBox("Are you sure this is a MAINNET address you are lookinp up? If not choose the TESTNET option above", vbCritical)
        End If
        Call GetTransactionInformation()
        Call GetBalance()
        lblTotalTransactions.Text = lstRewardsTransactions.Items.Count
    End Sub
    Private Sub btnTrackRewards_Click(sender As Object, e As EventArgs) Handles btnTrackRewards.Click
        Call HidePanels()
        PanelTrackRewards.Visible = True
    End Sub
    Private Sub lstRewardsTransactions_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstRewardsTransactions.SelectedIndexChanged
        Try
            With lstRewardsTransactions
                lstWalletTransactions.Items.Clear()
                lstWalletTransactionsUTXOs.Items.Clear()
                If .SelectedItems.Count > 0 Then
                    Dim itmX As ListViewItem = .SelectedItems(0)
                    Dim sTX_Hash As String = itmX.Text
                    Dim sJSON As String = ""
                    txtMetaDataMain.Text = sJSON
                    Call GetTransactionData(sTX_Hash)
                    Try
                        If itmX.SubItems(4).Text = "" Then itmX.SubItems(4).Text = GetTransactionMetaData(sTX_Hash)
                        sJSON = JToken.Parse(itmX.SubItems(4).Text).ToString(Formatting.Indented)
                    Catch ex As Exception

                    End Try
                    txtMetaDataMain.Text = sJSON
                    txtMetaDataMain.Tag = itmX.SubItems(4).Text
                End If
            End With
        Catch ex As Exception
            DoLog(3, ex.Message)
        End Try
    End Sub
    Sub GetTransactionData(sTX_Hash As String)
        Try
            Dim sAmount As String = ""
            lstRewardsTransactions.Refresh()
            'find lovelace unit
            'only show the transactions for the 1st entry, the others will appear when 
            'TRUE uses Mainnet FALSE uses Testnet

            Dim BlockfrostService = New clsBlockfrost(RadioButtonMAINNET.Checked)
            Dim MyTransaction As TX = BlockfrostService.GetTransactionInformation(sTX_Hash) 'Get transaction details again - we need the list of transactions
            If Not IsNothing(MyTransaction) Then
                For Each UTXO As TX_Amount In MyTransaction.output_amount
                    Dim itmX2 As New ListViewItem
                    itmX2.Text = (UTXO.unit) 'policy id
                    If UTXO.unit = "lovelace" Then
                        'ada payment
                        sAmount = UTXO.quantity & " (" & LovelaceToADA(UTXO.quantity) & " ADA)"
                        itmX2.SubItems.Add(sAmount)
                    Else
                        'if not lovelace then get the transaction information
                        'other type - Concatenation of the policy_id and hex-encoded asset_name
                        itmX2.SubItems.Add(UTXO.quantity)
                        txtAssetID.Text = UTXO.unit

                        Call RetreiveAssets(3, lstAssets)

                        'the results will be in the listview so retrieve them
                        If lstAssets.Items.Count > 0 Then
                            For i = 2 To lstAssets.Columns.Count - 1
                                itmX2.SubItems.Add(lstAssets.Items(0).SubItems(i).Text)
                            Next i
                        End If
                    End If
                    lstWalletTransactions.Items.Add(itmX2)
                Next
            End If

            'select the first one so the transactions are retrieved
            If lstWalletTransactions.Items.Count > 0 Then lstWalletTransactions.Items(0).Selected = True
            Call GetBalance()
        Catch ex As Exception
            DoLog(3, ex.Message)
        End Try
    End Sub
    Sub DoColourTX()
        Try
            For Each itmX As ListViewItem In lstRewardsTransactions.Items
                Dim sStatus As String = itmX.SubItems(1).Text
                Dim sType As String = itmX.SubItems(6).Text
                'check status

                If sType = "SENT" Then
                    itmX.BackColor = Color.LightGreen
                ElseIf sType = "RECEIVED" Then
                    itmX.BackColor = Color.LightPink
                Else
                    If sStatus = "NOT LINKED" Then
                        itmX.BackColor = Color.LightPink
                    ElseIf sStatus = "LINKED" Then
                        itmX.BackColor = Color.LightBlue
                    ElseIf sStatus = "NFT REQUIRED" Then
                        itmX.BackColor = Color.GreenYellow
                    ElseIf sStatus = "NFT ALLOCATED" Then
                        itmX.BackColor = Color.LightBlue
                    ElseIf sStatus = "NOT SENT" Then
                        itmX.BackColor = Color.LightBlue
                    ElseIf sStatus = "NOT STARTED" Then
                        itmX.BackColor = Color.Gray
                    ElseIf sStatus = "NFT RECEIVED" Then
                        itmX.BackColor = Color.Yellow
                    ElseIf sStatus = "NFT MINTED" Then
                        itmX.BackColor = Color.Yellow
                    ElseIf sStatus = "COIN SENT" Then
                        itmX.BackColor = Color.YellowGreen
                    ElseIf sStatus = "NFT SENT" Then
                        itmX.BackColor = Color.YellowGreen
                    ElseIf sStatus = "COIN RECEIVED" Then
                        itmX.BackColor = Color.Yellow
                    ElseIf sStatus = "TIMED OUT" Then
                        itmX.BackColor = Color.LightCoral
                    ElseIf sStatus > "" Then
                        itmX.BackColor = Color.White
                    Else
                        itmX.BackColor = Color.LightPink
                    End If
                End If
            Next
        Catch ex As Exception
            DoLog(3, ex.Message)
        End Try
    End Sub
    Private Sub lstWalletTransactions_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstWalletTransactions.SelectedIndexChanged
        Call DoWallets(lstWalletTransactions, lstWalletTransactionsUTXOs, lstRewardsTransactions, txtCardanoWalletAddress.Text, False)
    End Sub
    Sub DoWallets(MyWalletTransactions As ListView, MyWalletTransactionsUTXOs As ListView, MyRewardsTransactions As ListView, sCardanoWalletAddress As String, bVerifiy As Boolean)
        Try
            DoLog(2, "Reading UTXOs ..")
            DoLog(3, "")
            LinkLabel1.Text = ""
            With MyWalletTransactions
                MyWalletTransactionsUTXOs.Items.Clear()
                If .SelectedItems.Count > 0 Then
                    If lstRewardsTransactions.SelectedItems.Count > 0 Then
                        Dim MyTransactionItem As ListViewItem = MyRewardsTransactions.SelectedItems(0)
                        Dim bRECEIVED As Boolean = False
                        Dim bSENT As Boolean = False
                        Dim sTXID As String = MyTransactionItem.Text
                        Dim MyCardanoTX As New CardanoTX

                        'TRUE uses Mainnet FALSE uses Testnet
                        Dim BlockfrostService = New clsBlockfrost(RadioButtonMAINNET.Checked)
                        Dim MyTransactionUTXOs As TX_UTXOs = BlockfrostService.GetTransactionInformationUTXO(MyTransactionItem.Text) 'Get transaction details
                        If Not IsNothing(MyTransactionUTXOs) Then
                            For Each UTXO As TX_UTXO_Amount In MyTransactionUTXOs.inputs
                                For Each MyUTXOAmount As TX_UTXO_Units In UTXO.amount
                                    Dim itmX As New ListViewItem
                                    itmX.Text = "Inputs"
                                    itmX.SubItems.Add(MyUTXOAmount.unit)
                                    itmX.SubItems.Add(MyUTXOAmount.quantity)
                                    itmX.SubItems.Add(LovelaceToADA(MyUTXOAmount.quantity))
                                    itmX.SubItems.Add(UTXO.address.ToString)
                                    MyCardanoTX.TX_Hash = MyTransactionItem.Text
                                    If UTXO.address.ToString = sCardanoWalletAddress Then
                                        itmX.BackColor = Color.LightGreen
                                        MyCardanoTX.TX_Type = "SENT"
                                        MyCardanoTX.TX_Address_From = sCardanoWalletAddress
                                        bSENT = True
                                    Else
                                        itmX.BackColor = Color.LightPink
                                        MyCardanoTX.TX_Type = "RECEIVED"
                                        MyCardanoTX.TX_Address_To = sCardanoWalletAddress
                                        MyCardanoTX.TX_Address_From = UTXO.address
                                        bRECEIVED = True
                                    End If
                                    MyCardanoTX.ADA_Fee = LovelaceToADA(MyTransactionItem.SubItems(2).Text)
                                    If MyUTXOAmount.unit = "lovelace" Then
                                        'ada payment
                                        itmX.SubItems.Add("")
                                        itmX.SubItems.Add("")
                                        itmX.SubItems.Add("")
                                        itmX.SubItems.Add("")
                                        itmX.SubItems.Add("")
                                        itmX.SubItems.Add("")
                                        itmX.SubItems.Add("")
                                        itmX.SubItems.Add("")
                                        itmX.SubItems.Add("")
                                        itmX.SubItems.Add("")
                                    Else
                                        'if not lovelace then get the transaction information
                                        'other type - Concatenation of the policy_id and hex-encoded asset_name
                                        If MyCardanoTX.TX_Type = "RECEIVED" And UTXO.address.ToString = sCardanoWalletAddress Then
                                            Dim sAsset As String = MyUTXOAmount.unit
                                            Dim MyAsset As New Asset
                                            MyAsset = FindAssetInAssetLookupList(sAsset)
                                            If MyAsset.asset = "" Then
                                                MyAsset = BlockfrostService.GetSpecificAsset(sAsset)
                                                If Not IsNothing(MyAsset) Then Call AddAssetToHistory(MyAsset)
                                            End If
                                            If Not IsNothing(MyAsset) Then
                                                If MyAsset.asset > "" Then
                                                    MyCardanoTX.CNTAsset = MyAsset
                                                    MyCardanoTX.CNTAsset_QTY = MyAsset.quantity
                                                    Dim sImage1 As String = ""
                                                    Dim sImage2 As String = ""
                                                    itmX.SubItems.Add(MyAsset.policy_id)
                                                    itmX.SubItems.Add(MyAsset.asset_name)
                                                    itmX.SubItems.Add(MyAsset.fingerprint)
                                                    itmX.SubItems.Add(MyAsset.initial_mint_tx_hash)
                                                    If Not IsNothing(MyAsset.onchain_metadata) Then
                                                        itmX.SubItems.Add(MyAsset.onchain_metadata.name)
                                                        itmX.SubItems.Add(MyAsset.onchain_metadata.image)
                                                        sImage1 = MyAsset.onchain_metadata.image
                                                    Else
                                                        itmX.SubItems.Add("")
                                                        itmX.SubItems.Add("")
                                                    End If
                                                    If Not IsNothing(MyAsset.metadata) Then
                                                        itmX.SubItems.Add(MyAsset.metadata.name)
                                                        itmX.SubItems.Add(MyAsset.metadata.description)
                                                        itmX.SubItems.Add(MyAsset.metadata.ticker)
                                                        itmX.SubItems.Add(MyAsset.metadata.url)
                                                        sImage2 = MyAsset.metadata.logo
                                                        itmX.SubItems.Add(sImage2)
                                                    Else
                                                        itmX.SubItems.Add("")
                                                        itmX.SubItems.Add("")
                                                        itmX.SubItems.Add("")
                                                        itmX.SubItems.Add("")
                                                        itmX.SubItems.Add("")
                                                    End If
                                                End If
                                            End If
                                        Else
                                            itmX.SubItems.Add("")
                                            itmX.SubItems.Add("")
                                            itmX.SubItems.Add("")
                                            itmX.SubItems.Add("")
                                            itmX.SubItems.Add("")
                                            itmX.SubItems.Add("")
                                            itmX.SubItems.Add("")
                                            itmX.SubItems.Add("")
                                            itmX.SubItems.Add("")
                                            itmX.SubItems.Add("")
                                        End If
                                    End If
                                    itmX.SubItems.Add("")
                                    itmX.SubItems.Add(MyRewardsTransactions.SelectedItems(0).Text)
                                    MyWalletTransactionsUTXOs.Items.Add(itmX)
                                Next
                            Next
                            For Each UTXO As TX_UTXO_Amount In MyTransactionUTXOs.outputs
                                For Each MyUTXOAmount As TX_UTXO_Units In UTXO.amount
                                    Dim itmX As New ListViewItem
                                    itmX.Text = "Outputs"
                                    itmX.SubItems.Add(MyUTXOAmount.unit)
                                    itmX.SubItems.Add(MyUTXOAmount.quantity)
                                    itmX.SubItems.Add(LovelaceToADA(MyUTXOAmount.quantity))
                                    itmX.SubItems.Add(UTXO.address.ToString)
                                    If UTXO.address.ToString = sCardanoWalletAddress Then
                                        itmX.BackColor = Color.LightGreen
                                        If MyCardanoTX.TX_Type = "SENT" Then
                                            MyCardanoTX.TX_Address_To = UTXO.address
                                        Else
                                            If MyUTXOAmount.unit = "lovelace" Then MyCardanoTX.ADA_Amount = LovelaceToADA(MyUTXOAmount.quantity)
                                        End If
                                    Else
                                        itmX.BackColor = Color.LightPink
                                        If MyCardanoTX.TX_Type = "SENT" Then
                                            If MyUTXOAmount.unit = "lovelace" Then MyCardanoTX.ADA_Amount = LovelaceToADA(MyUTXOAmount.quantity)
                                        Else
                                        End If
                                    End If
                                    If MyCardanoTX.TX_Type = "SENT" Then
                                        MyCardanoTX.TX_Address_To = UTXO.address
                                    Else
                                    End If
                                    If MyUTXOAmount.unit = "lovelace" Then
                                        'ada payment
                                        itmX.SubItems.Add("")
                                        itmX.SubItems.Add("")
                                        itmX.SubItems.Add("")
                                        itmX.SubItems.Add("")
                                        itmX.SubItems.Add("")
                                        itmX.SubItems.Add("")
                                        itmX.SubItems.Add("")
                                        itmX.SubItems.Add("")
                                        itmX.SubItems.Add("")
                                        itmX.SubItems.Add("")
                                        itmX.SubItems.Add("")
                                    Else
                                        'if not lovelace then get the transaction information
                                        'other type - Concatenation of the policy_id and hex-encoded asset_name
                                        If MyCardanoTX.TX_Type = "RECEIVED" And UTXO.address.ToString = sCardanoWalletAddress Or MyCardanoTX.TX_Type = "SENT" And UTXO.address.ToString <> sCardanoWalletAddress Then
                                            Dim sAsset As String = MyUTXOAmount.unit
                                            Dim MyAsset As New Asset

                                            MyAsset = FindAssetInAssetLookupList(sAsset)
                                            If MyAsset.asset = "" Then
                                                MyAsset = BlockfrostService.GetSpecificAsset(sAsset)
                                                If Not IsNothing(MyAsset) Then
                                                    Call AddAssetToHistory(MyAsset)
                                                End If
                                            End If
                                            If Not IsNothing(MyAsset) Then
                                                If MyAsset.asset > "" Then
                                                    MyCardanoTX.CNTAsset = MyAsset
                                                    MyCardanoTX.CNTAsset_QTY = MyAsset.quantity
                                                    Dim sImage1 As String = ""
                                                    Dim sImage2 As String = ""
                                                    itmX.SubItems.Add(MyAsset.policy_id)
                                                    itmX.SubItems.Add(MyAsset.asset_name)
                                                    itmX.SubItems.Add(MyAsset.fingerprint)

                                                    itmX.SubItems.Add(MyAsset.initial_mint_tx_hash)
                                                    If Not IsNothing(MyAsset.onchain_metadata) Then
                                                        itmX.SubItems.Add(MyAsset.onchain_metadata.name)
                                                        itmX.SubItems.Add(MyAsset.onchain_metadata.image)
                                                        sImage1 = MyAsset.onchain_metadata.image
                                                    Else
                                                        itmX.SubItems.Add("")
                                                        itmX.SubItems.Add("")
                                                    End If
                                                    If Not IsNothing(MyAsset.metadata) Then
                                                        itmX.SubItems.Add(MyAsset.metadata.name)
                                                        itmX.SubItems.Add(MyAsset.metadata.description)
                                                        itmX.SubItems.Add(MyAsset.metadata.ticker)
                                                        itmX.SubItems.Add(MyAsset.metadata.url)
                                                        sImage2 = MyAsset.metadata.logo
                                                        itmX.SubItems.Add(sImage2)
                                                    Else
                                                        itmX.SubItems.Add("")
                                                        itmX.SubItems.Add("")
                                                        itmX.SubItems.Add("")
                                                        itmX.SubItems.Add("")
                                                        itmX.SubItems.Add("")
                                                        itmX.SubItems.Add("")
                                                        itmX.SubItems.Add("")
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                    MyCardanoTX.ADA_TotalValue = MyCardanoTX.ADA_Amount + MyCardanoTX.ADA_Fee
                                    MyCardanoTX.Time = MyTransactionItem.SubItems(3).Text
                                    itmX.SubItems.Add(MyRewardsTransactions.SelectedItems(0).Text)
                                    MyWalletTransactionsUTXOs.Items.Add(itmX)
                                Next
                                MyTransactionItem.SubItems(6).Text = MyCardanoTX.TX_Type
                                MyTransactionItem.SubItems(7).Text = MyCardanoTX.TX_Address_To
                                MyTransactionItem.SubItems(8).Text = MyCardanoTX.TX_Address_From
                                MyTransactionItem.SubItems(9).Text = MyCardanoTX.ADA_Amount
                                MyTransactionItem.SubItems(10).Text = MyCardanoTX.ADA_TotalValue
                                MyTransactionItem.SubItems(11).Text = MyCardanoTX.CNTAsset.policy_id
                                MyTransactionItem.SubItems(12).Text = MyCardanoTX.CNTAsset.asset_name
                                MyTransactionItem.SubItems(13).Text = MyCardanoTX.CNTAsset.fingerprint
                                MyTransactionItem.SubItems(14).Text = MyCardanoTX.CNTAsset.initial_mint_tx_hash
                                MyTransactionItem.SubItems(15).Text = MyCardanoTX.CNTAsset.onchain_metadata.name
                                MyTransactionItem.SubItems(16).Text = MyCardanoTX.CNTAsset.onchain_metadata.image
                                MyTransactionItem.SubItems(17).Text = MyCardanoTX.CNTAsset.metadata.name
                                MyTransactionItem.SubItems(18).Text = MyCardanoTX.CNTAsset.metadata.description
                                MyTransactionItem.SubItems(19).Text = MyCardanoTX.CNTAsset.metadata.ticker
                                MyTransactionItem.SubItems(20).Text = MyCardanoTX.CNTAsset.metadata.url
                                MyTransactionItem.SubItems(21).Text = MyCardanoTX.CNTAsset.metadata.logo
                            Next
                            MyRewardsTransactions.Refresh()
                            If MyCardanoTX.CNTAsset.asset_name > "" Then
                                Try
                                    Dim sIPFSLink As String = ""
                                    sIPFSLink = MyCardanoTX.CNTAsset.onchain_metadata.image
                                    If sIPFSLink = "" Then sIPFSLink = MyCardanoTX.CNTAsset.metadata.url

                                    If sIPFSLink > "" Then
                                        PictureBoxAssetLogo.Tag = sIPFSLink
                                        DrawAssetDetailsIPFS(sIPFSLink, PictureBoxAssetLogo)
                                    Else
                                        PictureBoxAssetLogo.Tag = ""
                                        PictureBoxAssetLogo.Visible = False
                                        PictureBoxAssetLogo.Image = Nothing
                                    End If
                                Catch ex As Exception
                                End Try

                                txtAssetID.Text = .SelectedItems(0).Text
                            Else
                                PictureBoxAssetLogo.Tag = ""
                                PictureBoxAssetLogo.Visible = False
                                PictureBoxAssetLogo.Image = Nothing
                            End If
                        End If
                    End If
                End If
            End With
            Call DoColourTX()
            Call GetBalance()
        Catch ex As Exception
            DoLog(3, ex.Message)
        End Try
        DoLog(2, "")
    End Sub
    Private Sub btnNFTGallery_Click(sender As Object, e As EventArgs) Handles btnNFTGallery.Click
        frmNFTGallery.Show()
        frmNFTGallery.WindowState = FormWindowState.Maximized
    End Sub
    Private Sub btnIPFS_Click(sender As Object, e As EventArgs) Handles btnIPFS.Click
        frmIPFS.Show()
    End Sub
    Private Sub btnCLI_Click(sender As Object, e As EventArgs) Handles btnCLI.Click
        frmCardanoCLI.Show()
        frmCardanoCLI.WindowState = FormWindowState.Maximized
    End Sub
    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        '    lblPolicyIPFS.Tag = lblPolicyIPFS.Tag + 1
        '    lblPolicyIPFS.Text = "Getting Image from IPFS.. " & lblPolicyIPFS.Tag
        '    Application.DoEvents()
    End Sub
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Timer1.Enabled = False
        'btnTrackRewards.PerformClick()
        'btnStakePools.PerformClick()
        'btnAssets.PerformClick()
        Call btnCLI.PerformClick()
    End Sub
    Private Sub btnAbout_Click(sender As Object, e As EventArgs) Handles btnAbout.Click
        frmAbout.Show()
    End Sub
    Function LovelaceToADA(iAmount As Long) As Double
        Dim dReturn As Double = 0.00
        Try
            If iAmount > 100000 Then dReturn = iAmount / 1000000
        Catch ex As Exception
            Call DoLog(2, "ERROR Converting Lovelace to ADA - " & ex.Message)
        End Try
        Return dReturn
    End Function
    Public Function ADAToLovelace(iAmount As Double) As Long
        Dim lReturn As Long = 0
        Try
            If iAmount > 0.1 Then lReturn = iAmount * 1000000
        Catch ex As Exception
            Call DoLog(2, "ERROR Converting ADA to Lovelace - " & ex.Message)
        End Try
        Return lReturn
    End Function
    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Dim sLink As String = txtAssetID.Tag
        If sLink > "" Then
            sLink = "https://pool.pm/" & sLink
            System.Diagnostics.Process.Start(sLink)
        End If
    End Sub
    Private Sub lblTrackRewardsAddress_Click(sender As Object, e As EventArgs) Handles lblTrackRewardsAddress.Click
        'txtCardanoWalletAddress.Text = "addr1qyymvhgw4gfplatesew30y32ztt674zhxv7x7vfwa9dyvu584wjveyhlc0r303zty73eeaswpjguvryrsrkj9lzxepssl9dxxk"
        Dim sTemp As String = txtCardanoWalletAddress.Text
        txtCardanoWalletAddress.Text = txtCardanoWalletAddress.Tag
        txtCardanoWalletAddress.Tag = sTemp
        txtAssetID.Text = "0fe7b9c1abbf139414d8e138721a85dd8d6e24ee7dc0d895587b4f576a633030303133363239"
        'RadioButtonMAINNET.Checked = True
    End Sub
    Private Sub btnMetadata_Click(sender As Object, e As EventArgs) Handles btnMetadata.Click
        Call HidePanels()
        PanelMetadata.Visible = True
        PanelRightImage.Visible = False
        If lstMetadata.Items.Count = 0 Then GetMetadataInformation()
        Call GetBalance()
    End Sub
    Function GetMetadataInformation() As Integer
        Dim iRet As Integer
        Try
            Call DoLog(2, "Getting Transaction Metadata Information ..")
            Call DoLog(3, "")

            lstMetadata.Items.Clear()
            lstMetadataLabelData.Items.Clear()

            'TRUE uses Mainnet FALSE uses Testnet
            Dim BlockfrostService = New clsBlockfrost(RadioButtonMAINNET.Checked)
            Dim MyMetaDataLabelsResponse As MetaDataLabelsResponse = BlockfrostService.GetMetadataLabels()
            For Each item In MyMetaDataLabelsResponse.labels

                Dim itmX As New ListViewItem(item.label)
                itmX.Text = item.label
                itmX.SubItems.Add(item.count)
                itmX.SubItems.Add(item.cip10)
                lstMetadata.Items.Add(itmX)
            Next
            Call DoLog(2, "Ready")
            Call DoLog(2, "Get Metadata Labels - Complete")
        Catch ex As Exception
            Call DoLog(2, "ERROR Get Metadata Labels - " & ex.Message)
        End Try

        Return iRet
    End Function
    Private Sub lstMetadata_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstMetadata.SelectedIndexChanged
        Try
            With lstMetadata
                lstMetadataLabelData.Items.Clear()
                If .SelectedItems.Count > 0 Then
                    Dim sID As String = .SelectedItems(0).Text
                    Call GetMetaDataLabel(sID)
                End If
            End With
            Call GetBalance()
        Catch ex As Exception
            Call DoLog(2, "ERROR Get Metadata Label Data - " & ex.Message)
        End Try
    End Sub
    Private Sub lstMetadataLabelData_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstMetadataLabelData.SelectedIndexChanged
        Try
            With lstMetadataLabelData
                If .SelectedItems.Count > 0 Then
                    Dim itmX As ListViewItem = .SelectedItems(0)

                    Dim sJSON As String = itmX.SubItems(1).Text
                    txtMetaData.Text = sJSON
                    PanelRightImage.Visible = False
                    PanelMetadataJSONTop.Visible = True
                    PanelMetaDataJSON.Width = 300
                    PanelMetaDataJSON.Visible = True
                    Dim sIPFSLink As String = ""
                    Dim i As Integer = InStr(sJSON, "image"": ""ipfs:")
                    If i > 0 Then
                        Dim sTemp As String = Mid(sJSON, i + 9)
                        i = InStr(sTemp, ",")
                        If i > 0 Then
                            sTemp = Mid(sTemp, 1, i - 1)
                            sIPFSLink = Replace(sTemp, Chr(34), "")
                            sIPFSLink = Replace(sIPFSLink, vbNewLine, "")
                            sIPFSLink = Replace(sIPFSLink, vbCr, "")
                            sIPFSLink = Replace(sIPFSLink, vbLf, "")
                            sIPFSLink = Replace(sIPFSLink, "'", "")
                        End If
                    End If

                    If sIPFSLink > "" Then
                        PictureBoxAssetIPFSMetadata.Tag = sIPFSLink
                        DrawAssetDetailsIPFS(sIPFSLink, PictureBoxAssetIPFSMetadata)
                    Else
                        PictureBoxAssetIPFSMetadata.Tag = ""
                        PictureBoxAssetIPFSMetadata.Visible = False
                        PictureBoxAssetIPFSMetadata.Image = Nothing
                    End If
                End If
            End With
            Call GetBalance()
        Catch ex As Exception
            DoLog(3, ex.Message)
        End Try
    End Sub
    Private Sub btnTop100_Click(sender As Object, e As EventArgs) Handles btnTop100.Click
        Call GetMetadataInformation()
    End Sub
    Private Sub btnGetNFTMetadata_Click(sender As Object, e As EventArgs) Handles btnGetNFTMetadata.Click
        Call GetMetaDataLabel("721")
    End Sub
    Sub GetMetaDataLabel(sLabelID As String)
        Try
            With lstMetadata
                PanelMetaDataJSON.Visible = False
                lstMetadataLabelData.Items.Clear()
                'TRUE uses Mainnet FALSE uses Testnet
                Dim BlockfrostService = New clsBlockfrost(RadioButtonMAINNET.Checked)
                Dim MyMetaDataLabelDataResponse As MetaDataLabelDataResponse = BlockfrostService.GetMetadataLabelData(sLabelID)
                For Each item In MyMetaDataLabelDataResponse.labels
                    Dim itmX As New ListViewItem(item.tx_hash)
                    itmX.Text = item.tx_hash
                    itmX.SubItems.Add(item.json_metadata)
                    lstMetadataLabelData.Items.Add(itmX)
                Next
            End With
        Catch ex As Exception
            Call DoLog(2, "ERROR Get Metadata Label Data - " & ex.Message)
        End Try
    End Sub
    Private Sub btnViewTx_Click(sender As Object, e As EventArgs) Handles btnViewTx.Click
        Try
            With lstMetadataLabelData
                lstWalletTransactions.Items.Clear()
                lstWalletTransactionsUTXOs.Items.Clear()
                If .SelectedItems.Count > 0 Then
                    Call btnTrackRewards.PerformClick()
                    Dim itmX As ListViewItem = .SelectedItems(0)
                    Dim itmX2 As New ListViewItem
                    Dim sTX_Hash As String = itmX.Text

                    lstRewardsTransactions.Items.Clear()
                    itmX2.Text = sTX_Hash

                    itmX2.SubItems.Add("") 'block
                    itmX2.SubItems.Add("") 'fees
                    itmX2.SubItems.Add("")
                    itmX2.SubItems.Add("")
                    itmX2.SubItems.Add("")
                    itmX2.SubItems.Add("")
                    itmX2.SubItems.Add("")
                    lstRewardsTransactions.Items.Add(itmX2)
                    Application.DoEvents()
                    'select the first one so the transactions are retrieved
                    If lstRewardsTransactions.Items.Count > 0 Then lstRewardsTransactions.Items(0).Selected = True
                End If
            End With
        Catch ex As Exception
            DoLog(3, ex.Message)
        End Try
    End Sub
    Function GetTransactionMetaData(sTX_Hash As String) As String
        Dim sRet As String = ""
        Try
            'TRUE uses Mainnet FALSE uses Testnet
            Dim BlockfrostService = New clsBlockfrost(RadioButtonMAINNET.Checked)
            sRet = BlockfrostService.GetTransactionMetadata(sTX_Hash)
            txtMetaDataMain.Text = ""
            txtMetaDataMain.Text = JToken.Parse(sRet).ToString(Formatting.Indented)
        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try
        Return Replace(sRet, "[]", "NO METADATA FOUND")
    End Function
    Function GetTransactionMetaDataAsset(sJSON As String, sAssetID As String, sAssetID2 As String) As String
        Dim sRet As String = ""
        txtMetaDataMain.Text = Replace(sRet, "[]", "NO METADATA FOUND")
        Try
            Dim sJSONFormatted As String = JToken.Parse(txtMetaDataMain.Tag).ToString(Formatting.Indented)
            Dim i As Integer = InStr(sJSONFormatted, sAssetID)
            Dim sTemp As String = ""
            If i > 0 Then
                sTemp = Mid(sJSONFormatted, i - 1)
                If sAssetID2 > "" Then
                    i = InStr(sTemp, sAssetID2)
                    If i > 0 Then
                        sTemp = Mid(sTemp, 1, i - 13)
                    End If
                End If
            End If
            txtMetaDataMain.Text = sTemp
        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try
        Return Replace(sRet, "[]", "NO METADATA FOUND")
    End Function
    Private Sub cmdCodeConvert_Click(sender As Object, e As EventArgs) Handles cmdCodeConvert.Click
        System.Diagnostics.Process.Start("https://converter.telerik.com/")
    End Sub
    Private Sub cmdAll_Click(sender As Object, e As EventArgs) Handles cmdAll.Click
        Call cmdTrackRewardsGO.PerformClick()

        lstWalletTransactionsUTXOsNFTS.Items.Clear()
        For Each MyTX As ListViewItem In lstRewardsTransactions.Items
            MyTX.Selected = True

            Dim bRECEIVED As Boolean = False
            If MyTX.SubItems(6).Text = "RECEIVED" Then bRECEIVED = True
            'search the NFT list for existing nfts
            For Each MyNFTUTXO As ListViewItem In lstWalletTransactionsUTXOs.Items
                If MyNFTUTXO.Text = "Outputs" Then
                    If MyNFTUTXO.BackColor = Color.LightGreen Then
                        If bRECEIVED = True Then lstWalletTransactionsUTXOsNFTS.Items.Add(MyNFTUTXO.Clone)
                    ElseIf MyNFTUTXO.BackColor = Color.LightPink Then
                        'Stop
                        If bRECEIVED = False Then lstWalletTransactionsUTXOsNFTS.Items.Add(MyNFTUTXO.Clone)
                    End If
                End If
            Next
        Next
    End Sub

    Private Sub lstAssetAddresses_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstAssetAddresses.SelectedIndexChanged
        With lstAssetAddresses
            If .SelectedItems.Count > 0 Then txtCardanoWalletAddress.Text = .SelectedItems(0).Text
        End With
    End Sub

    Private Sub lstAssetHistory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstAssetHistory.SelectedIndexChanged
        With lstAssetHistory
            lstRewardsTransactions.Items.Clear()
            For Each itmx As ListViewItem In lstAssetHistory.Items
                lstRewardsTransactions.Items.Add(itmx.Text)
            Next
        End With
    End Sub

    Private Sub lstAssetTransactions_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstAssetTransactions.SelectedIndexChanged
        With lstAssetTransactions
            lstRewardsTransactions.Items.Clear()
            For Each itmx As ListViewItem In lstAssetTransactions.Items
                Dim itmx2 As New ListViewItem
                itmx2.Text = itmx.Text
                itmx2.SubItems.Add("")
                itmx2.SubItems.Add("")
                itmx2.SubItems.Add(itmx.SubItems(3).Text)
                itmx2.SubItems.Add("")
                itmx2.SubItems.Add("")
                itmx2.SubItems.Add("")
                itmx2.SubItems.Add("")
                itmx2.SubItems.Add("")
                itmx2.SubItems.Add("")
                itmx2.SubItems.Add("")
                itmx2.SubItems.Add("")
                itmx2.SubItems.Add("")
                itmx2.SubItems.Add("")
                itmx2.SubItems.Add("")
                itmx2.SubItems.Add("")
                lstRewardsTransactions.Items.Add(itmx2)
            Next
        End With

    End Sub
    Private Sub cmdDecode_Click(sender As Object, e As EventArgs) Handles cmdDecode.Click
        Dim val As String
        Try
            If lstAssetsOfPolicy.SelectedItems.Count > 0 Then
                val = Replace(lstAssetsOfPolicy.SelectedItems(0).Text, txtPolicyID.Text, "")
                Dim BlockfrostService = New clsBlockfrost(RadioButtonMAINNET.Checked)
                Dim sAssetDec As String = BlockfrostService.DecodeHEXString(val)

                Dim s = BlockfrostService.EncodeHEXString(sAssetDec)
                Dim t = BlockfrostService.DecodeHEXString(s)
                Dim sAssetEnc As String = txtPolicyID.Text & s

                sAssetEnc = txtPolicyID.Text & BlockfrostService.EncodeHEXString("MonsterWarrior0001")

                'we can check to see if the asset has already been minted - if so dont remint !!!!
                If BlockfrostService.CheckAssetExists(sAssetEnc) Then
                    Stop
                    'asset exists - dont remint !!
                Else
                    Stop
                    'OK to mint
                End If
            End If
        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try
    End Sub
End Class

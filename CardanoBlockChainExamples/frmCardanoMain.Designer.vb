﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmCardanoMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCardanoMain))
        Me.PanelTop = New System.Windows.Forms.Panel()
        Me.cmdCodeConvert = New System.Windows.Forms.Button()
        Me.btnMetadata = New System.Windows.Forms.Button()
        Me.txtUTCOffset = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.RadioButtonTESTNET = New System.Windows.Forms.RadioButton()
        Me.RadioButtonMAINNET = New System.Windows.Forms.RadioButton()
        Me.btnAbout = New System.Windows.Forms.Button()
        Me.btnCLI = New System.Windows.Forms.Button()
        Me.btnIPFS = New System.Windows.Forms.Button()
        Me.btnNFTGallery = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblAPIBalance = New System.Windows.Forms.Label()
        Me.btnAssets = New System.Windows.Forms.Button()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.btnStakePools = New System.Windows.Forms.Button()
        Me.btnTrackRewards = New System.Windows.Forms.Button()
        Me.PanelStakePools = New System.Windows.Forms.Panel()
        Me.lstStakePoolsRetiring = New System.Windows.Forms.ListView()
        Me.lstStakePoolsRetired = New System.Windows.Forms.ListView()
        Me.lstStakePoolsActive = New System.Windows.Forms.ListView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.RadioStakePoolRetiring = New System.Windows.Forms.RadioButton()
        Me.RadioStakePoolRetired = New System.Windows.Forms.RadioButton()
        Me.RadioStakePoolActive = New System.Windows.Forms.RadioButton()
        Me.btnStakePoolsRetreive = New System.Windows.Forms.Button()
        Me.NumericUpDownMaxPools = New System.Windows.Forms.NumericUpDown()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblTotalStakePools = New System.Windows.Forms.Label()
        Me.PanelTrackRewards = New System.Windows.Forms.Panel()
        Me.cmdAll = New System.Windows.Forms.Button()
        Me.lstAssetsLookup = New System.Windows.Forms.ListView()
        Me.PanelAddressDetails = New System.Windows.Forms.Panel()
        Me.lblCardanoAddressStakeAddress = New System.Windows.Forms.Label()
        Me.lblCardanoAddressScript = New System.Windows.Forms.Label()
        Me.lblCardanoAddressType = New System.Windows.Forms.Label()
        Me.lblCardanoAddressAddress = New System.Windows.Forms.Label()
        Me.lstCardanoAddress = New System.Windows.Forms.ListView()
        Me.lstWalletTransactionsUTXOs = New System.Windows.Forms.ListView()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lstWalletTransactions = New System.Windows.Forms.ListView()
        Me.lstRewardsTransactions = New System.Windows.Forms.ListView()
        Me.lblTotalTransactions = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmdTrackRewardsGO = New System.Windows.Forms.Button()
        Me.lblTrackRewardsAddress = New System.Windows.Forms.Label()
        Me.txtCardanoWalletAddress = New System.Windows.Forms.TextBox()
        Me.lstWalletTransactionsUTXOsNFTS = New System.Windows.Forms.ListView()
        Me.PanelBottom = New System.Windows.Forms.Panel()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel3 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.PanelAssets = New System.Windows.Forms.Panel()
        Me.lstAssets = New System.Windows.Forms.ListView()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.btnAssetsIndividualRetreive = New System.Windows.Forms.Button()
        Me.txtAssetID = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.chkAssetImage = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnAssetsLatestRetreive = New System.Windows.Forms.Button()
        Me.NumericUpDownMaxAssets = New System.Windows.Forms.NumericUpDown()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblTotalAssets = New System.Windows.Forms.Label()
        Me.PanelAssetsPolicy = New System.Windows.Forms.Panel()
        Me.lstAssetTransactions = New System.Windows.Forms.ListView()
        Me.lstAssetHistory = New System.Windows.Forms.ListView()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.lstAssetAddresses = New System.Windows.Forms.ListView()
        Me.lblAssetLocation = New System.Windows.Forms.Label()
        Me.txtPolicyID = New System.Windows.Forms.TextBox()
        Me.lblTotalAssetsOfPolicy = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btnGetOtherPolicyAssets = New System.Windows.Forms.Button()
        Me.lstAssetsOfPolicy = New System.Windows.Forms.ListView()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblPolicyLogo = New System.Windows.Forms.Label()
        Me.lblPolicyIPFS = New System.Windows.Forms.Label()
        Me.PanelRightImage = New System.Windows.Forms.Panel()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.PictureBoxAssetIPFS = New System.Windows.Forms.PictureBox()
        Me.PictureBoxAssetLogo = New System.Windows.Forms.PictureBox()
        Me.txtMetaDataMain = New System.Windows.Forms.TextBox()
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.PanelMetadata = New System.Windows.Forms.Panel()
        Me.PanelMetadataMain = New System.Windows.Forms.Panel()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.lstMetadata = New System.Windows.Forms.ListView()
        Me.btnTop100 = New System.Windows.Forms.Button()
        Me.lstMetadataLabelData = New System.Windows.Forms.ListView()
        Me.btnGetNFTMetadata = New System.Windows.Forms.Button()
        Me.PanelMetaDataJSON = New System.Windows.Forms.Panel()
        Me.txtMetaData = New System.Windows.Forms.TextBox()
        Me.PictureBoxAssetIPFSMetadata = New System.Windows.Forms.PictureBox()
        Me.PanelMetadataJSONTop = New System.Windows.Forms.Panel()
        Me.btnViewTx = New System.Windows.Forms.Button()
        Me.cmdDecode = New System.Windows.Forms.Button()
        Me.PanelTop.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelStakePools.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.NumericUpDownMaxPools, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelTrackRewards.SuspendLayout()
        Me.PanelAddressDetails.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.PanelAssets.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.NumericUpDownMaxAssets, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelAssetsPolicy.SuspendLayout()
        Me.PanelRightImage.SuspendLayout()
        CType(Me.PictureBoxAssetIPFS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxAssetLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelMetadata.SuspendLayout()
        Me.PanelMetadataMain.SuspendLayout()
        Me.PanelMetaDataJSON.SuspendLayout()
        CType(Me.PictureBoxAssetIPFSMetadata, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelMetadataJSONTop.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelTop
        '
        Me.PanelTop.BackColor = System.Drawing.Color.FromArgb(CType(CType(63, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(119, Byte), Integer))
        Me.PanelTop.Controls.Add(Me.cmdCodeConvert)
        Me.PanelTop.Controls.Add(Me.btnMetadata)
        Me.PanelTop.Controls.Add(Me.txtUTCOffset)
        Me.PanelTop.Controls.Add(Me.Label11)
        Me.PanelTop.Controls.Add(Me.RadioButtonTESTNET)
        Me.PanelTop.Controls.Add(Me.RadioButtonMAINNET)
        Me.PanelTop.Controls.Add(Me.btnAbout)
        Me.PanelTop.Controls.Add(Me.btnCLI)
        Me.PanelTop.Controls.Add(Me.btnIPFS)
        Me.PanelTop.Controls.Add(Me.btnNFTGallery)
        Me.PanelTop.Controls.Add(Me.Label6)
        Me.PanelTop.Controls.Add(Me.lblAPIBalance)
        Me.PanelTop.Controls.Add(Me.btnAssets)
        Me.PanelTop.Controls.Add(Me.PictureBox2)
        Me.PanelTop.Controls.Add(Me.PictureBox1)
        Me.PanelTop.Controls.Add(Me.btnStakePools)
        Me.PanelTop.Controls.Add(Me.btnTrackRewards)
        Me.PanelTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelTop.Location = New System.Drawing.Point(0, 0)
        Me.PanelTop.Name = "PanelTop"
        Me.PanelTop.Size = New System.Drawing.Size(1601, 74)
        Me.PanelTop.TabIndex = 0
        '
        'cmdCodeConvert
        '
        Me.cmdCodeConvert.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdCodeConvert.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.cmdCodeConvert.Location = New System.Drawing.Point(907, 12)
        Me.cmdCodeConvert.Name = "cmdCodeConvert"
        Me.cmdCodeConvert.Size = New System.Drawing.Size(108, 23)
        Me.cmdCodeConvert.TabIndex = 19
        Me.cmdCodeConvert.Text = "VB to C#"
        Me.cmdCodeConvert.UseVisualStyleBackColor = True
        '
        'btnMetadata
        '
        Me.btnMetadata.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMetadata.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.btnMetadata.Location = New System.Drawing.Point(251, 43)
        Me.btnMetadata.Name = "btnMetadata"
        Me.btnMetadata.Size = New System.Drawing.Size(104, 23)
        Me.btnMetadata.TabIndex = 18
        Me.btnMetadata.Text = "Metadata"
        Me.btnMetadata.UseVisualStyleBackColor = True
        '
        'txtUTCOffset
        '
        Me.txtUTCOffset.Location = New System.Drawing.Point(961, 50)
        Me.txtUTCOffset.Name = "txtUTCOffset"
        Me.txtUTCOffset.Size = New System.Drawing.Size(54, 20)
        Me.txtUTCOffset.TabIndex = 16
        Me.txtUTCOffset.Text = "0"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(635, 54)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(320, 12)
        Me.Label11.TabIndex = 17
        Me.Label11.Text = "UTC Offset (use -18000 for 5 hrs from GMT whihci is Easterm Standard Time)"
        '
        'RadioButtonTESTNET
        '
        Me.RadioButtonTESTNET.AutoSize = True
        Me.RadioButtonTESTNET.ForeColor = System.Drawing.Color.Salmon
        Me.RadioButtonTESTNET.Location = New System.Drawing.Point(637, 32)
        Me.RadioButtonTESTNET.Name = "RadioButtonTESTNET"
        Me.RadioButtonTESTNET.Size = New System.Drawing.Size(75, 17)
        Me.RadioButtonTESTNET.TabIndex = 15
        Me.RadioButtonTESTNET.TabStop = True
        Me.RadioButtonTESTNET.Text = "TESTNET"
        Me.RadioButtonTESTNET.UseVisualStyleBackColor = True
        '
        'RadioButtonMAINNET
        '
        Me.RadioButtonMAINNET.AutoSize = True
        Me.RadioButtonMAINNET.ForeColor = System.Drawing.Color.GreenYellow
        Me.RadioButtonMAINNET.Location = New System.Drawing.Point(638, 9)
        Me.RadioButtonMAINNET.Name = "RadioButtonMAINNET"
        Me.RadioButtonMAINNET.Size = New System.Drawing.Size(74, 17)
        Me.RadioButtonMAINNET.TabIndex = 14
        Me.RadioButtonMAINNET.TabStop = True
        Me.RadioButtonMAINNET.Text = "MAINNET"
        Me.RadioButtonMAINNET.UseVisualStyleBackColor = True
        '
        'btnAbout
        '
        Me.btnAbout.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAbout.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAbout.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.btnAbout.Location = New System.Drawing.Point(1360, 12)
        Me.btnAbout.Name = "btnAbout"
        Me.btnAbout.Size = New System.Drawing.Size(120, 23)
        Me.btnAbout.TabIndex = 13
        Me.btnAbout.Text = "About Me"
        Me.btnAbout.UseVisualStyleBackColor = True
        '
        'btnCLI
        '
        Me.btnCLI.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCLI.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.btnCLI.Location = New System.Drawing.Point(739, 12)
        Me.btnCLI.Name = "btnCLI"
        Me.btnCLI.Size = New System.Drawing.Size(108, 23)
        Me.btnCLI.TabIndex = 12
        Me.btnCLI.Text = "Cardano CLI"
        Me.btnCLI.UseVisualStyleBackColor = True
        '
        'btnIPFS
        '
        Me.btnIPFS.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnIPFS.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.btnIPFS.Location = New System.Drawing.Point(371, 43)
        Me.btnIPFS.Name = "btnIPFS"
        Me.btnIPFS.Size = New System.Drawing.Size(108, 23)
        Me.btnIPFS.TabIndex = 11
        Me.btnIPFS.Text = "IPFS"
        Me.btnIPFS.UseVisualStyleBackColor = True
        '
        'btnNFTGallery
        '
        Me.btnNFTGallery.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNFTGallery.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.btnNFTGallery.Location = New System.Drawing.Point(488, 43)
        Me.btnNFTGallery.Name = "btnNFTGallery"
        Me.btnNFTGallery.Size = New System.Drawing.Size(120, 23)
        Me.btnNFTGallery.TabIndex = 10
        Me.btnNFTGallery.Text = "NFT Gallery"
        Me.btnNFTGallery.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(1387, 54)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(66, 13)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "API Balance"
        '
        'lblAPIBalance
        '
        Me.lblAPIBalance.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblAPIBalance.AutoSize = True
        Me.lblAPIBalance.ForeColor = System.Drawing.Color.White
        Me.lblAPIBalance.Location = New System.Drawing.Point(1467, 54)
        Me.lblAPIBalance.Name = "lblAPIBalance"
        Me.lblAPIBalance.Size = New System.Drawing.Size(13, 13)
        Me.lblAPIBalance.TabIndex = 8
        Me.lblAPIBalance.Text = "?"
        '
        'btnAssets
        '
        Me.btnAssets.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAssets.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.btnAssets.Location = New System.Drawing.Point(371, 12)
        Me.btnAssets.Name = "btnAssets"
        Me.btnAssets.Size = New System.Drawing.Size(104, 23)
        Me.btnAssets.TabIndex = 4
        Me.btnAssets.Text = "Assets"
        Me.btnAssets.UseVisualStyleBackColor = True
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.Image = Global.CardanoBlockChainExamples.My.Resources.Resources.cardano_logo_white
        Me.PictureBox2.Location = New System.Drawing.Point(1486, 1)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(127, 70)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox2.TabIndex = 3
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.CardanoBlockChainExamples.My.Resources.Resources.catalyst_logo
        Me.PictureBox1.Location = New System.Drawing.Point(-6, -50)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 179)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'btnStakePools
        '
        Me.btnStakePools.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnStakePools.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.btnStakePools.Location = New System.Drawing.Point(251, 12)
        Me.btnStakePools.Name = "btnStakePools"
        Me.btnStakePools.Size = New System.Drawing.Size(104, 23)
        Me.btnStakePools.TabIndex = 1
        Me.btnStakePools.Text = "Stake Pools"
        Me.btnStakePools.UseVisualStyleBackColor = True
        '
        'btnTrackRewards
        '
        Me.btnTrackRewards.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTrackRewards.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.btnTrackRewards.Location = New System.Drawing.Point(488, 12)
        Me.btnTrackRewards.Name = "btnTrackRewards"
        Me.btnTrackRewards.Size = New System.Drawing.Size(120, 23)
        Me.btnTrackRewards.TabIndex = 0
        Me.btnTrackRewards.Text = "Address Transactions"
        Me.btnTrackRewards.UseVisualStyleBackColor = True
        '
        'PanelStakePools
        '
        Me.PanelStakePools.Controls.Add(Me.lstStakePoolsRetiring)
        Me.PanelStakePools.Controls.Add(Me.lstStakePoolsRetired)
        Me.PanelStakePools.Controls.Add(Me.lstStakePoolsActive)
        Me.PanelStakePools.Controls.Add(Me.Panel1)
        Me.PanelStakePools.Location = New System.Drawing.Point(12, 414)
        Me.PanelStakePools.Name = "PanelStakePools"
        Me.PanelStakePools.Size = New System.Drawing.Size(462, 112)
        Me.PanelStakePools.TabIndex = 1
        '
        'lstStakePoolsRetiring
        '
        Me.lstStakePoolsRetiring.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.lstStakePoolsRetiring.FullRowSelect = True
        Me.lstStakePoolsRetiring.HideSelection = False
        Me.lstStakePoolsRetiring.Location = New System.Drawing.Point(173, 72)
        Me.lstStakePoolsRetiring.MultiSelect = False
        Me.lstStakePoolsRetiring.Name = "lstStakePoolsRetiring"
        Me.lstStakePoolsRetiring.Size = New System.Drawing.Size(58, 32)
        Me.lstStakePoolsRetiring.TabIndex = 9
        Me.lstStakePoolsRetiring.UseCompatibleStateImageBehavior = False
        Me.lstStakePoolsRetiring.Visible = False
        '
        'lstStakePoolsRetired
        '
        Me.lstStakePoolsRetired.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.lstStakePoolsRetired.FullRowSelect = True
        Me.lstStakePoolsRetired.HideSelection = False
        Me.lstStakePoolsRetired.Location = New System.Drawing.Point(87, 72)
        Me.lstStakePoolsRetired.MultiSelect = False
        Me.lstStakePoolsRetired.Name = "lstStakePoolsRetired"
        Me.lstStakePoolsRetired.Size = New System.Drawing.Size(58, 32)
        Me.lstStakePoolsRetired.TabIndex = 9
        Me.lstStakePoolsRetired.UseCompatibleStateImageBehavior = False
        Me.lstStakePoolsRetired.Visible = False
        '
        'lstStakePoolsActive
        '
        Me.lstStakePoolsActive.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.lstStakePoolsActive.FullRowSelect = True
        Me.lstStakePoolsActive.HideSelection = False
        Me.lstStakePoolsActive.Location = New System.Drawing.Point(10, 72)
        Me.lstStakePoolsActive.MultiSelect = False
        Me.lstStakePoolsActive.Name = "lstStakePoolsActive"
        Me.lstStakePoolsActive.Size = New System.Drawing.Size(58, 32)
        Me.lstStakePoolsActive.TabIndex = 6
        Me.lstStakePoolsActive.UseCompatibleStateImageBehavior = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.RadioStakePoolRetiring)
        Me.Panel1.Controls.Add(Me.RadioStakePoolRetired)
        Me.Panel1.Controls.Add(Me.RadioStakePoolActive)
        Me.Panel1.Controls.Add(Me.btnStakePoolsRetreive)
        Me.Panel1.Controls.Add(Me.NumericUpDownMaxPools)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.lblTotalStakePools)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(462, 61)
        Me.Panel1.TabIndex = 9
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 8)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(133, 13)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Max Pools to be Returned:"
        '
        'RadioStakePoolRetiring
        '
        Me.RadioStakePoolRetiring.AutoSize = True
        Me.RadioStakePoolRetiring.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.RadioStakePoolRetiring.Location = New System.Drawing.Point(233, 6)
        Me.RadioStakePoolRetiring.Name = "RadioStakePoolRetiring"
        Me.RadioStakePoolRetiring.Size = New System.Drawing.Size(61, 17)
        Me.RadioStakePoolRetiring.TabIndex = 13
        Me.RadioStakePoolRetiring.Text = "Retiring"
        Me.RadioStakePoolRetiring.UseVisualStyleBackColor = True
        '
        'RadioStakePoolRetired
        '
        Me.RadioStakePoolRetired.AutoSize = True
        Me.RadioStakePoolRetired.ForeColor = System.Drawing.Color.Red
        Me.RadioStakePoolRetired.Location = New System.Drawing.Point(300, 6)
        Me.RadioStakePoolRetired.Name = "RadioStakePoolRetired"
        Me.RadioStakePoolRetired.Size = New System.Drawing.Size(59, 17)
        Me.RadioStakePoolRetired.TabIndex = 12
        Me.RadioStakePoolRetired.Text = "Retired"
        Me.RadioStakePoolRetired.UseVisualStyleBackColor = True
        '
        'RadioStakePoolActive
        '
        Me.RadioStakePoolActive.AutoSize = True
        Me.RadioStakePoolActive.Checked = True
        Me.RadioStakePoolActive.ForeColor = System.Drawing.Color.Green
        Me.RadioStakePoolActive.Location = New System.Drawing.Point(172, 6)
        Me.RadioStakePoolActive.Name = "RadioStakePoolActive"
        Me.RadioStakePoolActive.Size = New System.Drawing.Size(55, 17)
        Me.RadioStakePoolActive.TabIndex = 11
        Me.RadioStakePoolActive.TabStop = True
        Me.RadioStakePoolActive.Text = "Active"
        Me.RadioStakePoolActive.UseVisualStyleBackColor = True
        '
        'btnStakePoolsRetreive
        '
        Me.btnStakePoolsRetreive.Location = New System.Drawing.Point(373, 3)
        Me.btnStakePoolsRetreive.Name = "btnStakePoolsRetreive"
        Me.btnStakePoolsRetreive.Size = New System.Drawing.Size(73, 23)
        Me.btnStakePoolsRetreive.TabIndex = 10
        Me.btnStakePoolsRetreive.Text = "REFRESH"
        Me.btnStakePoolsRetreive.UseVisualStyleBackColor = True
        '
        'NumericUpDownMaxPools
        '
        Me.NumericUpDownMaxPools.Location = New System.Drawing.Point(9, 31)
        Me.NumericUpDownMaxPools.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.NumericUpDownMaxPools.Name = "NumericUpDownMaxPools"
        Me.NumericUpDownMaxPools.Size = New System.Drawing.Size(49, 20)
        Me.NumericUpDownMaxPools.TabIndex = 9
        Me.NumericUpDownMaxPools.Value = New Decimal(New Integer() {6, 0, 0, 0})
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(265, 35)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(94, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Total Stake Pools:"
        '
        'lblTotalStakePools
        '
        Me.lblTotalStakePools.AutoSize = True
        Me.lblTotalStakePools.Location = New System.Drawing.Point(365, 35)
        Me.lblTotalStakePools.Name = "lblTotalStakePools"
        Me.lblTotalStakePools.Size = New System.Drawing.Size(13, 13)
        Me.lblTotalStakePools.TabIndex = 7
        Me.lblTotalStakePools.Text = "?"
        '
        'PanelTrackRewards
        '
        Me.PanelTrackRewards.Controls.Add(Me.cmdAll)
        Me.PanelTrackRewards.Controls.Add(Me.lstAssetsLookup)
        Me.PanelTrackRewards.Controls.Add(Me.PanelAddressDetails)
        Me.PanelTrackRewards.Controls.Add(Me.lstWalletTransactionsUTXOs)
        Me.PanelTrackRewards.Controls.Add(Me.Label12)
        Me.PanelTrackRewards.Controls.Add(Me.Label8)
        Me.PanelTrackRewards.Controls.Add(Me.lstWalletTransactions)
        Me.PanelTrackRewards.Controls.Add(Me.lstRewardsTransactions)
        Me.PanelTrackRewards.Controls.Add(Me.lblTotalTransactions)
        Me.PanelTrackRewards.Controls.Add(Me.Label1)
        Me.PanelTrackRewards.Controls.Add(Me.cmdTrackRewardsGO)
        Me.PanelTrackRewards.Controls.Add(Me.lblTrackRewardsAddress)
        Me.PanelTrackRewards.Controls.Add(Me.txtCardanoWalletAddress)
        Me.PanelTrackRewards.Controls.Add(Me.lstWalletTransactionsUTXOsNFTS)
        Me.PanelTrackRewards.Location = New System.Drawing.Point(12, 83)
        Me.PanelTrackRewards.Name = "PanelTrackRewards"
        Me.PanelTrackRewards.Size = New System.Drawing.Size(683, 528)
        Me.PanelTrackRewards.TabIndex = 2
        '
        'cmdAll
        '
        Me.cmdAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdAll.Location = New System.Drawing.Point(642, 23)
        Me.cmdAll.Name = "cmdAll"
        Me.cmdAll.Size = New System.Drawing.Size(38, 23)
        Me.cmdAll.TabIndex = 20
        Me.cmdAll.Text = "ALL"
        Me.cmdAll.UseVisualStyleBackColor = True
        '
        'lstAssetsLookup
        '
        Me.lstAssetsLookup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstAssetsLookup.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.lstAssetsLookup.FullRowSelect = True
        Me.lstAssetsLookup.HideSelection = False
        Me.lstAssetsLookup.Location = New System.Drawing.Point(429, 281)
        Me.lstAssetsLookup.MultiSelect = False
        Me.lstAssetsLookup.Name = "lstAssetsLookup"
        Me.lstAssetsLookup.Size = New System.Drawing.Size(242, 119)
        Me.lstAssetsLookup.TabIndex = 19
        Me.lstAssetsLookup.UseCompatibleStateImageBehavior = False
        '
        'PanelAddressDetails
        '
        Me.PanelAddressDetails.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.PanelAddressDetails.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.PanelAddressDetails.Controls.Add(Me.lblCardanoAddressStakeAddress)
        Me.PanelAddressDetails.Controls.Add(Me.lblCardanoAddressScript)
        Me.PanelAddressDetails.Controls.Add(Me.lblCardanoAddressType)
        Me.PanelAddressDetails.Controls.Add(Me.lblCardanoAddressAddress)
        Me.PanelAddressDetails.Controls.Add(Me.lstCardanoAddress)
        Me.PanelAddressDetails.Location = New System.Drawing.Point(13, 231)
        Me.PanelAddressDetails.Name = "PanelAddressDetails"
        Me.PanelAddressDetails.Size = New System.Drawing.Size(410, 169)
        Me.PanelAddressDetails.TabIndex = 11
        '
        'lblCardanoAddressStakeAddress
        '
        Me.lblCardanoAddressStakeAddress.AutoSize = True
        Me.lblCardanoAddressStakeAddress.Location = New System.Drawing.Point(3, 21)
        Me.lblCardanoAddressStakeAddress.Name = "lblCardanoAddressStakeAddress"
        Me.lblCardanoAddressStakeAddress.Size = New System.Drawing.Size(76, 13)
        Me.lblCardanoAddressStakeAddress.TabIndex = 14
        Me.lblCardanoAddressStakeAddress.Text = "Stake Address"
        '
        'lblCardanoAddressScript
        '
        Me.lblCardanoAddressScript.AutoSize = True
        Me.lblCardanoAddressScript.Location = New System.Drawing.Point(3, 37)
        Me.lblCardanoAddressScript.Name = "lblCardanoAddressScript"
        Me.lblCardanoAddressScript.Size = New System.Drawing.Size(45, 13)
        Me.lblCardanoAddressScript.TabIndex = 13
        Me.lblCardanoAddressScript.Text = "Is Script"
        '
        'lblCardanoAddressType
        '
        Me.lblCardanoAddressType.AutoSize = True
        Me.lblCardanoAddressType.Location = New System.Drawing.Point(74, 35)
        Me.lblCardanoAddressType.Name = "lblCardanoAddressType"
        Me.lblCardanoAddressType.Size = New System.Drawing.Size(31, 13)
        Me.lblCardanoAddressType.TabIndex = 12
        Me.lblCardanoAddressType.Text = "Type"
        '
        'lblCardanoAddressAddress
        '
        Me.lblCardanoAddressAddress.AutoSize = True
        Me.lblCardanoAddressAddress.Location = New System.Drawing.Point(3, 5)
        Me.lblCardanoAddressAddress.Name = "lblCardanoAddressAddress"
        Me.lblCardanoAddressAddress.Size = New System.Drawing.Size(88, 13)
        Me.lblCardanoAddressAddress.TabIndex = 11
        Me.lblCardanoAddressAddress.Text = "Cardano Address"
        '
        'lstCardanoAddress
        '
        Me.lstCardanoAddress.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lstCardanoAddress.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lstCardanoAddress.FullRowSelect = True
        Me.lstCardanoAddress.HideSelection = False
        Me.lstCardanoAddress.Location = New System.Drawing.Point(0, 53)
        Me.lstCardanoAddress.MultiSelect = False
        Me.lstCardanoAddress.Name = "lstCardanoAddress"
        Me.lstCardanoAddress.Size = New System.Drawing.Size(410, 116)
        Me.lstCardanoAddress.TabIndex = 10
        Me.lstCardanoAddress.UseCompatibleStateImageBehavior = False
        '
        'lstWalletTransactionsUTXOs
        '
        Me.lstWalletTransactionsUTXOs.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstWalletTransactionsUTXOs.FullRowSelect = True
        Me.lstWalletTransactionsUTXOs.HideSelection = False
        Me.lstWalletTransactionsUTXOs.Location = New System.Drawing.Point(429, 231)
        Me.lstWalletTransactionsUTXOs.MultiSelect = False
        Me.lstWalletTransactionsUTXOs.Name = "lstWalletTransactionsUTXOs"
        Me.lstWalletTransactionsUTXOs.Size = New System.Drawing.Size(242, 44)
        Me.lstWalletTransactionsUTXOs.TabIndex = 9
        Me.lstWalletTransactionsUTXOs.UseCompatibleStateImageBehavior = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(429, 52)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(324, 13)
        Me.Label12.TabIndex = 8
        Me.Label12.Text = "NOTE: The Balance below shows both the Transaction Addresses "
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(206, 52)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(217, 13)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Blockchain Date uses UTC Offset set above"
        '
        'lstWalletTransactions
        '
        Me.lstWalletTransactions.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstWalletTransactions.FullRowSelect = True
        Me.lstWalletTransactions.HideSelection = False
        Me.lstWalletTransactions.Location = New System.Drawing.Point(429, 68)
        Me.lstWalletTransactions.MultiSelect = False
        Me.lstWalletTransactions.Name = "lstWalletTransactions"
        Me.lstWalletTransactions.Size = New System.Drawing.Size(242, 157)
        Me.lstWalletTransactions.TabIndex = 6
        Me.lstWalletTransactions.UseCompatibleStateImageBehavior = False
        '
        'lstRewardsTransactions
        '
        Me.lstRewardsTransactions.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lstRewardsTransactions.FullRowSelect = True
        Me.lstRewardsTransactions.HideSelection = False
        Me.lstRewardsTransactions.Location = New System.Drawing.Point(13, 68)
        Me.lstRewardsTransactions.MultiSelect = False
        Me.lstRewardsTransactions.Name = "lstRewardsTransactions"
        Me.lstRewardsTransactions.Size = New System.Drawing.Size(410, 157)
        Me.lstRewardsTransactions.TabIndex = 5
        Me.lstRewardsTransactions.UseCompatibleStateImageBehavior = False
        '
        'lblTotalTransactions
        '
        Me.lblTotalTransactions.AutoSize = True
        Me.lblTotalTransactions.Location = New System.Drawing.Point(95, 52)
        Me.lblTotalTransactions.Name = "lblTotalTransactions"
        Me.lblTotalTransactions.Size = New System.Drawing.Size(13, 13)
        Me.lblTotalTransactions.TabIndex = 4
        Me.lblTotalTransactions.Text = "?"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 52)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Total:"
        '
        'cmdTrackRewardsGO
        '
        Me.cmdTrackRewardsGO.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdTrackRewardsGO.Location = New System.Drawing.Point(602, 23)
        Me.cmdTrackRewardsGO.Name = "cmdTrackRewardsGO"
        Me.cmdTrackRewardsGO.Size = New System.Drawing.Size(36, 23)
        Me.cmdTrackRewardsGO.TabIndex = 2
        Me.cmdTrackRewardsGO.Text = "GO"
        Me.cmdTrackRewardsGO.UseVisualStyleBackColor = True
        '
        'lblTrackRewardsAddress
        '
        Me.lblTrackRewardsAddress.AutoSize = True
        Me.lblTrackRewardsAddress.Location = New System.Drawing.Point(10, 10)
        Me.lblTrackRewardsAddress.Name = "lblTrackRewardsAddress"
        Me.lblTrackRewardsAddress.Size = New System.Drawing.Size(88, 13)
        Me.lblTrackRewardsAddress.TabIndex = 1
        Me.lblTrackRewardsAddress.Text = "Cardano Address"
        '
        'txtCardanoWalletAddress
        '
        Me.txtCardanoWalletAddress.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCardanoWalletAddress.Location = New System.Drawing.Point(10, 25)
        Me.txtCardanoWalletAddress.Name = "txtCardanoWalletAddress"
        Me.txtCardanoWalletAddress.Size = New System.Drawing.Size(586, 20)
        Me.txtCardanoWalletAddress.TabIndex = 0
        Me.txtCardanoWalletAddress.Text = "addr1qyv3zl392djql2duv978f58mxq3w7uygxnvpduhzed7x69zluwf9efswgrg29rmry6zvpdwgcd8z" &
    "xrdsjeauj6k0czaqemtlmk"
        '
        'lstWalletTransactionsUTXOsNFTS
        '
        Me.lstWalletTransactionsUTXOsNFTS.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lstWalletTransactionsUTXOsNFTS.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lstWalletTransactionsUTXOsNFTS.FullRowSelect = True
        Me.lstWalletTransactionsUTXOsNFTS.HideSelection = False
        Me.lstWalletTransactionsUTXOsNFTS.Location = New System.Drawing.Point(0, 406)
        Me.lstWalletTransactionsUTXOsNFTS.MultiSelect = False
        Me.lstWalletTransactionsUTXOsNFTS.Name = "lstWalletTransactionsUTXOsNFTS"
        Me.lstWalletTransactionsUTXOsNFTS.Size = New System.Drawing.Size(683, 122)
        Me.lstWalletTransactionsUTXOsNFTS.TabIndex = 21
        Me.lstWalletTransactionsUTXOsNFTS.UseCompatibleStateImageBehavior = False
        '
        'PanelBottom
        '
        Me.PanelBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelBottom.Location = New System.Drawing.Point(0, 773)
        Me.PanelBottom.Name = "PanelBottom"
        Me.PanelBottom.Size = New System.Drawing.Size(1262, 15)
        Me.PanelBottom.TabIndex = 3
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.ToolStripStatusLabel2, Me.ToolStripStatusLabel3})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 788)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1262, 22)
        Me.StatusStrip1.TabIndex = 4
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(119, 17)
        Me.ToolStripStatusLabel1.Text = "ToolStripStatusLabel1"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(1009, 17)
        Me.ToolStripStatusLabel2.Spring = True
        Me.ToolStripStatusLabel2.Text = "ToolStripStatusLabel2"
        '
        'ToolStripStatusLabel3
        '
        Me.ToolStripStatusLabel3.Name = "ToolStripStatusLabel3"
        Me.ToolStripStatusLabel3.Size = New System.Drawing.Size(119, 17)
        Me.ToolStripStatusLabel3.Text = "ToolStripStatusLabel3"
        '
        'Timer1
        '
        '
        'PanelAssets
        '
        Me.PanelAssets.Controls.Add(Me.lstAssets)
        Me.PanelAssets.Controls.Add(Me.Panel3)
        Me.PanelAssets.Controls.Add(Me.PanelAssetsPolicy)
        Me.PanelAssets.Location = New System.Drawing.Point(280, 350)
        Me.PanelAssets.Name = "PanelAssets"
        Me.PanelAssets.Size = New System.Drawing.Size(847, 434)
        Me.PanelAssets.TabIndex = 5
        '
        'lstAssets
        '
        Me.lstAssets.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.lstAssets.FullRowSelect = True
        Me.lstAssets.HideSelection = False
        Me.lstAssets.Location = New System.Drawing.Point(119, 276)
        Me.lstAssets.MultiSelect = False
        Me.lstAssets.Name = "lstAssets"
        Me.lstAssets.Size = New System.Drawing.Size(47, 112)
        Me.lstAssets.TabIndex = 6
        Me.lstAssets.UseCompatibleStateImageBehavior = False
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.btnAssetsIndividualRetreive)
        Me.Panel3.Controls.Add(Me.txtAssetID)
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Controls.Add(Me.chkAssetImage)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.btnAssetsLatestRetreive)
        Me.Panel3.Controls.Add(Me.NumericUpDownMaxAssets)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Controls.Add(Me.lblTotalAssets)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel3.Location = New System.Drawing.Point(0, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(377, 150)
        Me.Panel3.TabIndex = 9
        '
        'btnAssetsIndividualRetreive
        '
        Me.btnAssetsIndividualRetreive.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAssetsIndividualRetreive.Location = New System.Drawing.Point(294, 120)
        Me.btnAssetsIndividualRetreive.Name = "btnAssetsIndividualRetreive"
        Me.btnAssetsIndividualRetreive.Size = New System.Drawing.Size(68, 23)
        Me.btnAssetsIndividualRetreive.TabIndex = 19
        Me.btnAssetsIndividualRetreive.Text = "SEARCH"
        Me.btnAssetsIndividualRetreive.UseVisualStyleBackColor = True
        '
        'txtAssetID
        '
        Me.txtAssetID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAssetID.Location = New System.Drawing.Point(9, 94)
        Me.txtAssetID.Name = "txtAssetID"
        Me.txtAssetID.Size = New System.Drawing.Size(353, 20)
        Me.txtAssetID.TabIndex = 18
        Me.txtAssetID.Text = "ac3f4224723e2ed9d166478662f6e48bae9ddf0fc5ee58f54f6c322943454e54"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 77)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(95, 13)
        Me.Label7.TabIndex = 17
        Me.Label7.Text = "Individual Asset ID"
        '
        'chkAssetImage
        '
        Me.chkAssetImage.AutoSize = True
        Me.chkAssetImage.Checked = True
        Me.chkAssetImage.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAssetImage.Location = New System.Drawing.Point(205, 35)
        Me.chkAssetImage.Name = "chkAssetImage"
        Me.chkAssetImage.Size = New System.Drawing.Size(165, 17)
        Me.chkAssetImage.TabIndex = 16
        Me.chkAssetImage.Text = "Just show assets with Images"
        Me.chkAssetImage.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 8)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(142, 13)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "Max Results to be Returned:"
        '
        'btnAssetsLatestRetreive
        '
        Me.btnAssetsLatestRetreive.Location = New System.Drawing.Point(205, 6)
        Me.btnAssetsLatestRetreive.Name = "btnAssetsLatestRetreive"
        Me.btnAssetsLatestRetreive.Size = New System.Drawing.Size(165, 23)
        Me.btnAssetsLatestRetreive.TabIndex = 10
        Me.btnAssetsLatestRetreive.Text = "GET LATEST ASSETS"
        Me.btnAssetsLatestRetreive.UseVisualStyleBackColor = True
        '
        'NumericUpDownMaxAssets
        '
        Me.NumericUpDownMaxAssets.Location = New System.Drawing.Point(150, 6)
        Me.NumericUpDownMaxAssets.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.NumericUpDownMaxAssets.Name = "NumericUpDownMaxAssets"
        Me.NumericUpDownMaxAssets.Size = New System.Drawing.Size(49, 20)
        Me.NumericUpDownMaxAssets.TabIndex = 9
        Me.NumericUpDownMaxAssets.Value = New Decimal(New Integer() {20, 0, 0, 0})
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(3, 130)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(68, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Total Assets:"
        '
        'lblTotalAssets
        '
        Me.lblTotalAssets.AutoSize = True
        Me.lblTotalAssets.Location = New System.Drawing.Point(91, 130)
        Me.lblTotalAssets.Name = "lblTotalAssets"
        Me.lblTotalAssets.Size = New System.Drawing.Size(13, 13)
        Me.lblTotalAssets.TabIndex = 7
        Me.lblTotalAssets.Text = "?"
        '
        'PanelAssetsPolicy
        '
        Me.PanelAssetsPolicy.Controls.Add(Me.cmdDecode)
        Me.PanelAssetsPolicy.Controls.Add(Me.lstAssetTransactions)
        Me.PanelAssetsPolicy.Controls.Add(Me.lstAssetHistory)
        Me.PanelAssetsPolicy.Controls.Add(Me.Label13)
        Me.PanelAssetsPolicy.Controls.Add(Me.lstAssetAddresses)
        Me.PanelAssetsPolicy.Controls.Add(Me.lblAssetLocation)
        Me.PanelAssetsPolicy.Controls.Add(Me.txtPolicyID)
        Me.PanelAssetsPolicy.Controls.Add(Me.lblTotalAssetsOfPolicy)
        Me.PanelAssetsPolicy.Controls.Add(Me.Label10)
        Me.PanelAssetsPolicy.Controls.Add(Me.btnGetOtherPolicyAssets)
        Me.PanelAssetsPolicy.Controls.Add(Me.lstAssetsOfPolicy)
        Me.PanelAssetsPolicy.Controls.Add(Me.Label9)
        Me.PanelAssetsPolicy.Dock = System.Windows.Forms.DockStyle.Right
        Me.PanelAssetsPolicy.Location = New System.Drawing.Point(377, 0)
        Me.PanelAssetsPolicy.Name = "PanelAssetsPolicy"
        Me.PanelAssetsPolicy.Size = New System.Drawing.Size(470, 434)
        Me.PanelAssetsPolicy.TabIndex = 18
        '
        'lstAssetTransactions
        '
        Me.lstAssetTransactions.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lstAssetTransactions.FullRowSelect = True
        Me.lstAssetTransactions.HideSelection = False
        Me.lstAssetTransactions.Location = New System.Drawing.Point(3, 344)
        Me.lstAssetTransactions.MultiSelect = False
        Me.lstAssetTransactions.Name = "lstAssetTransactions"
        Me.lstAssetTransactions.Size = New System.Drawing.Size(453, 87)
        Me.lstAssetTransactions.TabIndex = 31
        Me.lstAssetTransactions.UseCompatibleStateImageBehavior = False
        Me.lstAssetTransactions.Visible = False
        '
        'lstAssetHistory
        '
        Me.lstAssetHistory.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lstAssetHistory.FullRowSelect = True
        Me.lstAssetHistory.HideSelection = False
        Me.lstAssetHistory.Location = New System.Drawing.Point(0, 264)
        Me.lstAssetHistory.MultiSelect = False
        Me.lstAssetHistory.Name = "lstAssetHistory"
        Me.lstAssetHistory.Size = New System.Drawing.Size(453, 74)
        Me.lstAssetHistory.TabIndex = 29
        Me.lstAssetHistory.UseCompatibleStateImageBehavior = False
        Me.lstAssetHistory.Visible = False
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(0, 248)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(145, 13)
        Me.Label13.TabIndex = 28
        Me.Label13.Text = "Assets History / Transactions"
        '
        'lstAssetAddresses
        '
        Me.lstAssetAddresses.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lstAssetAddresses.FullRowSelect = True
        Me.lstAssetAddresses.HideSelection = False
        Me.lstAssetAddresses.Location = New System.Drawing.Point(0, 167)
        Me.lstAssetAddresses.MultiSelect = False
        Me.lstAssetAddresses.Name = "lstAssetAddresses"
        Me.lstAssetAddresses.Size = New System.Drawing.Size(453, 75)
        Me.lstAssetAddresses.TabIndex = 27
        Me.lstAssetAddresses.UseCompatibleStateImageBehavior = False
        Me.lstAssetAddresses.Visible = False
        '
        'lblAssetLocation
        '
        Me.lblAssetLocation.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblAssetLocation.AutoSize = True
        Me.lblAssetLocation.Location = New System.Drawing.Point(0, 152)
        Me.lblAssetLocation.Name = "lblAssetLocation"
        Me.lblAssetLocation.Size = New System.Drawing.Size(85, 13)
        Me.lblAssetLocation.TabIndex = 26
        Me.lblAssetLocation.Text = "Assets Location:"
        '
        'txtPolicyID
        '
        Me.txtPolicyID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPolicyID.Location = New System.Drawing.Point(9, 23)
        Me.txtPolicyID.Name = "txtPolicyID"
        Me.txtPolicyID.Size = New System.Drawing.Size(404, 20)
        Me.txtPolicyID.TabIndex = 25
        Me.txtPolicyID.Text = "?"
        '
        'lblTotalAssetsOfPolicy
        '
        Me.lblTotalAssetsOfPolicy.AutoSize = True
        Me.lblTotalAssetsOfPolicy.Location = New System.Drawing.Point(130, 79)
        Me.lblTotalAssetsOfPolicy.Name = "lblTotalAssetsOfPolicy"
        Me.lblTotalAssetsOfPolicy.Size = New System.Drawing.Size(13, 13)
        Me.lblTotalAssetsOfPolicy.TabIndex = 24
        Me.lblTotalAssetsOfPolicy.Text = "?"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(11, 79)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(99, 13)
        Me.Label10.TabIndex = 23
        Me.Label10.Text = "Total Policy Assets:"
        '
        'btnGetOtherPolicyAssets
        '
        Me.btnGetOtherPolicyAssets.Location = New System.Drawing.Point(9, 46)
        Me.btnGetOtherPolicyAssets.Name = "btnGetOtherPolicyAssets"
        Me.btnGetOtherPolicyAssets.Size = New System.Drawing.Size(172, 23)
        Me.btnGetOtherPolicyAssets.TabIndex = 22
        Me.btnGetOtherPolicyAssets.Text = "Get Other Assets for this Policy"
        Me.btnGetOtherPolicyAssets.UseVisualStyleBackColor = True
        '
        'lstAssetsOfPolicy
        '
        Me.lstAssetsOfPolicy.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstAssetsOfPolicy.FullRowSelect = True
        Me.lstAssetsOfPolicy.HideSelection = False
        Me.lstAssetsOfPolicy.Location = New System.Drawing.Point(3, 95)
        Me.lstAssetsOfPolicy.MultiSelect = False
        Me.lstAssetsOfPolicy.Name = "lstAssetsOfPolicy"
        Me.lstAssetsOfPolicy.Size = New System.Drawing.Size(455, 55)
        Me.lstAssetsOfPolicy.TabIndex = 9
        Me.lstAssetsOfPolicy.UseCompatibleStateImageBehavior = False
        Me.lstAssetsOfPolicy.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(6, 8)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(49, 13)
        Me.Label9.TabIndex = 20
        Me.Label9.Text = "Policy ID"
        '
        'lblPolicyLogo
        '
        Me.lblPolicyLogo.AutoSize = True
        Me.lblPolicyLogo.Location = New System.Drawing.Point(191, 337)
        Me.lblPolicyLogo.Name = "lblPolicyLogo"
        Me.lblPolicyLogo.Size = New System.Drawing.Size(31, 13)
        Me.lblPolicyLogo.TabIndex = 19
        Me.lblPolicyLogo.Text = "Logo"
        Me.lblPolicyLogo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblPolicyLogo.Visible = False
        '
        'lblPolicyIPFS
        '
        Me.lblPolicyIPFS.Location = New System.Drawing.Point(0, 206)
        Me.lblPolicyIPFS.Name = "lblPolicyIPFS"
        Me.lblPolicyIPFS.Size = New System.Drawing.Size(217, 19)
        Me.lblPolicyIPFS.TabIndex = 18
        Me.lblPolicyIPFS.Text = "IPFS Image"
        Me.lblPolicyIPFS.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'PanelRightImage
        '
        Me.PanelRightImage.Controls.Add(Me.LinkLabel1)
        Me.PanelRightImage.Controls.Add(Me.PictureBoxAssetIPFS)
        Me.PanelRightImage.Controls.Add(Me.PictureBoxAssetLogo)
        Me.PanelRightImage.Controls.Add(Me.lblPolicyIPFS)
        Me.PanelRightImage.Controls.Add(Me.lblPolicyLogo)
        Me.PanelRightImage.Controls.Add(Me.txtMetaDataMain)
        Me.PanelRightImage.Dock = System.Windows.Forms.DockStyle.Right
        Me.PanelRightImage.Location = New System.Drawing.Point(1262, 74)
        Me.PanelRightImage.Name = "PanelRightImage"
        Me.PanelRightImage.Size = New System.Drawing.Size(339, 736)
        Me.PanelRightImage.TabIndex = 6
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel1.Location = New System.Drawing.Point(3, 209)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(106, 16)
        Me.LinkLabel1.TabIndex = 20
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "View on pool.pm"
        '
        'PictureBoxAssetIPFS
        '
        Me.PictureBoxAssetIPFS.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.PictureBoxAssetIPFS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxAssetIPFS.Dock = System.Windows.Forms.DockStyle.Top
        Me.PictureBoxAssetIPFS.InitialImage = Global.CardanoBlockChainExamples.My.Resources.Resources.cardano_logo_white
        Me.PictureBoxAssetIPFS.Location = New System.Drawing.Point(0, 0)
        Me.PictureBoxAssetIPFS.Name = "PictureBoxAssetIPFS"
        Me.PictureBoxAssetIPFS.Size = New System.Drawing.Size(339, 203)
        Me.PictureBoxAssetIPFS.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBoxAssetIPFS.TabIndex = 17
        Me.PictureBoxAssetIPFS.TabStop = False
        '
        'PictureBoxAssetLogo
        '
        Me.PictureBoxAssetLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxAssetLogo.Location = New System.Drawing.Point(119, 237)
        Me.PictureBoxAssetLogo.Name = "PictureBoxAssetLogo"
        Me.PictureBoxAssetLogo.Size = New System.Drawing.Size(100, 100)
        Me.PictureBoxAssetLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBoxAssetLogo.TabIndex = 15
        Me.PictureBoxAssetLogo.TabStop = False
        Me.PictureBoxAssetLogo.Visible = False
        '
        'txtMetaDataMain
        '
        Me.txtMetaDataMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMetaDataMain.Location = New System.Drawing.Point(2, 237)
        Me.txtMetaDataMain.Multiline = True
        Me.txtMetaDataMain.Name = "txtMetaDataMain"
        Me.txtMetaDataMain.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtMetaDataMain.Size = New System.Drawing.Size(334, 499)
        Me.txtMetaDataMain.TabIndex = 21
        '
        'Timer2
        '
        Me.Timer2.Interval = 1000
        '
        'PanelMetadata
        '
        Me.PanelMetadata.Controls.Add(Me.PanelMetadataMain)
        Me.PanelMetadata.Controls.Add(Me.PanelMetaDataJSON)
        Me.PanelMetadata.Location = New System.Drawing.Point(701, 103)
        Me.PanelMetadata.Name = "PanelMetadata"
        Me.PanelMetadata.Size = New System.Drawing.Size(530, 194)
        Me.PanelMetadata.TabIndex = 7
        '
        'PanelMetadataMain
        '
        Me.PanelMetadataMain.Controls.Add(Me.Label17)
        Me.PanelMetadataMain.Controls.Add(Me.lstMetadata)
        Me.PanelMetadataMain.Controls.Add(Me.btnTop100)
        Me.PanelMetadataMain.Controls.Add(Me.lstMetadataLabelData)
        Me.PanelMetadataMain.Controls.Add(Me.btnGetNFTMetadata)
        Me.PanelMetadataMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelMetadataMain.Location = New System.Drawing.Point(0, 0)
        Me.PanelMetadataMain.Name = "PanelMetadataMain"
        Me.PanelMetadataMain.Size = New System.Drawing.Size(449, 194)
        Me.PanelMetadataMain.TabIndex = 11
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(9, 16)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(81, 13)
        Me.Label17.TabIndex = 1
        Me.Label17.Text = "Cardano Labels"
        '
        'lstMetadata
        '
        Me.lstMetadata.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lstMetadata.FullRowSelect = True
        Me.lstMetadata.HideSelection = False
        Me.lstMetadata.Location = New System.Drawing.Point(12, 32)
        Me.lstMetadata.MultiSelect = False
        Me.lstMetadata.Name = "lstMetadata"
        Me.lstMetadata.Size = New System.Drawing.Size(170, 159)
        Me.lstMetadata.TabIndex = 5
        Me.lstMetadata.UseCompatibleStateImageBehavior = False
        '
        'btnTop100
        '
        Me.btnTop100.Location = New System.Drawing.Point(188, 7)
        Me.btnTop100.Name = "btnTop100"
        Me.btnTop100.Size = New System.Drawing.Size(100, 23)
        Me.btnTop100.TabIndex = 8
        Me.btnTop100.Text = "Top 100 Labels"
        Me.btnTop100.UseVisualStyleBackColor = True
        '
        'lstMetadataLabelData
        '
        Me.lstMetadataLabelData.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstMetadataLabelData.FullRowSelect = True
        Me.lstMetadataLabelData.HideSelection = False
        Me.lstMetadataLabelData.Location = New System.Drawing.Point(188, 32)
        Me.lstMetadataLabelData.MultiSelect = False
        Me.lstMetadataLabelData.Name = "lstMetadataLabelData"
        Me.lstMetadataLabelData.Size = New System.Drawing.Size(253, 159)
        Me.lstMetadataLabelData.TabIndex = 6
        Me.lstMetadataLabelData.UseCompatibleStateImageBehavior = False
        '
        'btnGetNFTMetadata
        '
        Me.btnGetNFTMetadata.Location = New System.Drawing.Point(295, 7)
        Me.btnGetNFTMetadata.Name = "btnGetNFTMetadata"
        Me.btnGetNFTMetadata.Size = New System.Drawing.Size(164, 23)
        Me.btnGetNFTMetadata.TabIndex = 7
        Me.btnGetNFTMetadata.Text = "Latest 100 Label ID: 721 (NFT)"
        Me.btnGetNFTMetadata.UseVisualStyleBackColor = True
        '
        'PanelMetaDataJSON
        '
        Me.PanelMetaDataJSON.Controls.Add(Me.txtMetaData)
        Me.PanelMetaDataJSON.Controls.Add(Me.PictureBoxAssetIPFSMetadata)
        Me.PanelMetaDataJSON.Controls.Add(Me.PanelMetadataJSONTop)
        Me.PanelMetaDataJSON.Dock = System.Windows.Forms.DockStyle.Right
        Me.PanelMetaDataJSON.Location = New System.Drawing.Point(449, 0)
        Me.PanelMetaDataJSON.Name = "PanelMetaDataJSON"
        Me.PanelMetaDataJSON.Size = New System.Drawing.Size(81, 194)
        Me.PanelMetaDataJSON.TabIndex = 10
        Me.PanelMetaDataJSON.Visible = False
        '
        'txtMetaData
        '
        Me.txtMetaData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtMetaData.Location = New System.Drawing.Point(0, 42)
        Me.txtMetaData.Multiline = True
        Me.txtMetaData.Name = "txtMetaData"
        Me.txtMetaData.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtMetaData.Size = New System.Drawing.Size(81, 35)
        Me.txtMetaData.TabIndex = 9
        '
        'PictureBoxAssetIPFSMetadata
        '
        Me.PictureBoxAssetIPFSMetadata.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.PictureBoxAssetIPFSMetadata.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxAssetIPFSMetadata.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PictureBoxAssetIPFSMetadata.InitialImage = Global.CardanoBlockChainExamples.My.Resources.Resources.cardano_logo_white
        Me.PictureBoxAssetIPFSMetadata.Location = New System.Drawing.Point(0, 77)
        Me.PictureBoxAssetIPFSMetadata.Name = "PictureBoxAssetIPFSMetadata"
        Me.PictureBoxAssetIPFSMetadata.Size = New System.Drawing.Size(81, 117)
        Me.PictureBoxAssetIPFSMetadata.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBoxAssetIPFSMetadata.TabIndex = 18
        Me.PictureBoxAssetIPFSMetadata.TabStop = False
        '
        'PanelMetadataJSONTop
        '
        Me.PanelMetadataJSONTop.Controls.Add(Me.btnViewTx)
        Me.PanelMetadataJSONTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelMetadataJSONTop.Location = New System.Drawing.Point(0, 0)
        Me.PanelMetadataJSONTop.Name = "PanelMetadataJSONTop"
        Me.PanelMetadataJSONTop.Size = New System.Drawing.Size(81, 42)
        Me.PanelMetadataJSONTop.TabIndex = 10
        '
        'btnViewTx
        '
        Me.btnViewTx.Location = New System.Drawing.Point(3, 10)
        Me.btnViewTx.Name = "btnViewTx"
        Me.btnViewTx.Size = New System.Drawing.Size(75, 23)
        Me.btnViewTx.TabIndex = 0
        Me.btnViewTx.Text = "View TX"
        Me.btnViewTx.UseVisualStyleBackColor = True
        '
        'cmdDecode
        '
        Me.cmdDecode.Location = New System.Drawing.Point(338, 49)
        Me.cmdDecode.Name = "cmdDecode"
        Me.cmdDecode.Size = New System.Drawing.Size(75, 23)
        Me.cmdDecode.TabIndex = 32
        Me.cmdDecode.Text = "Decode"
        Me.cmdDecode.UseVisualStyleBackColor = True
        '
        'frmCardanoMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1601, 810)
        Me.Controls.Add(Me.PanelMetadata)
        Me.Controls.Add(Me.PanelTrackRewards)
        Me.Controls.Add(Me.PanelAssets)
        Me.Controls.Add(Me.PanelStakePools)
        Me.Controls.Add(Me.PanelBottom)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.PanelRightImage)
        Me.Controls.Add(Me.PanelTop)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmCardanoMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cardano Project Catalyst - Funded 3 Proposal - Visual Studio Smart Contract Plugi" &
    "n"
        Me.PanelTop.ResumeLayout(False)
        Me.PanelTop.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelStakePools.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.NumericUpDownMaxPools, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelTrackRewards.ResumeLayout(False)
        Me.PanelTrackRewards.PerformLayout()
        Me.PanelAddressDetails.ResumeLayout(False)
        Me.PanelAddressDetails.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.PanelAssets.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.NumericUpDownMaxAssets, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelAssetsPolicy.ResumeLayout(False)
        Me.PanelAssetsPolicy.PerformLayout()
        Me.PanelRightImage.ResumeLayout(False)
        Me.PanelRightImage.PerformLayout()
        CType(Me.PictureBoxAssetIPFS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxAssetLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelMetadata.ResumeLayout(False)
        Me.PanelMetadataMain.ResumeLayout(False)
        Me.PanelMetadataMain.PerformLayout()
        Me.PanelMetaDataJSON.ResumeLayout(False)
        Me.PanelMetaDataJSON.PerformLayout()
        CType(Me.PictureBoxAssetIPFSMetadata, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelMetadataJSONTop.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PanelTop As Panel
    Friend WithEvents PanelStakePools As Panel
    Friend WithEvents PanelTrackRewards As Panel
    Friend WithEvents PanelBottom As Panel
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As ToolStripStatusLabel
    Friend WithEvents btnTrackRewards As Button
    Friend WithEvents lstWalletTransactions As ListView
    Friend WithEvents lstRewardsTransactions As ListView
    Friend WithEvents lblTotalTransactions As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents cmdTrackRewardsGO As Button
    Friend WithEvents lblTrackRewardsAddress As Label
    Friend WithEvents txtCardanoWalletAddress As TextBox
    Friend WithEvents ToolStripStatusLabel2 As ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel3 As ToolStripStatusLabel
    Friend WithEvents lblAPIBalance As Label
    Friend WithEvents lblTotalStakePools As Label
    Friend WithEvents Timer1 As Timer
    Friend WithEvents btnStakePools As Button
    Friend WithEvents lstStakePoolsActive As ListView
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label4 As Label
    Friend WithEvents lstStakePoolsRetiring As ListView
    Friend WithEvents lstStakePoolsRetired As ListView
    Friend WithEvents Label2 As Label
    Friend WithEvents RadioStakePoolRetiring As RadioButton
    Friend WithEvents RadioStakePoolRetired As RadioButton
    Friend WithEvents RadioStakePoolActive As RadioButton
    Friend WithEvents btnStakePoolsRetreive As Button
    Friend WithEvents NumericUpDownMaxPools As NumericUpDown
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents btnAssets As Button
    Friend WithEvents PanelAssets As Panel
    Friend WithEvents lstAssetsOfPolicy As ListView
    Friend WithEvents lstAssets As ListView
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Label3 As Label
    Friend WithEvents btnAssetsLatestRetreive As Button
    Friend WithEvents NumericUpDownMaxAssets As NumericUpDown
    Friend WithEvents Label5 As Label
    Friend WithEvents lblTotalAssets As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents PictureBoxAssetLogo As PictureBox
    Friend WithEvents chkAssetImage As CheckBox
    Friend WithEvents PictureBoxAssetIPFS As PictureBox
    Friend WithEvents PanelAssetsPolicy As Panel
    Friend WithEvents Label9 As Label
    Friend WithEvents lblPolicyLogo As Label
    Friend WithEvents lblPolicyIPFS As Label
    Friend WithEvents btnGetOtherPolicyAssets As Button
    Friend WithEvents lblTotalAssetsOfPolicy As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents btnAssetsIndividualRetreive As Button
    Friend WithEvents txtAssetID As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents PanelRightImage As Panel
    Friend WithEvents Timer2 As Timer
    Friend WithEvents btnNFTGallery As Button
    Friend WithEvents btnIPFS As Button
    Friend WithEvents btnCLI As Button
    Friend WithEvents btnAbout As Button
    Friend WithEvents txtPolicyID As TextBox
    Friend WithEvents RadioButtonTESTNET As RadioButton
    Friend WithEvents RadioButtonMAINNET As RadioButton
    Friend WithEvents Label8 As Label
    Friend WithEvents txtUTCOffset As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents lstWalletTransactionsUTXOs As ListView
    Friend WithEvents LinkLabel1 As LinkLabel
    Friend WithEvents btnMetadata As Button
    Friend WithEvents PanelMetadata As Panel
    Friend WithEvents lstMetadataLabelData As ListView
    Friend WithEvents lstMetadata As ListView
    Friend WithEvents Label17 As Label
    Friend WithEvents btnGetNFTMetadata As Button
    Friend WithEvents btnTop100 As Button
    Friend WithEvents PanelMetaDataJSON As Panel
    Friend WithEvents txtMetaData As TextBox
    Friend WithEvents PanelMetadataJSONTop As Panel
    Friend WithEvents btnViewTx As Button
    Friend WithEvents PanelMetadataMain As Panel
    Friend WithEvents PictureBoxAssetIPFSMetadata As PictureBox
    Friend WithEvents txtMetaDataMain As TextBox
    Friend WithEvents cmdCodeConvert As Button
    Friend WithEvents lstCardanoAddress As ListView
    Friend WithEvents PanelAddressDetails As Panel
    Friend WithEvents lblCardanoAddressStakeAddress As Label
    Friend WithEvents lblCardanoAddressScript As Label
    Friend WithEvents lblCardanoAddressType As Label
    Friend WithEvents lblCardanoAddressAddress As Label
    Friend WithEvents lstAssetsLookup As ListView
    Friend WithEvents cmdAll As Button
    Friend WithEvents lblAssetLocation As Label
    Friend WithEvents lstAssetAddresses As ListView
    Friend WithEvents lstAssetHistory As ListView
    Friend WithEvents Label13 As Label
    Friend WithEvents lstAssetTransactions As ListView
    Friend WithEvents lstWalletTransactionsUTXOsNFTS As ListView
    Friend WithEvents cmdDecode As Button
End Class

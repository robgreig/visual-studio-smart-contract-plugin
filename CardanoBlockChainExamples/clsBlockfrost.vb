﻿Imports System.IO
Imports System.Net
Imports System.Web.Script.Serialization
Imports Newtonsoft.Json.Linq
'************************************************
' IMPORTANT YOU NEED Json.NET for this class
'
' to install it open "Tools"->"NuGet Package Manager"->"Package Manager Console" and type: Install-Package Newtonsoft.Json
'************************************************
#Region "Main Classes"
Public Class MetaDataLabelsResponse
    Public Property labels As New List(Of MetaDataLabels)
    Public Property count As Integer = 0
End Class
Public Class MetaDataLabels
    Public Property label As String = ""
    Public Property cip10 As String = ""  'causes an error if you try to access it
    Public Property count As String = ""
End Class
Public Class MetaDataLabelDataResponse
    Public Property labels As New List(Of MetaDataLabelData)
End Class
Public Class MetaDataLabelData
    Public Property tx_hash As String = ""
    Public Property json_metadata
End Class
Public Class MetaDataTransactionData
    Public Property label As String = ""
    Public Property json_metadata
End Class
Public Class IPFSResponse
    Public Property name As String = ""
    Public Property ipfs_hash As String = ""
    Public Property size As Integer = 0
End Class
Public Class ErrorResponse
    'this can be 400, 403, 404, 418, 429, 500
    Public Property status_code As Integer = 0
    Public Property [error] As String = ""
    Public Property message As String = ""
End Class
Public Class CardanoTX
    Public TX_Hash As String = ""
    Public TX_Type As String = ""
    Public CNTAsset_QTY As Integer = 0
    Public CNTAsset As New Asset
    Public ADA_Fee As Double = 0.00
    Public ADA_Amount As Double = 0.00
    Public ADA_TotalValue As Double = 0.00
    Public Time As String = ""
    Public TX_Address_From As String = ""
    Public TX_Address_To As String = ""
End Class
Public Class AssetHistory
    Public Property address As String = ""
    Public Property quantity As Long = 0
End Class
Public Class Asset
    Public Property asset As String = ""      'Concatenation of the policy_id and hex-encoded asset_name
    Public Property policy_id As String = ""
    Public Property asset_name As String = ""
    Public Property fingerprint As String = ""
    Public Property quantity As Long = 0
    Public Property initial_mint_tx_hash As String = ""
    Public Property onchain_metadata As New AssetOnchainMetadata
    Public Property metadata As New AssetMetadata
End Class
Public Class AssetOnchainMetadata
    Public Property name As String = ""
    Public Property image As String = ""
End Class
Public Class AssetMetadata
    Public Property name As String = ""
    Public Property description As String = ""
    Public Property ticker As String = ""
    Public Property url As String = ""
    Public Property logo As String = ""
End Class
Public Class StakePoolMetaData
    Public Property poolid As String = ""
    Public Property url As String = ""
    Public Property hash As String = ""
    Public Property ticker As String = ""
    Public Property name As String = ""
    Public Property description As String = ""
    Public Property homepage As String = ""
    Public Property epoch As String = ""
End Class
Public Class TX_Address_Transactions
    Public Property tx_hash As String = ""
    Public Property txt_index As Integer = 0
    Public Property block_height As Integer = 0
End Class
Public Class Cardano_Address_Information
    Public Property address As String = ""
    Public Property amount As List(Of TX_UTXO_Units)
    Public Property stake_address As String = ""
    Public Property type As String = "" 'era Enum: "byron" "shelley"
    Public Property script As Boolean = False
End Class
Public Class TX_Amount
    Public Property unit As String = ""
    Public Property quantity As Long = 0
End Class
Public Class TX_UTXO_Amount
    Public Property address As String = ""
    Public Property amount As List(Of TX_UTXO_Units)
End Class

Public Class TX_UTXO_Units
    Public Property unit As String = ""
    Public Property quantity As String = ""
End Class
'https://docs.blockfrost.io/#tag/Cardano-Transactions
Public Class BLOCK
    Public Property time As Integer = 0
    Public Property time_string As String = ""
    Public Property timeUTC_string As String = ""
    Public Property height As Integer = 0
    Public Property hash As String = ""
    Public Property slot As Integer = 0
    Public Property epoch As Integer = 0
    Public Property epoch_slot As Integer = 0
    Public Property slot_leader As String = ""
    Public Property size As Integer = 0
    Public Property tx_count As Integer = 0
    Public Property output As String = ""
    Public Property fees As String = ""
    Public Property block_vrf As String = ""
    Public Property previous_block As String = ""
    Public Property next_block As String = ""
    Public Property confirmations As Integer = 0
End Class
Public Class TX_UTXOs
    Public Property inputs As List(Of TX_UTXO_Amount)
    Public Property outputs As List(Of TX_UTXO_Amount)
End Class
Public Class IPFS_Pin
    'this can be 400, 403, 404, 418, 429, 500
    Public Property ipfs_hash As String = ""
    Public Property state As String = ""
End Class
Public Class TX
    Public Property hash As String = ""  'added by BF for new format
    Public Property block As String = ""
    Public Property block_height As Integer = 0 'added by BF for new format
    Public Property block_time As Integer = 0
    Public Property slot As Integer = 0
    Public Property index As Integer = 0
    Public Property output_amount As List(Of TX_Amount)
    Public Property fees As String = ""
    Public Property deposit As String = ""
    Public Property size As Integer = 0
    Public Property invalid_before As String = ""
    Public Property invalid_hereafter As String = ""
    Public Property utxo_count As Integer = 0
    Public Property withdrawal_count As Integer = 0
    Public Property mir_cert_count As Integer = 0 'added by BF for new format
    Public Property delegation_count As Integer = 0
    Public Property stake_cert_count As Integer = 0
    Public Property pool_update_count As Integer = 0
    Public Property pool_retire_count As Integer = 0
    Public Property asset_mint_or_burn_count As Integer = 0 'added by BF for new format
    Public Property redeemer_count As Integer = 0
    Public Property valid_contract As Boolean = False
End Class
#End Region
Public Class clsBlockfrost
    Public Const UseTestNet As Boolean = False
    Public Const UseMainNet As Boolean = True
    'API DOCUMENTATION https://docs.blockfrost.io/#section/Authentication

    '***********************************************************************
    'Cardano Mainnet	Endpoint
    Private ENDPOINT_MAINNET As String = "https://cardano-mainnet.blockfrost.io/api/v0"
    'Cardano Testnet Endpoint
    Private ENDPOINT_TESTNET As String = "https://cardano-testnet.blockfrost.io/api/v0"
    'IPFS Endpoint
    Private ENDPOINT_IPFS As String = "https://ipfs.blockfrost.io/api/v0"
    '***********************************************************************
    Private BLOCKFROST_API_KEY As String = BLOCKFROST_API_KEY_MAINNET
    Private _ENDPOINT As String = ENDPOINT_TESTNET
    Private Response_Status_Code As Integer = 0
    Private Response_Error As String = ""
    Private Response_Message As String = ""
    Private Response_JSON As String = ""
    Private Balance As Integer = 0
    Public Sub New(bUseMainNet As Boolean, Optional bUseIPFS As Boolean = False)
        If bUseIPFS Then
            _ENDPOINT = ENDPOINT_IPFS  'reset if IPFS
            BLOCKFROST_API_KEY = BLOCKFROST_API_KEY_IPFS
        ElseIf bUseMainNet Then
            _ENDPOINT = ENDPOINT_MAINNET 'uses testnet by default
            BLOCKFROST_API_KEY = BLOCKFROST_API_KEY_MAINNET
        Else
            _ENDPOINT = ENDPOINT_TESTNET 'reset if changed
            BLOCKFROST_API_KEY = BLOCKFROST_API_KEY_TESTNET
        End If
    End Sub
    Public Function GetAddressExtendedList(sAddress As String) As List(Of String)
        Dim sURL As String = _ENDPOINT & "/addresses/" & sAddress & "/extended"
        Return GetStringListFromObject(sURL)
    End Function
    Public Function GetAddressTransactionsList(sAddress As String) As List(Of String)
        'Dim sURL As String = _ENDPOINT & "/addresses/" & sAddress & "/txs" depreciated
        Dim sURL As String = _ENDPOINT & "/addresses/" & sAddress & "/transactions"
        Return GetStringListFromObject(sURL)
    End Function
    Public Function GetAddressDetails(sAddress As String) As List(Of String)
        Dim sURL As String = _ENDPOINT & "/addresses/" & sAddress & "/total"
        Return GetStringListFromObject(sURL)
    End Function
    Public Function GetAddressInformation(sAddress As String) As Cardano_Address_Information
        Dim sURL As String = _ENDPOINT & "/addresses/" & sAddress
        Dim MyCardano_Address_Information As New Cardano_Address_Information
        Try
            Dim sJSONResponse = GetJSONStringURL(sURL)
            MyCardano_Address_Information = JsonHelper.ToClass(Of Cardano_Address_Information)(sJSONResponse)
            'MyTransaction.poolid = sPoolID
        Catch ex As Exception
            Debug.Print(ex.Message)
            'Stop
        End Try
        Return MyCardano_Address_Information
    End Function
    Public Function GetTransactionMetaDataList(sHash As String) As TX
        Dim sURL As String = _ENDPOINT & "/txs/" & sHash
        Dim MyTX As TX = GetTransactionInformation(sHash)
        Return MyTX
    End Function
    Public Function GetMetadataLabelData(sLabel As String) As MetaDataLabelDataResponse
        Dim sURL As String = _ENDPOINT & "/metadata/txs/labels/" & sLabel
        sURL = sURL & "?order=desc" 'newest first
        sURL = sURL & "&page=1"
        sURL = sURL & "&count=100"

        Dim sRet As List(Of String) = GetStringListFromObject(sURL)

        Dim retMetaDataLabelsResponse As New MetaDataLabelDataResponse
        Try
            Dim sJSONResponse = GetJSONStringURL(sURL)
            Dim retList As New List(Of String)
            Try
                Dim array As JArray = JArray.Parse(sJSONResponse)

                For Each item As JObject In array
                    Dim MetaDataLabeldata As New MetaDataLabelData
                    Try
                        MetaDataLabeldata.tx_hash = item.SelectToken("tx_hash").ToString
                        MetaDataLabeldata.json_metadata = item.SelectToken("json_metadata").ToString
                        retMetaDataLabelsResponse.labels.Add(MetaDataLabeldata)
                    Catch ex As Exception
                        Debug.Print(ex.Message)
                    End Try
                Next
            Catch ex As Exception
                Debug.Print(ex.Message)
            End Try
        Catch ex As Exception
            Debug.Print(ex.Message)
            'Stop
        End Try
        Return retMetaDataLabelsResponse
    End Function
    Public Function GetMetadataLabels() As MetaDataLabelsResponse
        Dim sURL As String = _ENDPOINT & "/metadata/txs/labels"
        Dim retMetaDataLabelsResponse As New MetaDataLabelsResponse
        Try
            Dim sJSONResponse = GetJSONStringURL(sURL)
            Dim retList As New List(Of String)
            Try
                If SurfWeb(sURL) = True Then
                    Dim array As JArray = JArray.Parse(sJSONResponse)

                    For Each item As JObject In array
                        Dim MetaDataLabels As New MetaDataLabels
                        MetaDataLabels.label = item.SelectToken("label").ToString
                        MetaDataLabels.count = item.SelectToken("count").ToString
                        Try
                            If Not IsNothing(item.SelectToken("cip10")) Then
                                MetaDataLabels.cip10 = item.SelectToken("cip10").ToString
                            End If
                        Catch ex As Exception
                            Debug.Print(ex.Message)
                        End Try
                        retMetaDataLabelsResponse.labels.Add(MetaDataLabels)
                    Next
                    retMetaDataLabelsResponse.count = retMetaDataLabelsResponse.labels.Count
                End If
            Catch ex As Exception
                Debug.Print(ex.Message)
            End Try
        Catch ex As Exception
            Debug.Print(ex.Message)
            'Stop
        End Try
        Return retMetaDataLabelsResponse
    End Function
    Public Function GetTransactionMetadata(sTX_Hash As String) As String
        Dim sURL As String = _ENDPOINT & "/txs/" & sTX_Hash & "/metadata"
        Return GetJSONStringURL(sURL)
    End Function
    Public Function GetStakePoolList(iMaxResults As Integer, iType As Integer) As List(Of Object)
        Dim sURL As String = _ENDPOINT & "/pools"

        If iType = 2 Then
            sURL = sURL & "/retiring"
            'iMaxResults = 1 'testing
        ElseIf iType = 3 Then
            sURL = sURL & "/retired"
            'iMaxResults = 2 'testing
        End If

        Return GetStringListFromObjectWithPages(sURL, iMaxResults)
    End Function
    Public Function GetAssetsList(iMaxResults As Integer) As List(Of Object)
        Dim sURL As String = _ENDPOINT & "/assets"
        sURL = sURL & "?order=desc"
        Return GetStringListFromObjectWithPages(sURL, iMaxResults)
    End Function
    Public Function GetPolicyAssetsList(sPolicyID As String, iMaxResults As Integer) As List(Of Object)
        Dim sURL As String = _ENDPOINT & "/assets/policy/" & sPolicyID
        'sURL  = sURL  & "?order=desc"
        Return GetStringListFromObjectWithPages(sURL, iMaxResults)
    End Function
    Public Function GetAssetAddressList(sAssetID As String, iMaxResultsPerPage As Integer, iPage As Integer, sOrder As String) As List(Of Object)
        Dim sURL As String = _ENDPOINT & "/assets/" & sAssetID & "/addresses"
        'sURL  = sURL  & "?order=desc"
        Return GetStringListFromObjectWithPages(sURL, iMaxResultsPerPage)
    End Function
    Public Function GetAssetTransactionList(sAssetID As String, iMaxResultsPerPage As Integer, iPage As Integer, sOrder As String) As List(Of Object)
        Dim sURL As String = _ENDPOINT & "/assets/" & sAssetID & "/transactions"
        'sURL  = sURL  & "?order=desc"
        Return GetStringListFromObjectWithPages(sURL, iMaxResultsPerPage)
    End Function
    Public Function GetAssetHistoryList(sAssetID As String, iMaxResultsPerPage As Integer, iPage As Integer, sOrder As String) As List(Of Object)
        Dim sURL As String = _ENDPOINT & "/assets/" & sAssetID & "/history"
        'sURL  = sURL  & "?order=desc"
        Return GetStringListFromObjectWithPages(sURL, iMaxResultsPerPage)
    End Function
    Public Function GetStakePoolTransactions(sHash As String, iMaxResults As Integer) As List(Of Object)
        Dim sURL As String = _ENDPOINT & "/txs/" & sHash
        Return GetStringListFromObjectWithPages(sURL, iMaxResults)
    End Function
    Function GetJSONStringURL(sURL As String) As String
        Dim sRet As String = ""
        Try
            If SurfWeb(sURL) = True Then
                Return Response_JSON
            Else
                'Stop
            End If
        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try
        Return sRet
    End Function
    Function PostJSONStringURL(sURL As String) As String
        Dim sRet As String = ""
        Try
            If SurfWeb(sURL, False) = True Then
                Return Response_JSON
            Else
                'Stop
            End If
        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try
        Return sRet
    End Function
    Function GetStringListFromObject(sURL As String) As List(Of String)
        Dim retList As New List(Of String)
        Try
            If SurfWeb(sURL) = True Then
                Dim sJSONResponse As String = Response_JSON
                Dim array As JArray = JArray.Parse(sJSONResponse)
                For Each item In array
                    retList.Add(item.ToString)
                Next
            Else
                'if there has never been any transactions on the address this will produce an error
                'Stop
            End If
        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try
        Return retList
    End Function
    Function GetObjectListFromObject(sURL As String) As List(Of Object)
        Dim retList As New List(Of Object)
        Try
            If SurfWeb(sURL) = True Then
                Dim sJSONResponse As String = Response_JSON
                Dim array As JObject = JObject.Parse(sJSONResponse)
                retList.Add(array)
            Else
                'Stop
            End If
        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try
        Return retList
    End Function
    Function IPFSGetImageURL(sText As String) As String
        Dim sRet As String = sText
        Try
            Dim sURL As String = Replace(sText, "ipfs://ipfs", "")
            sURL = Replace(sURL, "ipfs://", "/")
            sRet = "https://dweb.link/ipfs" & sURL
        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try
        Return sRet
    End Function
    Function IPFSPinFile(sIPFSPath As String) As IPFS_Pin
        Dim retIPFS_Pin As New IPFS_Pin
        Dim sURL As String = _ENDPOINT & "/ipfs/pin/add/" & sIPFSPath
        Try
            'THIS USES A POST NOT THE DEFAULT GET
            Dim sJSONResponse = PostJSONStringURL(sURL)
            retIPFS_Pin = JsonHelper.ToClass(Of IPFS_Pin)(sJSONResponse)
        Catch ex As Exception
            Debug.Print(ex.Message)
            'Stop
        End Try
        Return retIPFS_Pin
    End Function
    Function IPFSStripURL(sText As String) As String
        Dim sRet As String = sText
        Try
            Dim sURL As String = Replace(sText, "ipfs://ipfs", "")
            sURL = Replace(sURL, "ipfs://", "")
            sURL = Replace(sURL, "/", "")
            sRet = sURL
        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try
        Return sRet
    End Function
    Function IPFSGetImageURLDev(sText As String) As String
        Return "https://ipfs.blockfrost.dev/ipfs/" & IPFSStripURL(sText)
        'Return _ENDPOINT & "/ipfs/gateway/" & StripIPFSURL(sText)
    End Function
    Public Function ConvertFileToBase64(ByVal fileName As String) As String
        Dim sRet As String = ""
        Try
            sRet = Convert.ToBase64String(System.IO.File.ReadAllBytes(fileName))
        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try
        Return sRet
    End Function
    Private Function UploadFilesToRemoteUrl(ByVal MyFile As String) As String
        Dim sIPFSURL As String = ENDPOINT_IPFS & "/ipfs/add"
        Dim sRet As String = ""
        Dim boundary As String = "----------------------------" & DateTime.Now.Ticks.ToString("x")
        Dim headers As New WebHeaderCollection
        headers.Add("project_id", BLOCKFROST_API_KEY_IPFS)

        Dim request As HttpWebRequest = CType(WebRequest.Create(sIPFSURL), HttpWebRequest)
        With request
            .Headers = headers
            .ContentType = "multipart/form-data; boundary=" & boundary
            .Method = "POST"
            .KeepAlive = True
        End With
        Dim memStream As Stream = New System.IO.MemoryStream()
        Dim boundarybytes = System.Text.Encoding.ASCII.GetBytes(vbCrLf & "--" & boundary & vbCrLf)
        Dim endBoundaryBytes = System.Text.Encoding.ASCII.GetBytes(vbCrLf & "--" & boundary & "--")
        Dim formdataTemplate As String = vbCrLf & "--" & boundary & vbCrLf & "Content-Disposition: form-data; name=""{0}"";" & vbCrLf & vbCrLf & "{1}"
        Dim headerTemplate As String = "Content-Disposition: form-data; name=""{0}""; filename=""{1}""" & vbCrLf & "Content-Type: application/octet-stream" & vbCrLf & vbCrLf

        Try
            memStream.Write(boundarybytes, 0, boundarybytes.Length)
            Dim header = String.Format(headerTemplate, "uplTheFile", MyFile)
            Dim headerbytes = System.Text.Encoding.UTF8.GetBytes(header)
            memStream.Write(headerbytes, 0, headerbytes.Length)

            Using fs = New FileStream(MyFile, FileMode.Open, FileAccess.Read)
                Dim buffer = New Byte(1023) {}
                Dim bytesRead = 0
                Do
                    bytesRead = fs.Read(buffer, 0, buffer.Length)
                    If bytesRead > 0 Then memStream.Write(buffer, 0, bytesRead)
                Loop While bytesRead > 0
            End Using

            memStream.Write(endBoundaryBytes, 0, endBoundaryBytes.Length)
            request.ContentLength = memStream.Length

            Using requestStream As Stream = request.GetRequestStream()
                memStream.Position = 0
                Dim tempBuffer As Byte() = New Byte(memStream.Length - 1) {}
                memStream.Read(tempBuffer, 0, tempBuffer.Length)
                memStream.Close()
                requestStream.Write(tempBuffer, 0, tempBuffer.Length)
            End Using

            Using response = request.GetResponse()
                Dim stream2 As Stream = response.GetResponseStream()
                Dim reader2 As StreamReader = New StreamReader(stream2)
                sRet = reader2.ReadToEnd()
            End Using
        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try
        Return sRet
    End Function
    Function IPFSUploadFile(sFilename As String) As String
        Dim sRet As String = ""
        Try
            Dim bRet As Boolean = False
            Dim sJSONResponse As String = ""
            sJSONResponse = UploadFilesToRemoteUrl(sFilename)

            If (sJSONResponse > "") Then
                Dim MyIPFSResponse As New IPFSResponse
                MyIPFSResponse = JsonHelper.ToClass(Of IPFSResponse)(sJSONResponse)
                sRet = MyIPFSResponse.ipfs_hash
            End If

        Catch ex As Exception
            'Error in accessing the resource, handle it
            'DoLog(3, "ERROR SurfWeb - " & ex.Message)
            SetErrorCode(ex.Message)
            Response_Error = ex.Message
        End Try

        Return sRet
    End Function
    Function GetStringListFromObjectWithPages(sURL As String, iMaxResults As Integer) As List(Of Object)
        Dim retList As New List(Of Object)
        Dim iPage As Integer = 1
        Try
doquery:
            Dim sURL2 As String = sURL
            If InStr(sURL, "?") Then
                sURL2 = sURL2 & "&page=" & iPage
            Else
                sURL2 = sURL2 & "?page=" & iPage
            End If
            If SurfWeb(sURL2) = True Then
                Dim sJSONResponse As String = Response_JSON
                Dim array As JArray = JArray.Parse(sJSONResponse)
                For Each item In array
                    If retList.Count < iMaxResults Then
                        retList.Add(item)
                    Else
                        Exit For
                    End If
                Next

                If retList.Count > 0 And retList.Count < iMaxResults And array.Count > 0 And array.Count > 0 Then
                    Application.DoEvents()
                    iPage = iPage + 1
                    GoTo doquery
                End If
            End If
        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try
        Return retList
    End Function
    Public Function GetStakePoolMetaData(sPoolID As String) As StakePoolMetaData
        Dim MyStakePoolMetaData As New StakePoolMetaData
        Dim sURL As String = _ENDPOINT & "/pools/" & sPoolID & "/metadata"
        Try
            Dim sJSONResponse = GetJSONStringURL(sURL)
            MyStakePoolMetaData = JsonHelper.ToClass(Of StakePoolMetaData)(sJSONResponse)
            MyStakePoolMetaData.poolid = sPoolID
        Catch ex As Exception
            Debug.Print(ex.Message)
            'Stop
        End Try
        Return MyStakePoolMetaData
    End Function
    Public Function GetSpecificAsset(sAssetID As String) As Asset
        Dim MyAsset As New Asset
        Dim sURL As String = _ENDPOINT & "/assets/" & sAssetID
        Try
            Dim sJSONResponse = GetJSONStringURL(sURL)
            If sJSONResponse > "" Then
                MyAsset = JsonHelper.ToClass(Of Asset)(sJSONResponse)
                MyAsset.asset = sAssetID
            End If
        Catch ex As Exception
            Debug.Print(ex.Message)
            'Stop
        End Try
        Return MyAsset
    End Function
    Public Function CheckAssetExists(sAssetID As String) As Boolean
        Dim bRet As Boolean = False
        Try
            Dim MyAsset As New Asset
            MyAsset = GetSpecificAsset(sAssetID)
            If Not IsNothing(MyAsset) Then
                If MyAsset.asset_name > "" Then
                    bRet = True
                End If
            End If
        Catch ex As Exception
            Debug.Print(ex.Message)
            'Stop
        End Try
        Return bret
    End Function
    Public Function GetStakePoolMetaDataOLD(sPoolID As String) As StakePoolMetaData
        Dim MyStakePoolMetaData As New StakePoolMetaData
        Dim MyDictionary As New Dictionary(Of String, String)
        Dim sURL As String = _ENDPOINT & "/pools/" & sPoolID & "/metadata"
        Call GetStringListFromObject(sURL)

        Dim response = JsonHelper.ToClass(Of StakePoolMetaData)(Response_JSON)
        'Dim count As Integer = response.data.user.edge_follow.count
        'For i As Integer = 0 To count - 1
        '    Dim id = response.data.user.edge_follow.edges(i).node.id
        '    Console.WriteLine(id.ToString)
        'Next


        MyDictionary = GetDictionaryFromObject(sURL)
        MyStakePoolMetaData.poolid = sPoolID
        For Each item As KeyValuePair(Of String, String) In MyDictionary
            Select Case item.Key
                Case "url"
                    MyStakePoolMetaData.url = item.Value
                Case "hash"
                    MyStakePoolMetaData.hash = item.Value
                Case "name"
                    MyStakePoolMetaData.name = item.Value
                Case "description"
                    MyStakePoolMetaData.description = item.Value
                Case "ticker"
                    MyStakePoolMetaData.ticker = item.Value
                Case "homepage"
                    MyStakePoolMetaData.homepage = item.Value
                Case Else
                    'Stop
            End Select
        Next
        Return MyStakePoolMetaData
    End Function
    Public Function GetDictionaryFromObject(sURL As String) As Dictionary(Of String, String)
        Dim retDict As New Dictionary(Of String, String)
        Try
            If SurfWeb(sURL) = True Then

                'GetStringListFromObject(sURL As String) As List(Of String)

                Dim sJSONResponse As String = Response_JSON
                Try
                    Dim dict As Dictionary(Of String, Object) = New JavaScriptSerializer().Deserialize(Of Object)(sJSONResponse)
                    For Each item As KeyValuePair(Of String, Object) In dict
                        retDict.Add(item.Key, item.Value)
                    Next
                Catch ex As Exception
                    Debug.Print(ex.Message)
                End Try
            Else
                'Stop
            End If
        Catch ex As Exception
            'Stop
            Debug.Print(ex.Message)
        End Try
        Return retDict
    End Function
    'Public Function GetBlockInformation(sHash As String) As BLOCK
    '    Dim MyBlock As New BLOCK
    '    Dim sURL As String = _ENDPOINT & "/blocks/" & sHash
    '    Try
    '        Dim sJSONResponse = GetJSONStringURL(sURL)
    '        MyBlock = JsonHelper.ToClass(Of BLOCK)(sJSONResponse)

    '        If Not IsNothing(MyBlock) Then
    '            MyBlock.time_string = UnixToTime(MyBlock.time)

    '            Dim UTCOffset As Integer = -18000   '-18000 represents -5 hrs from GMT which is Eastern Standard Time
    '            'If IsNumeric(frmNFT.txtUTCOffset.Text) Then UTCOffset = Int(frmNFT.txtUTCOffset.Text)
    '            MyBlock.timeUTC_string = UnixToTimeUTC(MyBlock.time, UTCOffset)
    '        End If
    '    Catch ex As Exception
    '        Debug.Print(ex.Message)
    '        'Stop
    '    End Try
    '    Return MyBlock
    'End Function
    Public Function GetTransactionInformation(sHash As String) As TX
        Dim MyTransaction As New TX
        Dim sURL As String = _ENDPOINT & "/txs/" & sHash
        Try
            Dim sJSONResponse = GetJSONStringURL(sURL)
            MyTransaction = JsonHelper.ToClass(Of TX)(sJSONResponse)
            'MyTransaction.poolid = sPoolID
        Catch ex As Exception
            Debug.Print(ex.Message)
            'Stop
        End Try
        Return MyTransaction
    End Function
    Public Function GetTransactionInformationUTXO(sHash As String) As TX_UTXOs
        Dim MyTransaction As New TX_UTXOs
        Dim sURL As String = _ENDPOINT & "/txs/" & sHash & "/utxos"
        Try
            Dim sJSONResponse = GetJSONStringURL(sURL)
            MyTransaction = JsonHelper.ToClass(Of TX_UTXOs)(sJSONResponse)
            'MyTransaction.poolid = sPoolID
        Catch ex As Exception
            Debug.Print(ex.Message)
            'Stop
        End Try
        Return MyTransaction
    End Function
    Public Function GetAPIBalance() As Integer
        Dim iRet As Integer = -99
        Try
            Dim sURL As String = _ENDPOINT & "/metrics/"
            If SurfWeb(sURL) = True Then
                Dim sJSONResponse As String = Response_JSON
                Dim iTime As Integer = 0
                Dim iCalls As Integer = 0

                Try
                    Dim dict As Object = New JavaScriptSerializer().Deserialize(Of Object)(sJSONResponse)
                    For Each item As Object In dict
                        For Each pair As Object In item
                            Dim sPair As String = pair.ToString
                            If InStr(sPair, "calls") Then
                                sPair = Replace(Replace(sPair, "[", ""), "]", "")
                                Dim sFields() As String = Split(sPair, ",")
                                If sFields.Length = 2 Then iCalls = Int(sFields(1)) + iCalls
                            End If
                        Next
                    Next
                    'workaround ;(
                    'Dim dict As Dictionary(Of String, Integer) = ConvertStringToKeyValuePairStringInteger(sJSONResponse)
                    'If dict.ContainsKey("calls") = True Then iCalls = dict("calls").ToString()
                    'If dict.ContainsKey("time") = True Then iTime = dict("time").ToString()
                    iRet = iCalls
                Catch ex As Exception
                    Debug.Print(ex.Message)
                End Try
            Else
                'Stop
            End If
        Catch ex As Exception
            'Stop
            Debug.Print(ex.Message)
        End Try
        Return iRet
    End Function
    Private Function ConvertStringToKeyValuePairStringInteger(sJSONResponse As String) As Dictionary(Of String, Integer)
        Dim retDict As New Dictionary(Of String, Integer)
        Try
            Dim dict As Object = New JavaScriptSerializer().Deserialize(Of Object)(sJSONResponse)
            For Each item As Object In dict
                For Each pair As Object In item
                    Dim sPair As String = pair.ToString
                    sPair = Replace(Replace(sPair, "[", ""), "]", "")
                    Dim sFields() As String = Split(sPair, ",")
                    retDict.Add(sFields(0), Int(sFields(1)))
                Next
            Next
            'For Each pair As KeyValuePair(Of String, Integer) In retDict
            '    ' Display key and value.
            '    Dim key As String = pair.Key
            '    Dim value As Integer = pair.Value
            '    Console.WriteLine("{0}, {1}", key, value)
            'Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return retDict
    End Function
    Private Function ConvertStringToKeyValuePairStringStringOLD(sJSONResponse As String) As Dictionary(Of String, String)
        Dim retDict As New Dictionary(Of String, String)
        Try
            Dim dict As Dictionary(Of String, Object) = New JavaScriptSerializer().Deserialize(Of Object)(sJSONResponse)
            For Each item As Object In dict
                For Each pair As Object In item
                    Dim sPair As String = pair.ToString
                    sPair = Replace(Replace(sPair, "[", ""), "]", "")
                    Dim sFields() As String = Split(sPair, ",")
                    retDict.Add(Trim(sFields(0)), Trim(sFields(1)))
                Next
            Next
            For Each pair As KeyValuePair(Of String, String) In retDict
                ' Display key and value.
                Dim key As String = pair.Key
                Dim value As Integer = pair.Value
                Console.WriteLine("{0}, {1}", key, value)
            Next
        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try
        Return retDict
    End Function
    Private Function SurfWeb(sPage As String, Optional bGet As Boolean = True) As Boolean
        Dim bRet As Boolean = False
        If sPage = "" Then GoTo End_function
        Try
            Dim sJSONResponse As String = ""
            Dim targetURI As New Uri(sPage)
            Dim request = CType(WebRequest.Create(targetURI), HttpWebRequest)
            'Debug.Print(sPage)

            Dim headers As New WebHeaderCollection

            headers.Add("project_id", BLOCKFROST_API_KEY)
            request.Headers = headers
            If bGet Then
                request.Method = "GET"
            Else
                request.Method = "POST"
            End If

            Using sr = New StreamReader(request.GetResponse().GetResponseStream())
                sJSONResponse = sr.ReadToEnd()
            End Using

            If (sJSONResponse > "") Then
                Dim jss As New JavaScriptSerializer()
                Response_JSON = sJSONResponse 'serialise string response back to JSON
                bRet = True
            End If
        Catch ex As Exception
            'Error in accessing the resource, handle it
            SetErrorCode(ex.Message)
            Response_Error = ex.Message
            bRet = False
        End Try
End_function:
        Return bRet
    End Function
    Sub SetErrorCode(sText As String)
        Dim sMessage As String = ""
        If InStr(sText, "400") Then
            Response_Status_Code = 400
            sMessage = "ERROR Blockfrost Response 400 - the request is not valid."
        ElseIf InStr(sText, "402") Then
            Response_Status_Code = 402
            sMessage = "ERROR Blockfrost Response 402 - the project exceeded its daily request limit."
        ElseIf InStr(sText, "403") Then
            Response_Status_Code = 403
            sMessage = "ERROR Blockfrost Response 403 - the request is not authenticated."
        ElseIf InStr(sText, "404") Then
            Response_Status_Code = 404
            sMessage = "ERROR Blockfrost Response 404 - the resource doesn't exist."
        ElseIf InStr(sText, "418") Then
            Response_Status_Code = 418
            sMessage = "ERROR Blockfrost Response 418 - the user has been auto-banned for flooding too much data - this meesage preceeds previously receiving ERROR code 402 or 429."
        ElseIf InStr(sText, "429") Then
            Response_Status_Code = 429
            sMessage = "ERROR Blockfrost Response 429 - the user has sent too many requests in a given amount of time and therefore has been rate-limited."
        ElseIf InStr(sText, "500") Then
            Response_Status_Code = 500
            sMessage = "ERROR Blockfrost Response 500 - the Blockfrost endpoints are having a problem."
        ElseIf InStr(sText, "The remote name could not be resolved: ") Then
            MsgBox("Do you have internet access?", vbCritical)
        Else
            Response_Status_Code = 0
        End If

        frmCardanoMain.DoLog(3, sMessage)
    End Sub
    Public Function ConvertBytesToImage(ByVal BA As Byte()) As Image
        Dim converter As ImageConverter = New ImageConverter()
        Dim image = CType(converter.ConvertFrom(BA), Image)
        Return image
    End Function
    Public Function DecodeHEXString(sHEXEncodedString As String) As String
        Dim sRet As String = ""
        Dim bts As New List(Of Byte)
        For x As Integer = 0 To Len(sHEXEncodedString) - 1 Step 2
            bts.Add(CByte(Convert.ToInt32(sHEXEncodedString(x) & sHEXEncodedString(x + 1), 16)))
        Next
        If bts.Count > 0 Then sRet = System.Text.Encoding.ASCII.GetString(bts.ToArray)
        Return sRet
    End Function
    Public Function EncodeHEXString(sString As String) As String
        Dim bts() As Byte = System.Text.Encoding.ASCII.GetBytes(sString)
        Dim sb As New System.Text.StringBuilder
        For Each b As Byte In bts
            sb.Append(b.ToString("X").PadLeft(2, "0"c))
        Next
        Return sb.ToString.ToLower
    End Function
    Public Function UnixToTime(ByVal strUnixTime As String) As Date
        UnixToTime = DateAdd(DateInterval.Second, Val(strUnixTime), #1/1/1970#)
        If UnixToTime.IsDaylightSavingTime = True Then
            UnixToTime = DateAdd(DateInterval.Hour, 1, UnixToTime)
        End If
    End Function
    Public Function UnixToTimeUTC(ByVal strUnixTime As String, ByVal offset As Integer) As Date
        'uses an offset from GMT to work out timezone
        UnixToTimeUTC = DateAdd(DateInterval.Second, Val(strUnixTime - offset), #1/1/1970#)
        UnixToTimeUTC = DateAdd(DateInterval.Hour, 1, UnixToTimeUTC)
    End Function
    Public Function TimeToUnix(ByVal dteDate As Date) As String
        If dteDate.IsDaylightSavingTime = True Then
            dteDate = DateAdd(DateInterval.Hour, -1, dteDate)
        End If
        TimeToUnix = DateDiff(DateInterval.Second, #1/1/1970#, dteDate)
    End Function
End Class
